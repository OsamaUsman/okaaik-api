﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Notifications
{
    public class PushNotification
    {
        public static async Task<object> Send_Notification(NotificationViewModel vm)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {

                    var obj = new
                    {
                        app_id = "35f21322-4003-484f-8ab6-ea550e7da2b6",
                        contents = new { en = vm.message },
                        android_group = "Okaaik",
                        headings = new { en = vm.title },
                        large_icon = "https://dk0pm9zdlq16s.cloudfront.net/576b79b4-e452-4296-843b-7533cd93cb0a.png",
                        url = vm.url,
                        data = new { vm.tag , vm.id }, //lessons, homework, msg               
                        include_player_ids = vm.playerId
                    };
                    StringContent content = new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json");
                    using (var response = await httpClient.PostAsync("https://onesignal.com/api/v1/notifications", content))
                    {
                        string apiResponse = await response.Content.ReadAsStringAsync();
                        object o = JsonConvert.DeserializeObject(apiResponse);

                        //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                        //object routes_list = json_serializer.DeserializeObject(apiResponse);

                        return new
                        {
                            data = o,
                            status = "success"
                        };
                        //string status = apiResponse.Split("|")[0].ToString();
                        //if (status == "\"1701")
                        //{
                        //    return new
                        //    {
                        //        data = code,
                        //        status = "success"
                        //    };
                        //}
                        //else
                        //{
                        //    return new
                        //    {
                        //        data = "something went wrong.",
                        //        status = "error"
                        //    };
                        //}

                        //string mysteri = JsonConvert.DeserializeObject(apiResponse);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        


            //  var request = WebRequest.Create("https://onesignal.com/api/v1/notifications") as HttpWebRequest;

            //  request.KeepAlive = true;
            //  request.Method = "POST";
            //  request.ContentType = "application/json; charset=utf-8";

            ////  var serializer = new JavaScriptSerializer();
            //  var obj = new
            //  {
            //      app_id = "bd8bf30a-2275-4973-9649-cf417c26d982",
            //      contents = new { en = message },
            //      android_group = "fusion",
            //      headings = new { en = title },
            //      large_icon = "https://appick.io/u/noti_icon.png",
            //      //url = url,
            //      data = new { activity = activity }, //lessons, homework, msg               
            //      include_player_ids = playerIDs
            //  };


            //  //var param = serializer.Serialize(obj);
            //  byte[] byteArray = Encoding.UTF8.GetBytes(param);

            //  string responseContent = null;

            //  try
            //  {
            //      using (var writer = request.GetRequestStream())
            //      {
            //          writer.Write(byteArray, 0, byteArray.Length);
            //      }

            //      using (var response = request.GetResponse() as HttpWebResponse)
            //      {
            //          using (var reader = new StreamReader(response.GetResponseStream()))
            //          {
            //              responseContent = reader.ReadToEnd();
            //          }
            //      }
            //  }
            //  catch (WebException ex)
            //  {
            //      System.Diagnostics.Debug.WriteLine(ex.Message);
            //      System.Diagnostics.Debug.WriteLine(new StreamReader(ex.Response.GetResponseStream()).ReadToEnd());
            //  }

            //  System.Diagnostics.Debug.WriteLine(responseContent);
        }
    }
}
