﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class DeletedViewModel
    {
        public long[] productIds { get; set; }
        public bool isDeleted { get; set; }
    }
}