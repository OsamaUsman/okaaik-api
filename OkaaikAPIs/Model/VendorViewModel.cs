﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class VendorViewModel
    {
        public string userName { get; set; }
        public string email { get; set; }
        public string password { get; set; }
       

        public string bakeryName { get; set; }
        public long userId { get; set; }
        public string logo { get; set; }
        public string cover { get; set; }
        public string aboutUs { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string contactNo { get; set; }
        public string url { get; set; }
        public string tradelicenseNumber { get; set; }
        public DateTime tradelicenseExpiry { get; set; }
        public string licenseSource { get; set; }
        public string landlineNo { get; set; }
        public string instagramUsername { get; set; }
        public string nameOnTradeLicense { get; set; }
        public string[] emiratesids { get; set; }
        public string[] productsamples { get; set; }
        public string[] tradelicenses { get; set; }




    }
}