﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class UpdateVendorViewModel
    {

        public vendor vendor { get; set; }
        public string[] emiratesids { get; set; }
        public string[] productsamples { get; set; }
        public string[] tradelicenses { get; set; }
        public List<bakeryPartner> partners { get; set; }
    }
}