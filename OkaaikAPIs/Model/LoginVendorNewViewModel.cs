﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class LoginVendorNewViewModel
    {
        public string email { get; set; }
        public string password { get; set; }
        public string deviceId { get; set; }
    }
}