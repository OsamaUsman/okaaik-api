﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class NotificationViewModel
    {
        public string[] playerId { get; set; }
        public string message { get; set; }
        public string title { get; set; }
        public string url { get; set; }
        public string tag { get; set; }
        public long id { get; set; }

    }
}