﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class PStatusViewModel
    {
        public long id { get; set; }
        public string status { get; set; }
        public string note { get; set; }
    }
}