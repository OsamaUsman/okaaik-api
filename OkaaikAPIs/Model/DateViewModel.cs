﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class DateViewModel
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
    }
}