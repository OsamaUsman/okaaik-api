﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class CurrentPasswordViewModel
    {
        [Required]
        public long userId { get; set; }
        [Required]
        public string password { get; set; }

    }
}