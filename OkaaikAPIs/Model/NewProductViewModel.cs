﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class NewProductViewModel
    {
        public product product { get; set; }

        public IEnumerable<cakeSize> cakeSizes { get; set; }

        public IEnumerable<cakeLayer> cakeLayers { get; set; }

        public cakeFlavourViewModel cakeFlavours { get; set; }

        public CakeImagesViewModel cakeImages { get; set; }

    }
}