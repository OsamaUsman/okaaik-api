﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class FilterWishListVieModel
    {
        public DateTime Date { get; set; }
        public string[] Cities { get; set; }
        public string[] Categories { get; set; }
        public string[] Bakeries { get; set; }
        public string DeviceId { get; set; }
        public long? userid { get; set; }
        public string[] dietryId { get; set; }
    }
}