﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class StatusViewModel
    {
        public long userId { get; set; }
        public string status { get; set; }
        public string note { get; set; }

    }
}