﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class ProductViewModel
    {
        public long productId { get; set; }
        public string title { get; set; }
        public Nullable<long> vendorId { get; set; }
        public Nullable<long> categoryId { get; set; }
        public string description { get; set; }
      
        public Nullable<double> size { get; set; }
        public Nullable<double> weight { get; set; }
        public Nullable<decimal> price { get; set; }
        public string persons { get; set; }
      
        public Nullable<bool> isApproved { get; set; }
        public Nullable<System.DateTime> approvedOn { get; set; }
        public string coverImg { get; set; }
        public Nullable<int> minimumTime { get; set; }
        public Nullable<decimal> perLayerCost { get; set; }
    }
}