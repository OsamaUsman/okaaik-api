﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class ActiveAllViewModel
    {
        public string status { get; set; }
        public long[] productIds { get; set; }
    }
}