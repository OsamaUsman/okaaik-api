﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class OrderViewModel
    {
        public long orderId { get; set; }
        public Nullable<long> userId { get; set; }
        public Nullable<long> vendorId { get; set; }
        public string modeOfpayment { get; set; }
        public Nullable<decimal> bill { get; set; }
        public Nullable<decimal> vat { get; set; }
        public Nullable<decimal> deliveryCharges { get; set; }
        public Nullable<decimal> totalBill { get; set; }
        public Nullable<decimal> discount { get; set; }
        public Nullable<decimal> netBill { get; set; }
        public string url { get; set; }
        public string transactionId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string status { get; set; }
        public Nullable<long> promoCodeId { get; set; }
        public Nullable<decimal> paidAmount { get; set; }
        public string note { get; set; }
        public Nullable<long> deliveryModeId { get; set; }

      

        public List<orderDetail> orderlist { get; set; }


    }
}