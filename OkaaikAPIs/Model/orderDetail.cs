//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OkaaikAPIs.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class orderDetail
    {
        public long orderDetailsid { get; set; }
        public Nullable<long> orderId { get; set; }
        public Nullable<long> productId { get; set; }
        public Nullable<long> vendorId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string status { get; set; }
        public Nullable<long> addressId { get; set; }
        public string address { get; set; }
        public Nullable<int> cakeFlavour { get; set; }
        public string size { get; set; }
        public Nullable<int> qty { get; set; }
        public Nullable<decimal> cost { get; set; }
        public string deliveryMethod { get; set; }
        public string message { get; set; }
        public Nullable<bool> deliverToMe { get; set; }
        public Nullable<decimal> latitude { get; set; }
        public Nullable<decimal> longitude { get; set; }
        public string productName { get; set; }
        public string addressTitle { get; set; }
        public string flavourName { get; set; }
        public Nullable<long> flavourId { get; set; }
        public Nullable<long> sizeId { get; set; }
        public Nullable<long> layerId { get; set; }
        public string pickupTime { get; set; }
        public string name { get; set; }
        public string contactNo { get; set; }
        public string streetNo { get; set; }
        public string layerName { get; set; }
        public string note { get; set; }
        public Nullable<long> categoryId { get; set; }
    
        public virtual oOrder oOrder { get; set; }
        public virtual product product { get; set; }
        public virtual product product1 { get; set; }
        public virtual vendor vendor { get; set; }
    }
}
