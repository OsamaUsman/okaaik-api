﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class CheckProfileViewModel
    {
        [Required]
        public long userId { get; set; }

        [Required]
        public string email  { get; set; }

        [Required]
        public string mobileno { get; set; }
    }
}