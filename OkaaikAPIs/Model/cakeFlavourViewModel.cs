﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class cakeFlavourViewModel
    {
        public long[] cakeFlavourId { get; set; }

        public int[] cakeFlavours { get; set; }

        public int pId { get; set; }
    }
}