﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class SortingViewModel
    {
        public long userId { get; set; }
        public string sortBy { get; set; }
        public string city { get; set; }
    }
}