﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class MyProductViewModel
    {
        public long productId { get; set; }
        public string title { get; set; }
        public long vendorId { get; set; }
        public long categoryId { get; set; }
        public string description { get; set; }
       
      
        public decimal price { get; set; }
       
        public bool isApproved { get; set; }
        public DateTime approvedOn { get; set; }
        public string coverImg { get; set; }
        public int minimumTime { get; set; }
        public decimal perLayerCost { get; set; }

        public int[] flavourId { get; set; }
        public string[] images { get; set; }
        //public List<cakeSizeViewModel> cakesizeList { get; set; }
    }
}