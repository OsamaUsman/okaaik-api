//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OkaaikAPIs.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class productImg
    {
        public long productImgId { get; set; }
        public string imagePath { get; set; }
        public Nullable<long> productId { get; set; }
    
        public virtual product product { get; set; }
    }
}
