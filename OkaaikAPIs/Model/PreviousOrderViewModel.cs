﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class PreviousOrderViewModel
    {
        public  object order { get; set; }
        public List<object> orderdetails { get; set; }
        public List<object>  products { get; set; }
    }
}