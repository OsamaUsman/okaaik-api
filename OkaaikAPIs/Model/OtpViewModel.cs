﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OkaaikAPIs.Model
{
    public class OtpViewModel
    {
        public string mobileno { get; set; }
        public string email { get; set; }
        public string type { get; set; }
    }
}