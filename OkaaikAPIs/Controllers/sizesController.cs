﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{

    public class sizesController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll(long id,int pageNo)
        {
            using (Entities db = new Entities())
            {
                int page = 10;
                if (pageNo == 1)
                {
                    page = 0;
                }
                else 
                {
                    page = (pageNo - 1) * page;
                }

                var sizes = db.sizes.Where(x=> x.vendorId == id).Select(x => new { x.sizeId, x.name , x.vendorId }).OrderBy(x => x.sizeId).Skip(page).Take(10).ToList();
                //var query = (from bn in db.sizes
                //             where bn.vendorId == id
                //             select new
                //             {
                //                 bn.sizeId,
                //                 bn.name,
                //                 bn.vendorId
                //             }).ToList();
                var totalRows = db.sizes.Where(x => x.vendorId == id).Count();
                //List<int> pages = new List<int>();
                //int ten = 0;
                //if (totalSizes > 10) 
                //{
                //    for (int i = 0; i < totalSizes; i++)
                //    {
                        
                //        if (i == ten) 
                //        {
                //            pages.Add(ten);
                           
                //        }
                //    }
                //}


                return Ok(new {status="success" ,  sizes , totalRows } );
            }
                

        }


        [HttpGet]
        public IHttpActionResult GetAllForDropdown(long id)
        {
            using (Entities db = new Entities())
            {



                var query = (from bn in db.sizes
                             where bn.vendorId == id
                             select new
                             {
                                 bn.sizeId,
                                 bn.name,
                                 bn.vendorId
                             }).ToList();

            


                return Ok(new { status = "success", data = query });
            }


        }




        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.sizes
                             where bn.sizeId == id
                             select new
                             {
                                 bn.sizeId,
                                 bn.name
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, size size)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != size.sizeId)
                {
                    return BadRequest();
                }

                db.Entry(size).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!sizeExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(size size)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.sizes.Add(size);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                size size = db.sizes.Find(id);
                if (size == null)
                {
                    return NotFound();
                }

                db.sizes.Remove(size);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool sizeExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.sizes.Count(e => e.sizeId == id) > 0;
            }
        }
    }
}
