﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{
    public class vendorsController : ApiController
    {

        [HttpGet]
        [Route("api/vendors/VendorsByStatus/{status}")]
        public IHttpActionResult VendorsByStatus(string status)
        {
            using (Entities db = new Entities())
            {
               
                var query = (from ven in db.vendors
                             join us in db.users
                             on ven.userId equals us.userId
                             where us.status == status
                             select new
                             {
                                 ven.vendorId,
                                 ven.userId,
                                 ven.address,
                                 ven.bakeryName,
                                 ven.city,
                                 ven.contactNo,
                                 ven.cover,
                                 ven.aboutUs,
                                 ven.latitude,
                                 ven.logo,
                                 ven.longitude,
                                 ven.productType,
                                 ven.suspendedTill,
                                 ven.url,
                                 ven.landlineNo,
                                 ven.licenseSource,
                                 us.status,

                             }).OrderBy(x=> x.vendorId ).ToList();
                return Ok(query);
            }

        }

        [HttpGet]
        [Route("api/vendors/getVendors")]
        public IHttpActionResult Getvendors()
        {
            using (Entities db = new Entities())
            {
                var verified = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "verified").Count();
                var resubmitted = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "resubmitted").Count();
                var sendback = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "send back for revisions").Count();
                var suspended = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "suspended").Count();
                var approved = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "approved").Count();
                var declined = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "declined").Count();
                var unverified = db.users.Where(x => x.roles.ToLower() == "vendor" && x.status.ToLower() == "unverified").Count();
                var query = (from ven in db.vendors
                             join us in db.users
                             on ven.userId equals us.userId 
                             select new
                             {
                                 ven.vendorId,
                                 ven.userId,
                                 ven.address,
                                 ven.bakeryName,
                                 ven.city,
                                 ven.contactNo,
                                 ven.cover,
                                 ven.aboutUs,
                                 ven.latitude,
                                 ven.logo,
                                 ven.longitude,
                                 ven.productType,
                                 ven.suspendedTill,
                                 ven.url,
                                 ven.landlineNo,
                                 ven.licenseSource,
                                 us.status,

                             }).OrderBy(us => us.status == "unverified").ThenBy(us => us.status == "declined").ThenBy(us => us.status == "approved").ThenBy(us => us.status == "suspended").ThenBy(us => us.status == "send back for revisions").ThenBy(us => us.status == "resubmitted").ThenBy(us => us.status == "verified").ToList();
                return Ok(new {status="success" , query , verified , resubmitted , sendback , suspended, approved, declined , unverified });
            }

        }
        
        [HttpGet]
        [Route("api/vendors/vendorDetails/{id}")]
        public IHttpActionResult vendorDetails(long id)
        {
            using (Entities db = new Entities())
            {
                var vendor = (from ven in db.vendors
                              join us in db.users
                              on ven.userId equals us.userId
                              where ven.vendorId == id 
                              select new
                              {
                                  ven.vendorId,
                                  ven.address,
                                  ven.bakeryName,
                                  ven.userId,
                                  ven.city,
                                  ven.contactNo,
                                  ven.cover,
                                  ven.aboutUs,
                                  ven.latitude,
                                  ven.logo,
                                  ven.longitude,
                                  ven.productType,
                                  ven.suspendedTill,
                                  ven.url,
                                  ven.landlineNo,
                                  ven.licenseSource,
                                  ven.tradelicenseNumber,
                                  ven.tradelicenseExpiry,
                                  us.status,
                                  us.userName,
                                  us.email

                              }).ToList();

                var partners = (from bp in db.bakeryPartners
                                where bp.vendorId == id
                                select new
                                {
                                    bp.bpartnerId,
                                    bp.email,
                                    bp.name,
                                    bp.emiratesId

                                }).ToList();
                var documents = (from doc in db.vendordocs
                                 where doc.vendorId == id
                                 select new
                                 {
                                     doc.vendorId,
                                     doc.vendorDocId,
                                     doc.docType,
                                     doc.documentPath
                                 }).ToList();


                return Ok(new { vendor = vendor, partners = partners, documents = documents });
            }

        }

        [HttpGet]
        [Route("api/vendors/vendorData/{id}")]
        public IHttpActionResult vendorData(long id) 
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var vendor = db.vendors.Where(x => x.vendorId == id).Select(x => new
                    {
                        x.vendorId,
                        x.user.userName,
                        x.user.email,
                        x.user.password,
                        x.bakeryName,
                        x.logo,
                        x.cover,
                        x.aboutUs,
                        x.address,
                        x.city,
                        x.contactNo,
                        x.landlineNo,
                        x.tradelicenseNumber,
                        x.url,
                        x.tradelicenseExpiry,
                        x.nameOnTradeLicense,
                        x.licenseSource,
                        x.productType,
                        x.instagramUsername,
                        x.reviewNote,
                        x.userId

                    }).FirstOrDefault();

                    var partners = db.bakeryPartners.Where(x => x.vendorId == id).Select(x=> new 
                    {
                    x.bpartnerId,
                    x.email,
                    x.emiratesId,
                    x.name,
                    }).ToList();

                   var tradelicenses = db.vendordocs.Where(x => x.vendorId == id && x.docType == "license").Select(x=> x.documentPath).ToList();
                   var emiratesids = db.vendordocs.Where(x => x.vendorId == id && x.docType == "id").Select(x => x.documentPath).ToList();

                    return Ok(new { status = "success", data = new { vendor.vendorId, vendor.userName, vendor.email, vendor.password, vendor.bakeryName, vendor.logo, vendor.cover, vendor.aboutUs, vendor.address, vendor.city, vendor.contactNo, vendor.landlineNo, vendor.tradelicenseNumber, vendor.userId, vendor.url, vendor.tradelicenseExpiry, vendor.nameOnTradeLicense, vendor.licenseSource, producttype = vendor.productType, vendor.instagramUsername, vendor.reviewNote, emiratesids, tradelicenses, partners } });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        //[HttpGet]
        //[Route("api/vendors/checkff/{id}")]
        //public IHttpActionResult checkff(long id) 
        //{
        
        //}

        [HttpPut]

        public IHttpActionResult vendorStatus(long id, string status)
        {
            using (Entities db = new Entities())
            {
                var row = db.users.AsNoTracking().Where(ven => ven.userId == id).FirstOrDefault();
                if (row != null)
                {
                    if (status == "true")
                    {
                        user us = new user();
                        us.userId = row.userId;
                        us.firstName = row.firstName;
                        us.userName = row.userName;
                        us.lastName = row.lastName;
                        us.password = row.password;
                        us.verificationCode = row.verificationCode;
                        us.email = row.email;
                        us.mobileno = row.mobileno;
                        us.roles = row.roles;
                        us.suspendedTill = row.suspendedTill;
                        us.status = "approved";
                        try
                        {
                            db.Entry(us).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });

                            }
                        }
                        catch (Exception ex)
                        {

                            return Ok(ex);
                        }
                    }
                    else if (status == "false")
                    {
                        user us = new user();
                        us.userId = row.userId;
                        us.userName = row.userName;
                        us.firstName = row.firstName;
                        us.lastName = row.lastName;
                        us.password = row.password;
                        us.verificationCode = row.verificationCode;
                        us.email = row.email;
                        us.mobileno = row.mobileno;
                        us.roles = row.roles;
                        us.suspendedTill = row.suspendedTill;
                        us.status = "declined";

                        try
                        {
                            db.Entry(us).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });
                            }
                        }
                        catch (Exception ex)
                        {

                            return Ok(ex);
                        }
                    }
                    else if (status == "suspended")
                    {
                        user us = new user();
                        us.userId = row.userId;
                        us.userName = row.userName;
                        us.firstName = row.firstName;
                        us.lastName = row.lastName;
                        us.password = row.password;
                        us.verificationCode = row.verificationCode;
                        us.email = row.email;
                        us.mobileno = row.mobileno;
                        us.roles = row.roles;
                        us.suspendedTill = row.suspendedTill;
                        us.status = "suspended";

                        try
                        {
                            db.Entry(us).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });
                            }
                        }
                        catch (Exception ex)
                        {

                            return Ok(ex);
                        }
                    }
                    else if (status == "active")
                    {
                        user us = new user();
                        us.userId = row.userId;
                        us.userName = row.userName;
                        us.firstName = row.firstName;
                        us.lastName = row.lastName;
                        us.password = row.password;
                        us.verificationCode = row.verificationCode;
                        us.email = row.email;
                        us.mobileno = row.mobileno;
                        us.roles = row.roles;
                        us.suspendedTill = row.suspendedTill;
                        us.status = "active";

                        try
                        {
                            db.Entry(us).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });
                            }
                        }
                        catch (Exception ex)
                        {

                            return Ok(ex);
                        }
                    }
                    else
                    {
                        return Ok(new { data = "something went wrong", status = "error" });
                    }

                }
                else
                {
                    return NotFound();
                }

            }

        }

        [HttpPut]
        [Route("api/vendors/UpdateStatus")]
        public IHttpActionResult UpdateStatus(StatusViewModel vm)
        {
            using (Entities db = new Entities())
            {
                var user =  db.users.Where(x => x.userId == vm.userId).FirstOrDefault();
                if (user == null)
                {
                    return Ok(new { status = "error", data = "not exist." });
                }
                else 
                {
                    if (vm.status.ToLower() == "sent back for revision")
                    {
                        var vendor = db.vendors.Where(x => x.userId == vm.userId).FirstOrDefault();
                        if (vendor != null)
                        {
                            vendor.reviewNote = vm.note;
                            db.Entry(vendor).State = EntityState.Modified;
                            db.SaveChanges();
                        }



                    }

                    user.status = vm.status.ToLower();
                    db.Entry(user).State = EntityState.Modified;
                    int check = db.SaveChanges();
                    if (check > 0)
                    {
                        return Ok(new { status = "success", data = "updated" });
                    }
                    else
                    {
                        return Ok(new { status = "error", data = "something went wrong." });

                    }
                }
                

            }
        }

        [ResponseType(typeof(vendor))]
        [HttpGet]
        [Route("api/vendors/getVendor/{id}")]
        public IHttpActionResult Getvendor(long id)
        {

            using (Entities db = new Entities())
            {
                var query = (from ven in db.vendors
                             where ven.vendorId == id
                             select new
                             {

                                 ven.vendorId,
                                 ven.address,
                                 ven.bakeryName,
                                 ven.userId,
                                 ven.city,
                                 ven.contactNo,
                                 ven.cover,
                                 ven.aboutUs,
                                 ven.latitude,
                                 ven.logo,
                                 ven.longitude,
                                 ven.productType,
                                 ven.status,
                                 ven.suspendedTill,
                                 ven.landlineNo,
                                 ven.licenseSource,
                                 ven.url
                             }).ToList();

                if (query == null)
                {
                    return NotFound();
                }

                return Ok(query);
            }
        }

        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/vendors/putVendor")]
        public IHttpActionResult Putvendor(UpdateVendorViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {

              
                    db.Entry(vm.vendor).State = EntityState.Modified;

                    try
                    {
                        int che = db.SaveChanges();
                        long venId = vm.vendor.vendorId;
                        if (che > 0)
                        {
                          
                            try
                            {
                               
                                if (vm.emiratesids != null  && vm.tradelicenses != null)
                                {
                                //Delete Docs
                                var deletevendorsdocs =  db.vendordocs.Where(x=> x.vendorId == venId).ToList();
                                //Delete Partners
                                var deletePartners = db.bakeryPartners.Where(x => x.vendorId == venId).ToList();
                                
                                if (deletePartners.Count() != 0)
                                {
                                    db.bakeryPartners.RemoveRange(deletePartners);
                                    db.SaveChanges();
                                }

                                if (deletevendorsdocs.Count() != 0) 
                                {
                                    db.vendordocs.RemoveRange(deletevendorsdocs);
                                    db.SaveChanges();
                                }
                                //Add Partners
                                foreach (var item in vm.partners)
                                {
                                    item.vendorId = venId;
                                    db.bakeryPartners.Add(item);
                                }
                                db.SaveChanges();

                                for (int i = 0; i < vm.emiratesids.Length; i++)
                                    {
                                        vendordoc vendoc = new vendordoc();
                                        vendoc.docType = "id";
                                        vendoc.documentPath = vm.emiratesids[i];
                                        vendoc.vendorId = venId;

                                        db.vendordocs.Add(vendoc);

                                    }
                                    try
                                    {
                                        db.SaveChanges();

                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                    for (int i = 0; i < vm.tradelicenses.Length; i++)
                                    {
                                        vendordoc vendoc = new vendordoc();
                                        vendoc.docType = "license";
                                        vendoc.documentPath = vm.tradelicenses[i];
                                        vendoc.vendorId = venId;

                                        db.vendordocs.Add(vendoc);

                                    }
                                    try
                                    {
                                        db.SaveChanges();
                                         var user = db.users.Where(x => x.userId == vm.vendor.userId).FirstOrDefault();
                                        user.status = "resubmitted";
                                        db.Entry(user).State = EntityState.Modified;
                                         db.SaveChanges();
                                        return Ok(new { vendorId = venId, data = "Added", status = "success" });

                                    }
                                    catch (Exception)
                                    {

                                        throw;
                                    }
                                    
                                }


                                else
                                {
                                    return Ok(new { data = "something went wrong", status = "error" });
                                }

                            }
                            catch (Exception)
                            {

                                throw;
                            }
                        }
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                


                try
                {
                    db.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        [Route("api/vendors/postVendor")]
        public IHttpActionResult Postvendor(VendorViewModel vm)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                user us = new user();
                string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(vm.password);
                us.password = encpass;
                us.userName = vm.userName;
                us.email = vm.email;
                us.roles = "vendor";
                us.status = "unverified";
                db.users.Add(us);
                try
                {

                    int ch = db.SaveChanges();
                    var userID = us.userId;
                    if (ch > 0)
                    {
                        vendor vn = new vendor();
                        vn.aboutUs = vm.aboutUs;
                        vn.address = vm.address;
                        vn.bakeryName = vm.bakeryName;
                        vn.city = vm.city;
                        vn.contactNo = vm.contactNo;
                        vn.cover = vm.cover;
                        vn.logo = vm.logo;
                        vn.url = vm.url;
                        vn.landlineNo = vm.landlineNo;
                        vn.licenseSource = vm.licenseSource;
                        vn.tradelicenseExpiry = vm.tradelicenseExpiry;
                        vn.tradelicenseNumber = vm.tradelicenseNumber;
                        vn.instagramUsername = vm.instagramUsername;
                        vn.nameOnTradeLicense = vm.nameOnTradeLicense;

                        vn.userId = userID;
                        db.vendors.Add(vn);

                        try
                        {
                            int che = db.SaveChanges();
                            long venId = vn.vendorId;
                            if (che > 0)
                            {
                                //bakeryPartner bkp = new bakeryPartner();
                                //bkp.email = vm.pemail;
                                //bkp.emiratesId = vm.emiratesId;
                                //bkp.name = vm.name;
                                //bkp.vendorId = venId;
                                //db.bakeryPartners.Add(bkp);
                                try
                                {
                                    //int check = db.SaveChanges();
                                    //var pId = bkp.bpartnerId;

                                    if (vm.emiratesids != null &&  vm.tradelicenses != null)
                                    {
                                        for (int i = 0; i < vm.emiratesids.Length; i++)
                                        {
                                            vendordoc vendoc = new vendordoc();
                                            vendoc.docType = "id";
                                            vendoc.documentPath = vm.emiratesids[i];
                                            vendoc.vendorId = venId;

                                            db.vendordocs.Add(vendoc);

                                        }
                                        try
                                        {
                                            db.SaveChanges();

                                        }
                                        catch (Exception)
                                        {

                                            throw;
                                        }
                                        for (int i = 0; i < vm.tradelicenses.Length; i++)
                                        {
                                            vendordoc vendoc = new vendordoc();
                                            vendoc.docType = "license";
                                            vendoc.documentPath = vm.tradelicenses[i];
                                            vendoc.vendorId = venId;

                                            db.vendordocs.Add(vendoc);
                                        }
                                        try
                                        {
                                            db.SaveChanges();
                                            return Ok(new { vendorId = venId, userId = userID , userEmail = us.email, data = "Added", status = "success" });

                                        }
                                        catch (Exception)
                                        {

                                            throw;
                                        }
                                       
                                    }


                                    else
                                    {
                                        return Ok(new { data = "something went wrong", status = "error" });
                                    }

                                }
                                catch (Exception)
                                {

                                    throw;
                                }
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    else
                    {
                        return Ok(new { data = "something went wrong", status = "error" });
                    }

                }
                catch (Exception)
                {

                    throw;
                }


            }

            return Ok();
        }

        protected static object patnersList<bakeryPartner>(IEnumerable<bakeryPartner> list)
        {


            Entities db = new Entities();
            db.bakeryPartners.AddRange((IEnumerable<Model.bakeryPartner>)list);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex;
            }
            Added st = new Added();
            return st;

        }

        [HttpPost]
        public IHttpActionResult PostPartnersList(IEnumerable<bakeryPartner> list)
        {
            var row = patnersList(list);

            return Ok(row);

        }

        [ResponseType(typeof(vendor))]
        [HttpDelete]
        [Route("api/vendors/removeVendor/{id}")]
        public IHttpActionResult Deletevendor(long id)
        {
            using (Entities db = new Entities())
            {
                vendor vendor = db.vendors.Find(id);
                if (vendor == null)
                {
                    return NotFound();
                }

                db.vendors.Remove(vendor);
                int check;
                try
                {
                    check = db.SaveChanges();
                }
                catch (Exception ex)
                {

                    Error er = new Error();
                    return Ok(ex);

                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }



        private bool vendorExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.vendors.Count(e => e.vendorId == id) > 0;
            }
        }
    }
}