﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace Okaaikapis.controllers
{

    public class cakeFlavourscontroller : ApiController
    {


        [Route("api/cakeFlavours/getcakeFlavours")]
        [HttpGet]
        public IHttpActionResult getcakeFlavours()
        {
            using (Entities db = new Entities())
            {
                var query = (from cf in db.cakeFlavours
                             select new
                             {
                                 cf.cakeFlavourId,
                                 cf.productId,
                                 cf.flavourId
                             }).ToList();
                if (query == null)
                {

                }

                return Ok(query);
            }
        }


        [Route("api/cakeFlavours/getcakeFlavoursbyproduct/{id}")]
        [HttpGet]
        public IHttpActionResult getcakeFlavoursbyproduct(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from cf in db.cakeFlavours
                             join pro in db.products
                             on cf.cakeFlavourId equals pro.productId
                             where cf.productId == id
                             select new
                             {
                                 cf.cakeFlavourId,
                                 cf.productId,
                                 cf.flavourId,

                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }
        }



        [Route("api/cakeFlavours/getcakeflavour/{id}")]
        [HttpGet]
        public IHttpActionResult getcakeflavour(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from cf in db.cakeFlavours
                             where cf.cakeFlavourId == id
                             select new
                             {
                                 cf.cakeFlavourId,
                                 cf.productId,
                                 cf.flavourId
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }
        }

        //[Route("api/cakeFlavours/addcakeflavourwithspecific/{cakeflavour}/{proid}")]
        [HttpPost]
        public IHttpActionResult addcakeFlavours(cakeFlavourViewModel cf)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                for (int i = 0; i < cf.cakeFlavours.Length; i++)
                {
                    cakeFlavour f = new cakeFlavour();
                    f.productId = cf.pId;
                    f.flavourId = cf.cakeFlavours[i];
                    db.cakeFlavours.Add(f);

                }

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);

        }
        [HttpPut]
        public IHttpActionResult UpdatecakeFlavours(cakeFlavourViewModel cf)
        {
            using (Entities db = new Entities())
            {


                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                //if () { }

                try
                {
                    if (cf.pId != 0)
                    {
                        db.cakeFlavours.RemoveRange(db.cakeFlavours.Where(x => x.productId == cf.pId));
                        db.SaveChanges();
                    }


                    for (int i = 0; i < cf.cakeFlavours.Length; i++)
                    {
                        cakeFlavour f = new cakeFlavour();
                        //f.cakeFlavourId = cf.cakeFlavourId[i];
                        f.productId = cf.pId;
                        f.flavourId = cf.cakeFlavours[i];
                        db.cakeFlavours.Add(f);
                    }
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);

        }



        [Route("api/cakeFlavours/updatecakeflavour/{id}")]
        [HttpPut]
        public IHttpActionResult putcakeflavour(long id, cakeFlavour cakeflavour)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != cakeflavour.cakeFlavourId)
                {
                    return BadRequest();
                }

                db.Entry(cakeflavour).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (!cakeflavourexists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated up = new Updated();
            return Ok(up);
        }

        //[Route("api/cakeFlavours/addcakeflavour")]
        //[HttpPost]
        //public IHttpActionResult postcakeflavour(cakeflavour cakeflavour)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        if (!ModelState.isvalid)
        //        {
        //            return BadRequest(ModelState);
        //        }

        //        db.cakeFlavours.add(cakeflavour);
        //        db.SaveChanges();
        //    }
        //    added st = new added();
        //    return Ok(st);
        //}


        [Route("api/cakeFlavours/removecakeflavour/{id}")]
        [HttpDelete]
        public IHttpActionResult deletecakeflavour(long id)
        {
            using (Entities db = new Entities())
            {
                cakeFlavour cakeflavour = db.cakeFlavours.Find(id);
                if (cakeflavour == null)
                {
                    return NotFound();
                }

                db.cakeFlavours.Remove(cakeflavour);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }



        private bool cakeflavourexists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.cakeFlavours.Count(e => e.cakeFlavourId == id) > 0;
            }
        }
    }
}