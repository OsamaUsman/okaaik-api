﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class favouritiesController : ApiController
    {


        [HttpGet]
        [Route("api/favourities/getFavourities")]
        public IHttpActionResult Getfavourities()
        {
            using (Entities db = new Entities())
            {
                var query = (from fav in db.favourities
                             select new
                             {
                                 fav.favId,
                                 fav.customerId,
                                 fav.productId,
                                 fav.date
                             }).ToList();
                return Ok(query);
            }
        }


        [ResponseType(typeof(favourity))]
        [HttpGet]
        [Route("api/favourities/getFavourit/{id}")]
        public IHttpActionResult Getfavourity(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from fav in db.favourities
                             where fav.favId == id
                             select new
                             {
                                 fav.favId,
                                 fav.customerId,
                                 fav.productId,
                                 fav.date
                             }).ToList();

                if (query == null)
                {
                    return NotFound();
                }

                return Ok(query);
            }
        }

        // PUT: api/favourities/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/favourities/updateFavourit/{id}")]
        public IHttpActionResult Putfavourity(long id, favourity favourity)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != favourity.favId)
                {
                    return BadRequest();
                }

                db.Entry(favourity).State = EntityState.Modified;

                try
                {
                    int status = db.SaveChanges();
                    if (status > 0)
                    {

                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!favourityExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

            }
            Updated st = new Updated();
            return Ok(st);
        }

        // POST: api/favourities
        [ResponseType(typeof(favourity))]
        [HttpPost]
        [Route("api/favourities/addFavourit")]
        public IHttpActionResult Postfavourity(favourity favourity)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.favourities.Add(favourity);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }

        // DELETE: api/favourities/5
        [ResponseType(typeof(favourity))]
        [HttpDelete]
        [Route("api/favourities/removefavourit/{id}")]
        public IHttpActionResult Deletefavourity(long id)
        {
            using (Entities db = new Entities())
            {
                favourity favourity = db.favourities.Find(id);
                if (favourity == null)
                {
                    return NotFound();
                }

                db.favourities.Remove(favourity);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }


        private bool favourityExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.favourities.Count(e => e.favId == id) > 0;
            }
        }
    }
}