using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class dashboardController : ApiController
    {
        public static List<long> GetLists()
        {
            Entities db = new Entities();
            {
                var result = (from s in db.orderDetails
                              join pro in db.products
                              on s.productId equals pro.productId
                              // define the data source
                              group s by s.productId into g  // group all items by status
                              orderby g.Count() descending // order by count descending
                              select new
                              {

                                  g.Key,

                              }) // cast the output
                .Take(5) // take just 5 items
                .ToList();
                List<long> ids = new List<long>();

                for (int i = 0; i < result.Count; i++)
                {

                    ids.Add(result[i].Key.Value);
                }


                return ids;
            }





            //var _result1 = from pro in db.products join i in ids on pro.productId equals i select new
            //{
            //    pro.productId

            //};
            //var _result2 = (db.products.Where(p => ids.Contains(p.productId))).ToList();

            // List<product> _result3 = new List<product>();

            //foreach (long k in ids) { _result3.Add(db.products.Where(j => j.productId == k).SingleOrDefault()); };




        }


        [HttpGet]
        public IHttpActionResult Sorting(string city, string sortBy, long userId)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    if (sortBy.ToLower() == "time")
                    {
                        var products = db.products.Join(db.vendors, pro => pro.vendorId, ven => ven.vendorId, (pro, ven) => new { pro, ven }).Where(pro => pro.ven.city.ToLower().Contains(city.ToLower())).Select(pro => new
                        {
                            pro.pro.productId,
                            pro.pro.title,
                            pro.pro.approvedOn,
                            pro.pro.categoryId,
                            pro.pro.coverImg,
                            pro.pro.description,
                            pro.pro.fillingName,
                            pro.pro.isApproved,
                            pro.pro.keywords,
                            pro.pro.minimumTime,
                            pro.pro.perLayerCost,
                            pro.pro.persons,
                            pro.pro.price,
                            pro.pro.skuNo,
                            pro.pro.subCategoryId,
                            pro.pro.totalLayer,
                            pro.pro.vendorId,
                            pro.pro.dietryId,
                        }).OrderBy(pro => pro.minimumTime).ToList();

                        return Ok(new { status = "success", data = products });
                    }
                    else if (sortBy.ToLower() == "distance")
                    {
                        var products = db.products.Join(db.vendors, pro => pro.vendorId, ven => ven.vendorId, (pro, ven) => new { pro, ven }).Where(pro => pro.ven.city.ToLower().Contains(city)).Select(pro => new
                        {
                            pro.pro.productId,
                            pro.pro.title,
                            pro.pro.approvedOn,
                            pro.pro.categoryId,
                            pro.pro.coverImg,
                            pro.pro.description,
                            pro.pro.fillingName,
                            pro.pro.isApproved,
                            pro.pro.keywords,
                            pro.pro.minimumTime,
                            pro.pro.perLayerCost,
                            pro.pro.persons,
                            pro.pro.price,
                            pro.pro.skuNo,
                            pro.pro.subCategoryId,
                            pro.pro.totalLayer,
                            pro.pro.vendorId,
                            pro.pro.dietryId,
                            pro.ven.city
                        }).OrderBy(pro => pro.city == city).ToList();
                        return Ok(new { status = "success", data = products });

                    }
                    else if (sortBy.ToLower() == "top rated")
                    {
                        var mostsold = db.orderDetails.Join(db.vendors, ord => ord.vendorId, ven => ven.vendorId, (ord, ven) => new { ord, ven }).Where(ord => ord.ven.city.ToLower().Contains(city.ToLower()))

                               .GroupBy(q => q.ord.productId)
                               .OrderByDescending(gp => gp.Count())

                               .ToDictionary(g => g.Key, g => g.Count())
                               .ToList();
                        List<long> ids = new List<long>();
                        foreach (var item in mostsold)
                        {
                            ids.Add(item.Key.Value);
                        }
                        var products = db.products.Where(pro => ids.Contains(pro.productId)).Select(pro => new
                        {
                            pro.productId,
                            pro.title,
                            pro.approvedOn,
                            pro.categoryId,
                            pro.coverImg,
                            pro.description,
                            pro.fillingName,
                            pro.isApproved,
                            pro.keywords,
                            pro.minimumTime,
                            pro.perLayerCost,
                            pro.persons,
                            pro.price,
                            pro.skuNo,
                            pro.subCategoryId,
                            pro.totalLayer,
                            pro.vendorId,
                        }).ToList();



                        return Ok(new { status = "success", data = products });

                    }
                    else if (sortBy.ToLower() == "recommended")
                    {
                        var mostsold = db.oOrders.Join(db.vendors, ord => ord.vendorId, ven => ven.vendorId, (ord, ven) => new { ord, ven }).Where(ord => ord.ord.userId == userId && ord.ven.city.ToLower().Contains(city.ToLower()))
                                  .Select(ord => ord.ord.orderId)
                               //.GroupBy(q => q.ord.productId)
                               //.OrderByDescending(gp => gp.Count())
                               //.Take(5)
                               //.ToDictionary(g => g.Key, g => g.Count())
                               .ToList();

                        if (mostsold.Count() == 0)
                        {
                            var categories = db.categories.Select(cat => cat.categoryId).Take(4).ToList();
                            var randomProducts = db.products.Where(pro => categories.Contains((long)pro.categoryId)).Select(pro => new
                            {
                                pro.productId,
                                pro.title,
                                pro.approvedOn,
                                pro.categoryId,
                                pro.coverImg,
                                pro.description,
                                pro.fillingName,
                                pro.isApproved,
                                pro.keywords,
                                pro.minimumTime,
                                pro.perLayerCost,
                                pro.persons,
                                pro.price,
                                pro.skuNo,
                                pro.subCategoryId,
                                pro.totalLayer,
                                pro.vendorId,
                            }).ToList();


                            return Ok(new { status = "success", data = randomProducts });

                        }
                        var categoriesIds = db.orderDetails.Join(db.products, ord => ord.productId, pro => pro.productId, (ord, pro) => new { ord, pro }).Where(pro => mostsold.Contains((long)pro.ord.orderId)).Select(pro => pro.pro.categoryId).ToList();


                        var products = db.products.Where(pro => categoriesIds.Contains(pro.categoryId)).Select(pro => new
                        {
                            pro.productId,
                            pro.title,
                            pro.approvedOn,
                            pro.categoryId,
                            pro.coverImg,
                            pro.description,
                            pro.fillingName,
                            pro.isApproved,
                            pro.keywords,
                            pro.minimumTime,
                            pro.perLayerCost,
                            pro.persons,
                            pro.price,
                            pro.skuNo,
                            pro.subCategoryId,
                            pro.totalLayer,
                            pro.vendorId,
                        }).ToList();



                        return Ok(new { status = "success", data = products });

                    }
                    else
                    {
                        return Ok(new { status = "error", data = "something went wrong" });

                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = (7 + (dt.DayOfWeek - startOfWeek)) % 7;
            return dt.AddDays(-1 * diff).Date;
        }
        [HttpPost]
        public IHttpActionResult ChartData(ChartDataViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    DateTime Today = DateTime.Now;
                    DateTime from = StartOfWeek(Today, DayOfWeek.Monday);
                    DateTime to = StartOfWeek(Today, Today.DayOfWeek);

                    DateTime EmptyDate = new DateTime(0001, 01, 01);
                    var sales = new List<decimal>();
                    var mydays = new List<string>();
                    DateTime mytodate = vm.to.AddDays(1);
                    int days = (mytodate - vm.from).Days;
                    //int months = (mytodate - vm.from).Mon;
                    if (days <= 7 && vm.MonthOrWeek == null)
                    {
                        var dates = new List<DateTime>();
                        var mostsold = db.orderDetails
                                  .Where(v => v.vendorId == vm.vendorId && v.date >= vm.from && v.date <= vm.to)
                                  .GroupBy(q => q.categoryId)
                                  .OrderByDescending(gp => gp.Count())
                                  .Take(5)
                                  .ToDictionary(g => g.Key, g => g.Count())
                                  .ToList();


                        List<object> costlist = new List<object>();
                        foreach (var item in mostsold)
                        {
                            var total = db.orderDetails.Where(u => u.categoryId == item.Key && u.date >= vm.from && u.date <= vm.to).Sum(u => u.cost);
                            var categoryName = db.categories.Where(x => x.categoryId == item.Key).Select(x => x.Name).FirstOrDefault();
                            //var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            //    .Where(u => u.pro.productId == item.Key).Select(u => new {  u.cat.Name }).FirstOrDefault();
                            costlist.Add(new { Name = categoryName, totalCost = total });
                        }
                        for (var dt = vm.from; dt <= vm.to; dt = dt.AddDays(1))
                        {

                            dates.Add(dt);
                            //default locale
                            mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date == item).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);
                        }


                        return Ok(new { status = "success", sales, mydays, costlist });
                    }

                    else if (days > 7 && days <= 31)
                    {


                        int daysInMonth = DateTime.DaysInMonth(vm.from.Year, vm.from.Month);
                        DateTime firstOfMonth = new DateTime(vm.from.Year, vm.from.Month, 1);
                        DateTime lastOfMonth = new DateTime(vm.from.Year, vm.from.Month, daysInMonth);

                        //days of week starts by default as Sunday = 0
                        int firstDayOfMonth = (int)firstOfMonth.DayOfWeek;
                        int weeksInMonth = (int)Math.Ceiling((firstDayOfMonth + daysInMonth) / 7.0);
                        weeksInMonth = (weeksInMonth - 1);
                        var mostsold = db.orderDetails
                                .Where(v => v.vendorId == vm.vendorId && v.date >= firstOfMonth && v.date <= lastOfMonth)
                                .GroupBy(q => q.categoryId)
                                .OrderByDescending(gp => gp.Count())
                                .Take(5)
                                .ToDictionary(g => g.Key, g => g.Count())
                                .ToList();


                        List<object> costlist = new List<object>();
                        foreach (var item in mostsold)
                        {
                            var total = db.orderDetails.Where(u => u.categoryId == item.Key && u.date >= firstOfMonth && u.date <= lastOfMonth).Sum(u => u.cost);
                            var categoryName = db.categories.Where(x => x.categoryId == item.Key).Select(x => x.Name).FirstOrDefault();
                            //var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            //    .Where(u => u.pro.productId == item.Key).Select(u => new {  u.cat.Name }).FirstOrDefault();
                            costlist.Add(new { Name = categoryName, totalCost = total });
                        }

                        var dates = new List<DateViewModel>();
                        if (daysInMonth > 28)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();

                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    //mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));

                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to.AddDays(1);
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    //mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));


                                }

                            }
                            DateViewModel datevm2 = new DateViewModel();

                            datevm2.from = dates[3].to.AddDays(1);
                            datevm2.to = lastOfMonth;
                            dates.Add(datevm2);
                            int myindex = 4 + 1;
                            //mydays.Add("Week " + myindex);
                            mydays.Add(datevm2.from.ToString("dd MMM") + " - " + datevm2.to.ToString("dd MMM"));

                        }
                        else
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();
                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to.AddDays(1);
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);

                                }

                            }
                        }
                        foreach (var item in dates)
                        {
                            //int index = index + 1;
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);


                        }


                        return Ok(new { status = "success", sales, mydays, dates, costlist });


                    }

                    else if (days > 31)
                    {
                        int monthsApart = 12 * (vm.from.Year - vm.to.Year) + vm.from.Month - vm.to.Month;
                        monthsApart = Math.Abs(monthsApart);
                        var dates = new List<DateViewModel>();

                        DateTime firstOfMonth = new DateTime(vm.from.Year, vm.from.Month, 1);
                        var mostsold = db.orderDetails
                               .Where(v => v.vendorId == vm.vendorId && v.date >= vm.from && v.date <= vm.to)
                               .GroupBy(q => q.categoryId)
                               .OrderByDescending(gp => gp.Count())
                               .Take(5)
                               .ToDictionary(g => g.Key, g => g.Count())
                               .ToList();

                        List<object> costlist = new List<object>();
                        foreach (var item in mostsold)
                        {
                            var total = db.orderDetails.Where(u => u.categoryId == item.Key && u.date >= vm.from && u.date <= vm.to).Sum(u => u.cost);
                            var categoryName = db.categories.Where(x => x.categoryId == item.Key).Select(x => x.Name).FirstOrDefault();
                            //var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            //    .Where(u => u.pro.productId == item.Key).Select(u => new {  u.cat.Name }).FirstOrDefault();
                            costlist.Add(new { Name = categoryName, totalCost = total });
                        }

                        for (var i = 0; i <= monthsApart; i++)
                        {


                            DateViewModel datevm = new DateViewModel();
                            if (i == 0)
                            {
                                datevm.from = firstOfMonth;
                                datevm.to = firstOfMonth.AddMonths(1);
                                mydays.Add(firstOfMonth.ToString("MMM"));
                                dates.Add(datevm);
                            }
                            else
                            {
                                int a = i - 1;
                                datevm.from = dates[a].to.AddDays(1);
                                datevm.to = dates[a].to.AddMonths(1);
                                mydays.Add(dates[a].to.ToString("MMM"));
                                dates.Add(datevm);

                            }

                            //dates.Add(dt);
                            ////default locale
                            //mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);
                        }


                        return Ok(new { status = "success", sales, mydays, dates, costlist });
                    }
                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "week")
                    {


                        var dates = new List<DateTime>();
                        var mostsold = db.orderDetails
                            .Where(v => v.vendorId == vm.vendorId && v.date >= vm.from && v.date <= vm.to)
                            .GroupBy(q => q.categoryId)
                            .OrderByDescending(gp => gp.Count())
                            .Take(5)
                            .ToDictionary(g => g.Key, g => g.Count())
                            .ToList();

                        List<object> costlist = new List<object>();
                        foreach (var item in mostsold)
                        {
                            var total = db.orderDetails.Where(u => u.categoryId == item.Key && u.date >= vm.from && u.date <= vm.to).Sum(u => u.cost);
                            var categoryName = db.categories.Where(x => x.categoryId == item.Key).Select(x => x.Name).FirstOrDefault();
                            //var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            //    .Where(u => u.pro.productId == item.Key).Select(u => new {  u.cat.Name }).FirstOrDefault();
                            costlist.Add(new { Name = categoryName, totalCost = total });
                        }
                        for (var dt = from; dt <= to; dt = dt.AddDays(1))
                        {

                            dates.Add(dt);
                            //default locale
                            mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date == item).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);
                        }


                        return Ok(new { status = "success", sales, mydays });



                    }
                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "month")
                    {
                          
                        int daysInMonth = DateTime.DaysInMonth(Today.Year, Today.Month);
                        DateTime firstOfMonth = new DateTime(Today.Year, Today.Month, 1);
                        DateTime lastOfMonth = new DateTime(Today.Year, Today.Month, daysInMonth);

                        //days of week starts by default as Sunday = 0
                        int firstDayOfMonth = (int)firstOfMonth.DayOfWeek;
                        int weeksInMonth = (int)Math.Ceiling((firstDayOfMonth + daysInMonth) / 7.0);
                        weeksInMonth = (weeksInMonth - 1);
                        //Pie Chart
                        List<object> costlist = new List<object>();

                        try
                        {
                            var mostsold = db.orderDetails
                                        .Where(v => v.vendorId == vm.vendorId && v.date >= firstOfMonth && v.date <= lastOfMonth)
                                        .GroupBy(q => q.categoryId)
                                        .OrderByDescending(gp => gp.Count())
                                        .Take(5)
                                        .ToDictionary(g => g.Key, g => g.Count())
                                        .ToList();

                           
                            foreach (var item in mostsold)
                            {
                                var total = db.orderDetails.Where(u => u.categoryId == item.Key && u.date >= firstOfMonth && u.date <= lastOfMonth).Sum(u => u.cost);
                                var categoryName = db.categories.Where(x => x.categoryId == item.Key).Select(x => x.Name).FirstOrDefault();
                                //var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                                //    .Where(u => u.pro.productId == item.Key).Select(u => new {  u.cat.Name }).FirstOrDefault();
                                costlist.Add(new { Name = categoryName, totalCost = total });
                            }
                        }
                        catch (Exception x)
                        {
                          

                        }


                       

                        var dates = new List<DateViewModel>();
                        if (daysInMonth > 28)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();

                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    // mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));

                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to.AddDays(1);
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    //  mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));


                                }

                            }
                            DateViewModel datevm2 = new DateViewModel();

                            datevm2.from = dates[3].to.AddDays(1);
                            datevm2.to = lastOfMonth;
                            dates.Add(datevm2);
                            int myindex = 4 + 1;
                            // mydays.Add("Week " + myindex);
                            mydays.Add(datevm2.from.ToString("dd MMM") + " - " + datevm2.to.ToString("dd MMM"));

                            foreach (var item in dates)
                            {
                                var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                                decimal sum = (decimal)Bill.Sum(key => key.bill);
                                sales.Add(sum);
                            }
                            return Ok(new { status = "success", sales, mydays, costlist });


                        }
                        else
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();
                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to.AddDays(1);
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);

                                }

                            }
                        }
                        foreach (var item in dates)
                        {
                            //int index = index + 1;
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);


                        }
                        return Ok(new { status = "success", sales, mydays });


                    }

                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "3month")
                    {
                        DateTime dtPreviousDate = Today.AddMonths(-3);
                        DateTime firstOfMonth = new DateTime(dtPreviousDate.Year, dtPreviousDate.Month, 1);

                        var dates = new List<DateViewModel>();

                        for (var i = 0; i <= 2; i++)
                        {


                            DateViewModel datevm = new DateViewModel();
                            if (i == 0)
                            {
                                datevm.from = firstOfMonth;
                                datevm.to = firstOfMonth.AddMonths(1);
                                mydays.Add(firstOfMonth.ToString("MMM"));
                                dates.Add(datevm);
                            }
                            else
                            {
                                int a = i - 1;
                                datevm.from = dates[a].to.AddDays(1);
                                datevm.to = dates[a].to.AddMonths(1);
                                mydays.Add(dates[a].to.ToString("MMM"));
                                dates.Add(datevm);

                            }

                            //dates.Add(dt);
                            ////default locale
                            //mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);
                        }


                        return Ok(new { status = "success", sales, mydays, dates });
                    }
                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "year")
                    {
                        DateTime dtPreviousDate = Today.AddYears(-1);
                        DateTime firstOfMonth = new DateTime(dtPreviousDate.Year, 1, 1);

                        var dates = new List<DateViewModel>();

                        for (var i = 0; i <= 11; i++)
                        {


                            DateViewModel datevm = new DateViewModel();
                            if (i == 0)
                            {
                                datevm.from = firstOfMonth;
                                datevm.to = firstOfMonth.AddMonths(1);
                                mydays.Add(firstOfMonth.ToString("MMM"));
                                dates.Add(datevm);
                            }
                            else
                            {
                                int a = i - 1;
                                datevm.from = dates[a].to.AddDays(1);
                                datevm.to = dates[a].to.AddMonths(1);
                                mydays.Add(dates[a].to.ToString("MMM"));
                                dates.Add(datevm);

                            }

                            //dates.Add(dt);
                            ////default locale
                            //mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.vendorId == vm.vendorId && ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);
                        }


                        return Ok(new { status = "success", sales, mydays, dates });
                    }
                    return Ok(new { status = "success", data = days });

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public IHttpActionResult LoadData(DeviceViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var paymentInfo = db.paymentInfoes.FirstOrDefault();

                    List<address> RecentAddress = new List<address>();
                    object Ratting = new object();

                    List<object> recommendedProducts =  new List<object>();
                    if (vm.userid != 0 && vm.userid != null) 
                    {
                        var recProducts = db.orderDetails.Join(db.oOrders, det => det.orderId, ord => ord.orderId, (det, ord) => new
                        {
                            det,
                            ord
                        }).Join(db.vendors, det=> det.det.vendorId , ven=> ven.vendorId,(det,ven) => new { det, ven }).Where(x => x.det.ord.userId == vm.userid && x.ven.city.Contains(vm.city.ToLower()) ).Select(x => x.det.det.productId).Distinct().ToList();

                        var categoryIds = db.products.Where(x => recProducts.Contains(x.productId)).Select(x => x.categoryId).ToList();
                        var recommended = db.products.Where(x => categoryIds.Contains(x.categoryId)).Join(db.vendors, pro => pro.vendorId, ven => ven.vendorId, (pro, ven) => new { pro, ven }).Where(x=> x.ven.city.Contains(vm.city)).Select(pro => new
                        {
                            pro.pro.productId,
                            pro.pro.title,
                            pro.pro.approvedOn,
                            pro.pro.categoryId,
                            pro.pro.coverImg,
                            pro.pro.description,
                            pro.pro.fillingName,
                            pro.pro.isApproved,
                            pro.pro.keywords,
                            pro.pro.minimumTime,
                            pro.pro.perLayerCost,
                            pro.pro.persons,
                            pro.pro.price,
                            pro.pro.skuNo,
                            pro.pro.subCategoryId,
                            pro.pro.totalLayer,
                            pro.pro.vendorId,
                        }).Take(5).ToList();
                        recommendedProducts.AddRange(recommended);
                    }
                   

                    if (vm.userid != 0)
                    {
                        var recentOrder = db.oOrders.Where(ord => ord.userId == vm.userid).OrderByDescending(ord => ord.date).Select(pro => new
                        {
                            pro.addressId,
                        }).FirstOrDefault();

                        if (recentOrder != null)
                        {
                            var recentAddress = db.addresses.Where(ad => ad.addressId == recentOrder.addressId).FirstOrDefault();
                            RecentAddress.Add(recentAddress);
                        }


                        var recentOrderComplete = db.oOrders.Join(db.vendors, ord => ord.vendorId, ven => ven.vendorId, (ord, ven) => new { ord, ven }).Where(ord => ord.ord.userId == vm.userid
                      && ord.ord.status.ToLower() != "under progress" && ord.ord.status.ToLower() != "cancel" && ord.ord.status.ToLower() != "requires actions" && ord.ord.status.ToLower() != "reject").OrderByDescending(ord => ord.ord.date).Select(ord => new
                      {
                          ord.ord.orderId,
                          ord.ord.vendorId,
                          ord.ord.userId,
                          ord.ven.bakeryName,
                          ord.ven.logo,
                      }).FirstOrDefault();

                        Ratting = recentOrderComplete;
                        if (Ratting != null)
                        {
                            var rate = db.rattings.Where(rate => rate.orderId == recentOrderComplete.orderId).Select(rate => new
                            {
                                rate.rattingId,
                                rate.avgRatting,
                                rate.count,
                                rate.orderId,
                                rate.userId,
                                rate.vendorId
                            }).FirstOrDefault();

                            if (rate != null)
                            {
                                Ratting = null;
                            }

                        }

                    }
                    if (vm.userid == 0)
                    {
                        Ratting = null;
                    }
                    var dietries = db.dietries.Select(diet => new
                    {
                        diet.dietryId,
                        diet.name
                    }).ToList();

                    var occasions = db.occasions.Select(pro => new
                    {
                        pro.occasionId,
                        pro.title,
                        pro.image
                    }).ToList();


                    var needItToday = db.products.Join(db.vendors, pro => pro.vendorId, ven => ven.vendorId, (pro, ven) => new { pro, ven }).Where(pro => pro.pro.minimumTime <= 1 && pro.ven.city.Contains(vm.city)).Select(pro => new
                    {
                        pro.pro.productId,
                        pro.pro.title,
                        pro.pro.approvedOn,
                        pro.pro.categoryId,
                        pro.pro.coverImg,
                        pro.pro.description,
                        pro.pro.fillingName,
                        pro.pro.isApproved,
                        pro.pro.keywords,
                        pro.pro.minimumTime,
                        pro.pro.perLayerCost,
                        pro.pro.persons,
                        pro.pro.price,
                        pro.pro.skuNo,
                        pro.pro.subCategoryId,
                        pro.pro.totalLayer,
                        pro.pro.vendorId,
                    }).Take(5).ToList();
                    List<PreviousOrderViewModel> previousOrderList = new List<PreviousOrderViewModel>();
                    if (vm.userid != 0)
                    {
                        var previousOrders = db.oOrders.Where(ord => ord.userId == vm.userid && ord.status == "Ready for delivery").Select(ord => new
                        {
                            ord.orderId

                        }).Take(5).ToList();

                        foreach (var item in previousOrders)
                        {


                            PreviousOrderViewModel myvm = new PreviousOrderViewModel();
                            myvm.orderdetails = new List<object>();
                            myvm.products = new List<object>();
                            var Order = db.oOrders.Where(ord => ord.orderId == item.orderId).Select(ord => new
                            {
                                ord.orderId,

                                ord.bill,
                                ord.discount,
                                ord.modeOfpayment,
                                ord.vendorId,
                                ord.deliveryCharges,
                                ord.date,
                                ord.netBill,
                                ord.totalBill,
                                ord.status,
                                ord.vat,
                                ord.transactionId,
                                ord.url,
                                ord.promoCodeId,
                                ord.note,
                                ord.deliveryModeId,
                                ord.paidAmount,
                                ord.userId,
                            }).FirstOrDefault();

                            myvm.order = Order;

                            var OrderDetails = db.orderDetails.Where(ord => ord.orderId == item.orderId).Select(det => new
                            {
                                det.orderDetailsid,
                                det.addressId,
                                det.addressTitle,
                                det.cost,
                                det.deliverToMe,
                                det.deliveryMethod,
                                det.flavourId,
                                det.flavourName,
                                det.latitude,
                                det.layerId,
                                det.layerName,
                                det.longitude,
                                det.message,
                                det.address,
                                det.pickupTime,
                                det.productId,
                                det.productName,
                                det.contactNo,
                                det.name,
                                det.size,
                                det.sizeId,
                                det.streetNo,
                                det.note,
                                det.orderId,
                                detailStatus = det.status,
                                detailVendorId = det.vendorId
                            }).ToList();

                            myvm.orderdetails.AddRange(OrderDetails);

                            //    var Distinct = OrderDetails.Distinct()
                            List<long> pIds = new List<long>();
                            foreach (var item2 in OrderDetails)
                            {
                                pIds.Add((long)item2.productId);
                            }

                            var Products = db.products.Where(pro => pIds.Distinct().Contains(pro.productId)).Select(pro => new {
                                pro.productId,
                                pro.title,
                                pro.price,
                                pro.categoryId,
                                pro.description,
                                pro.size,
                                pro.vendorId,
                                pro.persons,
                                pro.weight,
                                pro.approvedOn,
                                pro.isApproved,
                                pro.coverImg,
                                pro.fillingName,
                                pro.minimumTime,
                                pro.perLayerCost,
                            }).ToList();




                            myvm.products.AddRange(Products);

                            previousOrderList.Add(myvm);

                        }
                    }


                    //.Join(db.products, occ => occ.occ.occasionId, pro => pro.occasionId, (occ, pro) => new
                    // {
                    //     occ,
                    //     pro
                    // })
                    var reminder = db.reminders.Join(db.occasions, remind => remind.occasionId, occ => occ.occasionId, (remind, occ) => new { remind, occ })
                       .Where(
                        remind => remind.remind.userId == vm.userid || remind.remind.deviceId == vm.DeviceId && remind.remind.date >= DateTime.Today).Select(remind => new
                        {
                            remind.remind.userId,
                            remind.remind.reminderId,
                            remind.remind.date,
                            remind.remind.deviceId,
                            remind.remind.notification,
                            remind.remind.occasionId,
                            remind.remind.remindDate,
                            remind.remind.reminderTitle,
                            remind.remind.occasionTitle,
                            remind.occ.image,
                            remind.occ.title

                        }).Take(5).ToList();

                    var mostsold = db.orderDetails.Join(db.vendors, ord => ord.vendorId, ven => ven.vendorId, (ord, ven) => new { ord, ven }).Where(ord => ord.ven.city.ToLower().Contains(vm.city.ToLower()) )

                                    .GroupBy(q => q.ord.productId)
                                    .OrderByDescending(gp => gp.Count())
                                    .Take(5)
                                    .ToDictionary(g => g.Key, g => g.Count())
                                    .ToList();
                    List<long> ids = new List<long>();
                    foreach (var item in mostsold)
                    {
                        ids.Add(item.Key.Value);
                    }
                    var mostSelling = db.products.Where(pro => ids.Contains(pro.productId)).Select(pro => new
                    {
                        pro.productId,
                        pro.title,
                        pro.approvedOn,
                        pro.categoryId,
                        pro.coverImg,
                        pro.description,
                        pro.fillingName,
                        pro.isApproved,
                        pro.keywords,
                        pro.minimumTime,
                        pro.perLayerCost,
                        pro.persons,
                        pro.price,
                        pro.skuNo,
                        pro.subCategoryId,
                        pro.totalLayer,
                        pro.vendorId,
                    }).ToList();

                    var banners = (from bn in db.banners
                                   where bn.validityDate >= DateTime.Today
                                   select new
                                   {
                                       bn.bannerId,
                                       bn.categoryId,
                                       bn.image,
                                       bn.validityDate
                                   }).ToList();

                    var categories = (from cat in db.categories
                                      select new
                                      {
                                          cat.categoryId,
                                          cat.productId,
                                          cat.Image,
                                          cat.Name
                                      }).ToList();
                    var subCategory = (from sub in db.subCategories
                                       join cat in db.categories
                                       on sub.categoryId equals cat.categoryId
                                       select new
                                       {
                                           sub.subCategoryId,
                                           sub.name,
                                           sub.image,
                                           sub.categoryId,
                                           cat.Name
                                       }).ToList();
                    var vendors = (from ven in db.vendors
                                   join us in db.users
                                   on ven.userId equals us.userId
                                   select new
                                   {
                                       ven.vendorId,
                                       ven.userId,
                                       ven.address,
                                       ven.bakeryName,
                                       ven.city,
                                       //cityList = ven.city.Split(','),
                                       ven.contactNo,
                                       ven.cover,
                                       ven.aboutUs,
                                       ven.latitude,
                                       ven.logo,
                                       ven.longitude,
                                       ven.productType,
                                       ven.suspendedTill,
                                       ven.url,
                                       ven.landlineNo,
                                       ven.licenseSource,
                                       us.status,

                                   }).OrderByDescending(us => us.status == "pending").ThenBy(us => us.status == "declined").ThenBy(us => us.status == "active").ThenBy(us => us.status == "approved").ToList();
                    var pickUpSlots = (from bn in db.pickupSlots
                                       select new
                                       {
                                           bn.pickupSlotsId,
                                           bn.day,
                                           bn.slot,
                                           bn.vendorId,
                                       }).ToList();

                    var row = GetLists();
                    var psProducts = (from pro in db.products
                                      where row.Contains(pro.productId)
                                      select new
                                      {
                                          pro.productId,
                                          pro.title,
                                          pro.price,
                                          pro.categoryId,
                                          pro.description,
                                          pro.size,
                                          pro.vendorId,
                                          pro.persons,
                                          pro.weight,
                                          pro.approvedOn,
                                          pro.isApproved,
                                          pro.coverImg,
                                          pro.minimumTime,
                                          pro.subCategoryId


                                      }).AsEnumerable().OrderBy(pro => row.IndexOf(pro.productId)).ToList();




                    var psCategory = (from bn in db.pSearches

                                      select new
                                      {
                                          bn.psId,
                                          bn.categoryId,
                                          bn.category

                                      }).ToList();

                    var addresses = (from ad in db.addresses
                                     where ad.userId == vm.userid || ad.deviceId == vm.DeviceId
                                     select new
                                     {
                                         ad.addressId,
                                         ad.customerId,
                                         ad.title,
                                         ad.date,
                                         ad.deviceId,
                                         ad.latitude,
                                         ad.longitude,
                                         ad.userId,
                                         ad.mapAddress,
                                         ad.mobilePhone,
                                         ad.name,
                                         ad.addressOne,
                                         ad.city
                                     }).ToList();

                    return Ok(new { paymentInfo, recommendedProducts, Ratting, dietries, RecentAddress, occasions, banners, previousOrderList, needItToday, mostSelling, categories, subCategory, vendors, pickUpSlots, psProducts, psCategory, addresses, reminder });

                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public IHttpActionResult NeedItToday(DeviceViewModel vm)
        {
            using (Entities db = new Entities())
            {
                var NeedItToday = db.products.Join(db.vendors, pro => pro.vendorId, ven => ven.vendorId, (pro, ven) => new { pro, ven }).Where(pro => pro.pro.minimumTime <= 1 && pro.ven.city.ToLower().Contains(vm.city.ToLower())).Select(pro => new
                {
                    pro.pro.productId,
                    pro.pro.title,
                    pro.pro.approvedOn,
                    pro.pro.categoryId,
                    pro.pro.coverImg,
                    pro.pro.description,
                    pro.pro.fillingName,
                    pro.pro.isApproved,
                    pro.pro.keywords,
                    pro.pro.minimumTime,
                    pro.pro.perLayerCost,
                    pro.pro.persons,
                    pro.pro.price,
                    pro.pro.skuNo,
                    pro.pro.subCategoryId,
                    pro.pro.totalLayer,
                    pro.pro.vendorId,
                }).Take(5).ToList();
                var wishLists = (
                           from ws in db.wishLists
                           where ws.userId == vm.userid || ws.deviceId == vm.DeviceId
                           join pro in db.products
                           on ws.productId equals pro.productId
                           select new
                           {
                               ws.wishId,
                               ws.productId,
                               ws.userId,
                               ws.deviceId,
                               pro.title,
                               pro.price,
                               pro.categoryId,
                               pro.description,
                               pro.size,
                               pro.vendorId,
                               pro.persons,
                               pro.weight,
                               pro.approvedOn,
                               pro.isApproved,
                               pro.coverImg,
                               pro.minimumTime,
                               pro.perLayerCost,
                           }).ToList();
                return Ok(new { status = "success", data = NeedItToday, wishLists });
            }
        }

        [HttpPost]
        public IHttpActionResult MostSelling(DeviceViewModel vm)
        {
            using (Entities db = new Entities())
            {
                var mostsold = db.orderDetails.Join(db.vendors, ord => ord.vendorId, ven => ven.vendorId, (ord, ven) => new { ord, ven }).Where(ord => ord.ven.city.ToLower().Contains(vm.city.ToLower()))

                                 .GroupBy(q => q.ord.productId)
                                 .OrderByDescending(gp => gp.Count())
                                 .ToDictionary(g => g.Key, g => g.Count())
                                 .ToList();
                List<long> ids = new List<long>();
                foreach (var item in mostsold)
                {
                    ids.Add(item.Key.Value);
                }
                var mostSelling = db.products.Where(pro => ids.Contains(pro.productId)).Select(pro => new
                {
                    pro.productId,
                    pro.title,
                    pro.approvedOn,
                    pro.categoryId,
                    pro.coverImg,
                    pro.description,
                    pro.fillingName,
                    pro.isApproved,
                    pro.keywords,
                    pro.minimumTime,
                    pro.perLayerCost,
                    pro.persons,
                    pro.price,
                    pro.skuNo,
                    pro.subCategoryId,
                    pro.totalLayer,
                    pro.vendorId,
                }).ToList();

                var wishLists = (
                             from ws in db.wishLists
                             where ws.userId == vm.userid || ws.deviceId == vm.DeviceId
                             join pro in db.products
                             on ws.productId equals pro.productId
                             select new
                             {
                                 ws.wishId,
                                 ws.productId,
                                 ws.userId,
                                 ws.deviceId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.perLayerCost,
                             }).ToList();

                return Ok(new { status = "success", data = mostSelling , wishLists });
            }
        }

        [HttpGet]
        public IHttpActionResult RecommendedProducts(long userId,string city)
        {
            using (Entities db = new Entities())
            {
                var recProducts = db.orderDetails.Join(db.oOrders, det => det.orderId, ord => ord.orderId, (det, ord) => new
                {
                    det,
                    ord
                }).Join(db.vendors, det => det.det.vendorId, ven => ven.vendorId, (det, ven) => new { det, ven }).Where(x => x.det.ord.userId == userId && x.ven.city.ToLower().Contains(city.ToLower())).Select(x => x.det.det.productId).Distinct().ToList();

                var categoryIds = db.products.Where(x => recProducts.Contains(x.productId)).Select(x => x.categoryId).ToList();
                var recommended = db.products.Where(x => categoryIds.Contains(x.categoryId)).Select(pro => new
                {
                    pro.productId,
                    pro.title,
                    pro.approvedOn,
                    pro.categoryId,
                    pro.coverImg,
                    pro.description,
                    pro.fillingName,
                    pro.isApproved,
                    pro.keywords,
                    pro.minimumTime,
                    pro.perLayerCost,
                    pro.persons,
                    pro.price,
                    pro.skuNo,
                    pro.subCategoryId,
                    pro.totalLayer,
                    pro.vendorId,
                }).ToList();
                return Ok(new { status = "success", data = recommended});
            }
        }
        [HttpGet]
        public IHttpActionResult TotalOrders(long id, DateTime fromdate, DateTime todate)
            {
            using (Entities db = new Entities())
            {
                DateTime EmptyDate = new DateTime(0001, 01, 01);
                try
                {
                    if (fromdate != EmptyDate && fromdate != EmptyDate)
                    {
                        DateTime mytodate = todate.AddDays(1);
                        var latestOrders = (from od in db.oOrders
                                            join us in db.users
                                            on od.userId equals us.userId
                                            where od.vendorId == id && od.date >= fromdate && od.date <= mytodate

                                            select new
                                            {
                                                od.orderId,
                                                us.userName,
                                                od.bill,
                                                od.date,
                                            }).OrderByDescending(od => od.date).Take(5).ToList();


                        var totalOrders = (from od in db.oOrders
                                           where od.vendorId == id && od.date >= fromdate && od.date <= mytodate
                                           select new
                                           {
                                               od
                                           }).Count();

                        var totalSales = (from od in db.oOrders
                                          where od.vendorId == id && od.date >= fromdate && od.date <= mytodate /*&& od.status == "completed"*/
                                          select new
                                          {
                                              od.bill
                                          });
                        var sum = totalSales.Sum(x => x.bill);

                        decimal completed = (from od in db.oOrders
                                             where od.vendorId == id && od.status == "completed" && od.date >= fromdate && od.date <= mytodate
                                             select new
                                             {
                                                 od
                                             }).Count();
                        decimal Ongoing = (from od in db.oOrders
                                           where od.vendorId == id && od.date >= fromdate && od.date <= mytodate && od.status != "completed"
                                           select new
                                           {
                                               od
                                           }).Count();
                        long readyForDelivery = (from od in db.oOrders
                                                 where od.vendorId == id && od.status == "Ready for delivery"
                                                 select new
                                                 {
                                                     od
                                                 }).Count();
                        long requiresActions = (from od in db.oOrders
                                                where od.vendorId == id && od.status == "Requires actions"
                                                select new
                                                {
                                                    od
                                                }).Count();
                        long underProgress = (from od in db.oOrders
                                              where od.vendorId == id && od.status == "Under progress"
                                              select new
                                              {
                                                  od
                                              }).Count();
                        decimal percentage;
                        if (completed != 0)
                        {
                            percentage = ((completed / Ongoing) * 100);
                            percentage = Math.Round(percentage, 1);
                        }
                        else
                        {
                            percentage = 0;
                        }
                        var goal = new { Ongoing, completed, percentage };


                        return Ok(new { status = "success", totalOrders, sum, goal, latestOrders });
                    }
                    else
                    {
                        var latestOrders = (from od in db.oOrders
                                            join us in db.users
                                            on od.userId equals us.userId
                                            where od.vendorId == id
                                            select new
                                            {
                                                od.orderId,
                                                us.userName,
                                                od.bill,
                                                od.date,
                                            }).OrderByDescending(od => od.date).Take(5).ToList();


                        var totalOrders = (from od in db.oOrders
                                           where od.vendorId == id
                                           select new
                                           {
                                               od
                                           }).Count();

                        var totalSales = (from od in db.oOrders
                                          where od.vendorId == id /*&& od.status == "completed"*/
                                          select new
                                          {
                                              od.bill
                                          });
                        var totalProducts = db.products.Where(x => x.vendorId == id).Count();
                        var mostsold = db.orderDetails.Join(db.vendors, ord => ord.vendorId, ven => ven.vendorId, (ord, ven) => new { ord, ven }).Where(ord => ord.ven.vendorId == id)
                                        .GroupBy(q => q.ord.productId)
                                        .OrderByDescending(gp => gp.Count())
                                        .ToDictionary(g => g.Key, g => g.Count())
                                        .ToList().First();
                        var mostsoldName = db.products.Where(x => x.productId == mostsold.Key).Select(x => x.title).SingleOrDefault();
                        var sum = totalSales.Sum(x => x.bill);

                        decimal total = (from od in db.oOrders
                                         where od.vendorId == id
                                         select new
                                         {
                                             od
                                         }).Count();

                        decimal completed = (from od in db.oOrders
                                             where od.vendorId == id && od.status == "Ready for delivery"
                                             select new
                                             {
                                                 od
                                             }).Count();
                        decimal Ongoing = (from od in db.oOrders
                                           where od.vendorId == id && od.status != "Ready for delivery"
                                           select new
                                           {
                                               od
                                           }).Count();

                        long readyForDelivery = (from od in db.oOrders
                                                 where od.vendorId == id && od.status == "Ready for delivery"
                                                 select new
                                                 {
                                                     od
                                                 }).Count();
                        long requiresActions = (from od in db.oOrders
                                                where od.vendorId == id && od.status == "Requires actions"
                                                select new
                                                {
                                                    od
                                                }).Count();
                        long underProgress = (from od in db.oOrders
                                              where od.vendorId == id && od.status == "Under progress"
                                              select new
                                              {
                                                  od
                                              }).Count();
                        //RequireAction Percentage
                        decimal percentage1;
                        if (requiresActions != 0)
                        {
                            percentage1 = ((requiresActions / total) * 100);
                            percentage1 = Math.Round(percentage1, 1);
                        }
                        else
                        {
                            percentage1 = 0;
                        }

                        //underProgress Percentage
                        decimal percentage2;
                        if (underProgress != 0)
                        {
                            percentage2 = ((underProgress / total) * 100);
                            percentage2 = Math.Round(percentage2, 1);
                        }
                        else
                        {
                            percentage2 = 0;
                        }

                        //readyForDelivery Percentage
                        decimal percentage3;
                        if (readyForDelivery != 0)
                        {
                            percentage3 = ((readyForDelivery / total) * 100);
                            percentage3 = Math.Round(percentage3, 1);
                        }
                        else
                        {
                            percentage3 = 0;
                        }

                        decimal percentage;
                        if (completed != 0)
                        {
                            percentage = ((completed / Ongoing) * 100);
                            percentage = Math.Round(percentage, 1);
                        }
                        else
                        {
                            percentage = 0;
                        }
                        var goal = new { Ongoing, completed, percentage };
                        List<decimal> p1arr = new List<decimal>() ;
                        List<decimal> p2arr =  new List<decimal>();
                        List<decimal> p3arr = new List<decimal>();
                        p1arr.Add(percentage1);
                        p2arr.Add(percentage2);
                        p3arr.Add(percentage3);
                        var orderTracker = new { p1arr, p2arr, p3arr, readyForDelivery, requiresActions, underProgress };

                        return Ok(new { status = "success", totalOrders, sum, goal, latestOrders, readyForDelivery, requiresActions, underProgress, mostsoldName, totalProducts, orderTracker });
                    }
                }
                catch (Exception)
                {

                    throw;
                }
              


            }
        }
        [HttpGet]
        public IHttpActionResult Status(long id)
        {
            using (Entities db = new Entities())
            {
                long readyForDelivery = (from od in db.oOrders
                                         where od.vendorId == id && od.status == "Ready for delivery"
                                         select new
                                         {
                                             od
                                         }).Count();
                long requiresActions = (from od in db.oOrders
                                        where od.vendorId == id && od.status == "Requires actions"
                                        select new
                                        {
                                            od
                                        }).Count();
                long underProgress = (from od in db.oOrders
                                      where od.vendorId == id && od.status == "Under progress"
                                      select new
                                      {
                                          od
                                      }).Count();
                return Ok(new { status = "success", requiresActions, readyForDelivery, underProgress });
            }
        }

        //}
        //[HttpGet]
        //public IHttpActionResult TotalNoOrders(long id)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        var completed = (from od in db.oOrders
        //                         where od.vendorId == id && od.status == "completed"
        //                         select new
        //                         {
        //                             od
        //                         }).Count();
        //        var Ongoing = (from od in db.oOrders
        //                       where od.vendorId == id && od.status == "On going"
        //                       select new
        //                       {
        //                           od
        //                       }).Count();
        //        return Ok(new { completed, Ongoing });
        //    }

        //}
        ////[HttpGet]
        ////public IHttpActionResult MostLovedCustomers(long id)
        ////{
        ////    using (Entities db = new Entities())
        ////    {


        ////        return Ok(new { data, mostrepeated });
        ////    }
        ////}

        [HttpGet]
        public IHttpActionResult productsAndCustomers(long id, DateTime fromdate, DateTime todate)
        {
            using (Entities db = new Entities())
            {
                DateTime EmptyDate = new DateTime(0001, 01, 01);
                DateTime mytodate = todate.AddDays(1);

                if (fromdate != EmptyDate && fromdate != EmptyDate)
                {
                    var mostsold = db.orderDetails
                                 .Where(v => v.vendorId == id && v.date >= fromdate && v.date <= mytodate)
                                 .GroupBy(q => q.productId)
                                 .OrderByDescending(gp => gp.Count())
                                 .Take(5)
                                 .ToDictionary(g => g.Key, g => g.Count())
                                 .ToList();


                    //List<long> mylist = new List<long>();
                    //for (int i = 0; i < mostsold.Count(); i++)
                    //{
                    //    mylist.Add(mostsold[i].Key.Value);
                    //}


                    //var categoriesIds = db.categories.Select(x => x.productId).ToArray();

                    //var products = (from pro in db.products.AsNoTracking()
                    //                join cat in db.categories
                    //                on pro.categoryId equals cat.categoryId
                    //                where mylist.Contains(pro.productId)
                    //                select new
                    //                {

                    //                    pro.productId,

                    //                }).ToList().OrderBy(m => mylist.IndexOf(m.productId));


                    //   var result = products.Where(p => mostsold.All(p2 => p2.pro != p.ID));

                    //var totalRevenue = db.orderDetails.AsNoTracking().Where(m => mylist.Contains((long)m.productId)).Select(m=> new { m.productId, m.productName ,m.cost } )
                    //    .ToList().OrderBy(m => mylist.IndexOf((long)m.productId));
                    List<object> costlist = new List<object>();
                    foreach (var item in mostsold)
                    {
                        var total = db.orderDetails.Where(u => u.productId == item.Key).Sum(u => u.cost);
                        var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            .Where(u => u.pro.productId == item.Key).Select(u => new { u.pro.title, u.pro.coverImg, u.cat.Name }).FirstOrDefault();
                        costlist.Add(new { productId = item.Key, title = myproduct.title, coverImg = myproduct.coverImg, Name = myproduct.Name, totalCost = total });
                    }



                    var mostrepeated = db.oOrders.AsNoTracking()

                                        .Where(v => v.vendorId == id && v.date >= fromdate && v.date <= mytodate)
                                       .GroupBy(q => q.userId)

                                       .OrderByDescending(gp => gp.Count())
                                       .Take(6)

                                       .ToDictionary(g => g.Key, g => g.Count()).ToList();



                    List<object> customers = new List<object>();
                    foreach (var item in mostrepeated)
                    {
                        var user = db.users.Where(u => u.userId == item.Key).Select(u => new { u.userName, u.mobileno }).FirstOrDefault();
                        var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            .Where(u => u.pro.productId == item.Key).Select(u => new { u.pro.title, u.pro.coverImg, u.cat.Name }).FirstOrDefault();
                        customers.Add(new { userId = item.Key, times = item.Value, userName = user.userName, mobileno = user.mobileno, });
                    }

                    var newCustomers = (from od in db.oOrders.AsNoTracking()
                                        join us in db.users
                                        on od.userId equals us.userId
                                        where od.vendorId == id && od.date >= fromdate && od.date <= mytodate
                                        select new
                                        {
                                            od.orderId,
                                            us.userId,
                                            us.userName,

                                        }).Distinct().Take(4).OrderByDescending(m => m.orderId).ToList();

                    // for (int i = 0; i < mostrepeated.Count(); i++)
                    // {
                    //     mycuslist.Add(mostrepeated[i].Key.Value);
                    // }

                    // var customers = db.users.AsNoTracking()
                    // // Add a criteria that we only want the known ids
                    // .Where(t => mycuslist.Contains(t.userId))
                    // // Anything after this is done in-memory instead of by the database
                    //.AsEnumerable()
                    // // Sort the results, in-memory
                    // .OrderBy(m => mycuslist.IndexOf(m.userId))
                    // // Materialize into a list
                    // .ToList();

                    var repeatedCustomers = new { customers };
                    var mostSoldProducts = new { costlist };
                    return Ok(new { status = "success", mostSoldProducts, repeatedCustomers, newCustomers });

                }

                else
                {
                    var mostsold = db.orderDetails

                                      .Where(v => v.vendorId == id)
                                      .GroupBy(q => q.productId)
                                      .OrderByDescending(gp => gp.Count())
                                      .Take(5)
                                      .ToDictionary(g => g.Key, g => g.Count())
                                      .ToList();


                    List<object> costlist = new List<object>();
                    foreach (var item in mostsold)
                    {
                        var total = db.orderDetails.Where(u => u.productId == item.Key).Sum(u => u.cost);
                        var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            .Where(u => u.pro.productId == item.Key).Select(u => new { u.pro.title, u.pro.coverImg, u.cat.Name }).FirstOrDefault();
                        costlist.Add(new { productId = item.Key, title = myproduct.title, coverImg = myproduct.coverImg, Name = myproduct.Name, totalCost = total });
                    }



                    var mostrepeated = db.oOrders.AsNoTracking()

                                        .Where(v => v.vendorId == id)
                                       .GroupBy(q => q.userId)

                                       .OrderByDescending(gp => gp.Count())
                                       .Take(6)

                                       .ToDictionary(g => g.Key, g => g.Count()).ToList();



                    List<object> customers = new List<object>();
                    foreach (var item in mostrepeated)
                    {
                        var user = db.users.Where(u => u.userId == item.Key).Select(u => new { u.userName, u.mobileno }).FirstOrDefault();
                        var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            .Where(u => u.pro.productId == item.Key).Select(u => new { u.pro.title, u.pro.coverImg, u.cat.Name }).FirstOrDefault();
                        customers.Add(new { userId = item.Key, times = item.Value, userName = user.userName, mobileno = user.mobileno, });
                    }

                    var newCustomers = (from od in db.oOrders.AsNoTracking()
                                        join det in db.orderDetails
                                        on od.orderId equals det.orderId
                                        where od.vendorId == id
                                        select new
                                        {
                                            od.orderId,
                                            det.name,
                                            det.address,

                                        }).Distinct().Take(4).OrderByDescending(m => m.orderId).ToList();



                    var repeatedCustomers = new { customers };
                    var mostSoldProducts = new { costlist };
                    return Ok(new { status = "success", mostSoldProducts, repeatedCustomers, newCustomers });

                }


            }
        }

        [HttpPost]
        public IHttpActionResult AdminDashBoard(AdminDashBoardViewModel vm)
        {
            using (Entities db = new Entities())
            {
                DateTime EmptyDate = new DateTime(0001, 01, 01);

                if (vm.from != EmptyDate && vm.from != EmptyDate)
                {
                    DateTime mytodate = vm.to.AddDays(1);
                    var latestOrders = (from od in db.oOrders
                                        join us in db.users
                                        on od.userId equals us.userId
                                        where od.date >= vm.@from && od.date <= mytodate

                                        select new
                                        {
                                            od.orderId,
                                            us.userName,
                                            od.bill,
                                            od.date,
                                        }).OrderByDescending(od => od.date).Take(5).ToList();


                    var totalOrders = (from od in db.oOrders
                                       where od.date >= vm.@from && od.date <= mytodate
                                       select new
                                       {
                                           od
                                       }).Count();

                    var totalSales = (from od in db.oOrders
                                      where od.date >= vm.@from && od.date <= mytodate /*&& od.status == "completed"*/
                                      select new
                                      {
                                          od.bill
                                      });
                    var sum = totalSales.Sum(x => x.bill);

                    decimal completed = (from od in db.oOrders
                                         where od.status.ToLower() == "ready for delivery" && od.date >= vm.@from && od.date <= mytodate
                                         select new
                                         {
                                             od
                                         }).Count();
                    decimal Ongoing = (from od in db.oOrders
                                       where od.date >= vm.@from && od.date <= mytodate && od.status.ToLower() != "ready for delivery"
                                       select new
                                       {
                                           od
                                       }).Count();
                    long readyForDelivery = (from od in db.oOrders
                                             where od.status == "Ready for delivery"
                                             select new
                                             {
                                                 od
                                             }).Count();
                    long requiresActions = (from od in db.oOrders
                                            where od.status == "Requires actions"
                                            select new
                                            {
                                                od
                                            }).Count();
                    long underProgress = (from od in db.oOrders
                                          where od.status == "Under progress"
                                          select new
                                          {
                                              od
                                          }).Count();
                    decimal percentage;
                    if (completed != 0)
                    {
                        percentage = ((completed / Ongoing) * 100);
                        percentage = Math.Round(percentage, 1);
                    }
                    else
                    {
                        percentage = 0;
                    }
                    var goal = new { Ongoing, completed, percentage };

                    var mostsold = db.orderDetails

                                 .Where(v => v.date >= vm.from && v.date <= mytodate)
                                 .GroupBy(q => q.productId)
                                 .OrderByDescending(gp => gp.Count())
                                 .Take(5)
                                 .ToDictionary(g => g.Key, g => g.Count())
                                 .ToList();


                    List<object> costlist = new List<object>();
                    foreach (var item in mostsold)
                    {
                        var total = db.orderDetails.Where(u => u.productId == item.Key).Sum(u => u.cost);
                        var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            .Where(u => u.pro.productId == item.Key).Select(u => new { u.pro.title, u.pro.coverImg, u.cat.Name }).FirstOrDefault();
                        costlist.Add(new { productId = item.Key, title = myproduct.title, coverImg = myproduct.coverImg, Name = myproduct.Name, totalCost = total });
                    }




                    var newCustomers = (from od in db.oOrders.AsNoTracking()
                                        join us in db.users
                                        on od.userId equals us.userId
                                        where od.date >= vm.@from && od.date <= mytodate
                                        select new
                                        {
                                            od.orderId,
                                            us.userId,
                                            us.userName,

                                        }).Distinct().Take(4).OrderByDescending(m => m.orderId).ToList();


                    var mostSoldProducts = new { costlist };





                    return Ok(new { status = "success", totalOrders, sum, goal, latestOrders, mostSoldProducts, newCustomers });
                }
                else
                {
                    var latestOrders = (from od in db.oOrders
                                        join us in db.users
                                        on od.userId equals us.userId
                                        select new
                                        {
                                            od.orderId,
                                            us.userName,
                                            od.bill,
                                            od.date,
                                        }).OrderByDescending(od => od.orderId).Take(5).ToList();


                    var totalOrders = (from od in db.oOrders
                                       select new
                                       {
                                           od
                                       }).Count();

                    var totalSales = (from od in db.oOrders
                                      select new
                                      {
                                          od.bill
                                      });
                    var sum = totalSales.Sum(x => x.bill);

                    decimal completed = (from od in db.oOrders
                                         where od.status.ToLower() == "ready for delivery"
                                         select new
                                         {
                                             od
                                         }).Count();
                    decimal Ongoing = (from od in db.oOrders 
                                       where od.status.ToLower() != "ready for delivery" 
                                       select new
                                       {
                                           od
                                       }).Count();

                    long readyForDelivery = (from od in db.oOrders
                                             where od.status == "Ready for delivery"
                                             select new
                                             {
                                                 od
                                             }).Count();
                    long requiresActions = (from od in db.oOrders
                                            where od.status == "Requires actions"
                                            select new
                                            {
                                                od
                                            }).Count();
                    long underProgress = (from od in db.oOrders
                                          where od.status == "Under progress"
                                          select new
                                          {
                                              od
                                          }).Count();

                    decimal percentage;
                    if (completed != 0)
                    {
                        percentage = ((completed / Ongoing) * 100);
                        percentage = Math.Round(percentage, 1);
                    }
                    else
                    {
                        percentage = 0;
                    }
                    var goal = new { Ongoing, completed, percentage };


                    var mostsold = db.orderDetails


                                     .GroupBy(q => q.productId)
                                     .OrderByDescending(gp => gp.Count())
                                     .Take(5)
                                     .ToDictionary(g => g.Key, g => g.Count())
                                     .ToList();


                    List<object> costlist = new List<object>();
                    foreach (var item in mostsold)
                    {
                        var total = db.orderDetails.Where(u => u.productId == item.Key).Sum(u => u.cost);
                        var myproduct = db.products.Join(db.categories, pro => pro.categoryId, cat => cat.categoryId, (pro, cat) => new { pro, cat })
                            .Where(u => u.pro.productId == item.Key).Select(u => new { u.pro.title, u.pro.coverImg, u.cat.Name }).FirstOrDefault();
                        costlist.Add(new { productId = item.Key, title = myproduct.title, coverImg = myproduct.coverImg, Name = myproduct.Name, totalCost = total });
                    }





                    var newCustomers = (from od in db.oOrders.AsNoTracking()
                                        join det in db.orderDetails
                                        on od.orderId equals det.orderId

                                        select new
                                        {
                                            od.orderId,
                                            det.name,
                                            det.address,

                                        }).Distinct().Take(4).OrderByDescending(m => m.orderId).ToList();



                    // var repeatedCustomers = new { customers };
                    var mostSoldProducts = new { costlist };


                    return Ok(new { status = "success", totalOrders, sum, goal, latestOrders, readyForDelivery, requiresActions, underProgress, mostSoldProducts, newCustomers });
                }

            }

        }

        [HttpPost]
        public IHttpActionResult AdminChartData(AdminChartDataViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    decimal percent = db.discountPercents.Select(x => x.discount).FirstOrDefault();

                    DateTime Today = DateTime.Now;
                    DateTime from = StartOfWeek(Today, DayOfWeek.Monday);
                    DateTime to = StartOfWeek(Today, Today.DayOfWeek);

                    DateTime EmptyDate = new DateTime(0001, 01, 01);
                    var sales = new List<decimal>();
                    var mydays = new List<string>();
                    DateTime mytodate = vm.to.AddDays(1);
                    int days = (mytodate - vm.from).Days;

                    //int months = (mytodate - vm.from).Mon;
                    if (days <= 7 && vm.MonthOrWeek == null)
                    {
                        var dates = new List<DateTime>();

                        for (var dt = vm.from; dt <= vm.to; dt = dt.AddDays(1))
                        {

                            dates.Add(dt);
                            //default locale
                            mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.date == item).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            //(int)Math.Round((double)(100 * 15) / sum);
                            //decimal off = 0.15M;
                            decimal percentage = percent * sum;
                            percentage = Math.Round(percentage, 2);
                            sales.Add(percentage);
                        }


                        return Ok(new { status = "success", sales, mydays });
                    }

                    else if (days > 7 && days <= 31)
                    {


                        int daysInMonth = DateTime.DaysInMonth(vm.from.Year, vm.from.Month);
                        DateTime firstOfMonth = new DateTime(vm.from.Year, vm.from.Month, 1);
                        DateTime lastOfMonth = new DateTime(vm.from.Year, vm.from.Month, daysInMonth);

                        //days of week starts by default as Sunday = 0
                        int firstDayOfMonth = (int)firstOfMonth.DayOfWeek;
                        int weeksInMonth = (int)Math.Ceiling((firstDayOfMonth + daysInMonth) / 7.0);
                        weeksInMonth = (weeksInMonth - 1);


                        var dates = new List<DateViewModel>();
                        if (daysInMonth > 28)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();

                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    //mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));

                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to;
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    //mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));


                                }

                            }
                            DateViewModel datevm2 = new DateViewModel();

                            datevm2.from = dates[3].to;
                            datevm2.to = lastOfMonth;
                            dates.Add(datevm2);
                            int myindex = 4 + 1;
                            //mydays.Add("Week " + myindex);
                            mydays.Add(datevm2.from.ToString("dd MMM") + " - " + datevm2.to.ToString("dd MMM"));

                        }
                        else
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();
                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to;
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);

                                }

                            }
                        }
                        foreach (var item in dates)
                        {
                            //int index = index + 1;
                            var Bill = db.oOrders.Where(ord => ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            decimal percentage = percent * sum;
                            percentage = Math.Round(percentage, 2);
                            sales.Add(percentage);


                        }


                        return Ok(new { status = "success", sales, mydays, dates });


                    }

                    else if (days > 31)
                    {
                        int monthsApart = 12 * (vm.from.Year - vm.to.Year) + vm.from.Month - vm.to.Month;
                        monthsApart = Math.Abs(monthsApart);
                        var dates = new List<DateViewModel>();

                        DateTime firstOfMonth = new DateTime(vm.from.Year, vm.from.Month, 1);

                        for (var i = 0; i <= monthsApart; i++)
                        {


                            DateViewModel datevm = new DateViewModel();
                            if (i == 0)
                            {
                                datevm.from = firstOfMonth;
                                datevm.to = firstOfMonth.AddMonths(1);
                                mydays.Add(firstOfMonth.ToString("MMM"));
                                dates.Add(datevm);
                            }
                            else
                            {
                                int a = i - 1;
                                datevm.from = dates[a].to;
                                datevm.to = dates[a].to.AddMonths(1);
                                mydays.Add(dates[a].to.ToString("MMM"));
                                dates.Add(datevm);

                            }

                            //dates.Add(dt);
                            ////default locale
                            //mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            sales.Add(sum);
                        }


                        return Ok(new { status = "success", sales, mydays, dates });
                    }
                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "week")
                    {


                        var dates = new List<DateTime>();

                        for (var dt = from; dt <= to; dt = dt.AddDays(1))
                        {

                            dates.Add(dt);
                            //default locale
                            mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.date == item).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            decimal percentage = percent * sum;
                            percentage = Math.Round(percentage, 2);
                            sales.Add(percentage);
                        }


                        return Ok(new { status = "success", sales, mydays });



                    }
                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "month")
                    {

                        int daysInMonth = DateTime.DaysInMonth(Today.Year, Today.Month);
                        DateTime firstOfMonth = new DateTime(Today.Year, Today.Month, 1);
                        DateTime lastOfMonth = new DateTime(Today.Year, Today.Month, daysInMonth);

                        //days of week starts by default as Sunday = 0
                        int firstDayOfMonth = (int)firstOfMonth.DayOfWeek;
                        int weeksInMonth = (int)Math.Ceiling((firstDayOfMonth + daysInMonth) / 7.0);
                        weeksInMonth = (weeksInMonth - 1);


                        var dates = new List<DateViewModel>();
                        if (daysInMonth > 28)
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();

                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    // mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));

                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to;
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);
                                    int index = i + 1;
                                    //  mydays.Add("Week " + index);
                                    mydays.Add(datevm.from.ToString("dd MMM") + " - " + datevm.to.ToString("dd MMM"));


                                }

                            }
                            DateViewModel datevm2 = new DateViewModel();

                            datevm2.from = dates[3].to;
                            datevm2.to = lastOfMonth;
                            dates.Add(datevm2);
                            int myindex = 4 + 1;
                            // mydays.Add("Week " + myindex);
                            mydays.Add(datevm2.from.ToString("dd MMM") + " - " + datevm2.to.ToString("dd MMM"));

                            foreach (var item in dates)
                            {
                                var Bill = db.oOrders.Where(ord => ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                                decimal sum = (decimal)Bill.Sum(key => key.bill);
                                decimal percentage = percent * sum;
                                percentage = Math.Round(percentage, 2);
                                sales.Add(percentage);
                            }
                            return Ok(new { status = "success", sales, mydays });


                        }
                        else
                        {
                            for (int i = 0; i < 4; i++)
                            {
                                DateViewModel datevm = new DateViewModel();
                                if (i == 0)
                                {
                                    datevm.from = firstOfMonth;
                                    datevm.to = firstOfMonth.AddDays(7);
                                    dates.Add(datevm);
                                }
                                else
                                {
                                    int a = i - 1;
                                    datevm.from = dates[a].to;
                                    datevm.to = dates[a].to.AddDays(7);
                                    dates.Add(datevm);

                                }

                            }
                        }
                        foreach (var item in dates)
                        {
                            //int index = index + 1;
                            var Bill = db.oOrders.Where(ord => ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            decimal percentage = percent * sum;
                            percentage = Math.Round(percentage, 2);
                            sales.Add(percentage);


                        }
                        return Ok(new { status = "success", sales, mydays });


                    }

                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "3month")
                    {
                        DateTime dtPreviousDate = Today.AddMonths(-3);
                        DateTime firstOfMonth = new DateTime(dtPreviousDate.Year, dtPreviousDate.Month, 1);

                        var dates = new List<DateViewModel>();

                        for (var i = 0; i <= 2; i++)
                        {


                            DateViewModel datevm = new DateViewModel();
                            if (i == 0)
                            {
                                datevm.from = firstOfMonth;
                                datevm.to = firstOfMonth.AddMonths(1);
                                mydays.Add(firstOfMonth.ToString("MMM"));
                                dates.Add(datevm);
                            }
                            else
                            {
                                int a = i - 1;
                                datevm.from = dates[a].to;
                                datevm.to = dates[a].to.AddMonths(1);
                                mydays.Add(dates[a].to.ToString("MMM"));
                                dates.Add(datevm);

                            }

                            //dates.Add(dt);
                            ////default locale
                            //mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            decimal percentage = percent * sum;
                            percentage = Math.Round(percentage, 2);
                            sales.Add(percentage);
                        }


                        return Ok(new { status = "success", sales, mydays, dates });
                    }
                    else if (vm.from == EmptyDate && vm.to == EmptyDate && vm.MonthOrWeek == "year")
                    {
                        DateTime dtPreviousDate = Today.AddYears(-1);
                        DateTime firstOfMonth = new DateTime(dtPreviousDate.Year, 1, 1);

                        var dates = new List<DateViewModel>();

                        for (var i = 0; i <= 11; i++)
                        {


                            DateViewModel datevm = new DateViewModel();
                            if (i == 0)
                            {
                                datevm.from = firstOfMonth;
                                datevm.to = firstOfMonth.AddMonths(1);
                                mydays.Add(firstOfMonth.ToString("MMM"));
                                dates.Add(datevm);
                            }
                            else
                            {
                                int a = i - 1;
                                datevm.from = dates[a].to;
                                datevm.to = dates[a].to.AddMonths(1);
                                mydays.Add(dates[a].to.ToString("MMM"));
                                dates.Add(datevm);

                            }

                            //dates.Add(dt);
                            ////default locale
                            //mydays.Add(dt.DayOfWeek.ToString());
                            //localized version
                            //  System.DateTime.Now.ToString("dddd");
                        }
                        foreach (var item in dates)
                        {
                            var Bill = db.oOrders.Where(ord => ord.date >= item.from && ord.date <= item.to).Select(ord => new { ord.bill }).ToList();
                            decimal sum = (decimal)Bill.Sum(key => key.bill);
                            decimal percentage = percent * sum;
                            percentage = Math.Round(percentage, 2);
                            sales.Add(percentage);
                        }


                        return Ok(new { status = "success", sales, mydays, dates });
                    }
                    return Ok(new { status = "success", data = days });

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
