﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class vendordocsController : ApiController
    {

        // GET: api/vendordocs
        [HttpGet]
        [Route("api/vendordocs/getVendordocs")]

        public IHttpActionResult Getvendordocs()
        {
            using (Entities db = new Entities())
            {

                var query = (from vndoc in db.vendordocs
                             select new
                             {
                                 vndoc.vendorDocId,
                                 vndoc.vendorId,
                                 vndoc.documentPath,
                                 vndoc.docType,
                             }).ToList();


                return Ok(query);


            }
        }

        // GET: api/vendordocs/5
        [ResponseType(typeof(vendordoc))]
        [HttpGet]
        [Route("api/vendordocs/getVendordoc/{id}")]
        public IHttpActionResult Getvendordoc(long id)
        {
            using (Entities db = new Entities())
            {

                var query = (from vndoc in db.vendordocs
                             where vndoc.vendorDocId == id
                             select new
                             {
                                 vndoc.vendorDocId,
                                 vndoc.vendorId,
                                 vndoc.documentPath,
                                 vndoc.docType,
                             }).ToList();


                return Ok(query);


            }
        }

        // PUT: api/vendordocs/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/vendordocs/updateVendordoc/{id}")]
        public IHttpActionResult Putvendordoc(long id, vendordoc vendordoc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != vendordoc.vendorDocId)
                {
                    return BadRequest();
                }

                db.Entry(vendordoc).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!vendordocExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);

        }

        // POST: api/vendordocs
        [ResponseType(typeof(vendordoc))]
        [HttpPost]
        [Route("api/vendordocs/addVendordoc")]
        public IHttpActionResult Postvendordoc(vendordoc vendordoc)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.vendordocs.Add(vendordoc);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }

        // DELETE: api/vendordocs/5
        [ResponseType(typeof(vendordoc))]
        [HttpDelete]
        [Route("api/vendordocs/removeVendordoc/{id}")]
        public IHttpActionResult Deletevendordoc(long id)
        {
            using (Entities db = new Entities())
            {
                vendordoc vendordoc = db.vendordocs.Find(id);
                if (vendordoc == null)
                {
                    return NotFound();
                }

                db.vendordocs.Remove(vendordoc);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }


        private bool vendordocExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.vendordocs.Count(e => e.vendorDocId == id) > 0;
            }
        }
    }
}