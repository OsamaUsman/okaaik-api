﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class promoCodesController : ApiController
    {


        // GET: api/promoCodes
        [HttpGet]
        public IHttpActionResult GenerateToken()
        {
            using (Entities db = new Entities())
            {
                static string method3(int length)
                {
                    using (var crypto = new RNGCryptoServiceProvider())
                    {
                        var bits = (length * 6);
                        var byte_size = ((bits + 7) / 8);
                        var bytesarray = new byte[byte_size];
                        crypto.GetBytes(bytesarray);
                        return Convert.ToBase64String(bytesarray);
                    }
                }
                promoCode promo = new promoCode();
                promo.code = method3(8);
                promo.discountPercentage = 10;
                promo.maximumLimit = 100;
                promo.validity = null;
                promo.isUsed = false;
                promo.isUser = true;

                db.promoCodes.Add(promo);
                try
                {
                   int check =  db.SaveChanges();
                    if (check > 0)
                    {

                        return Ok(new { status = "success", data = promo });
                    }
                    else 
                    {
                        return Ok(new { status = "error", data = "something went wrong." });

                    }
                }
                catch (Exception ex)
                {


                    return Ok(ex);

                }

              
            }

        }

        [HttpGet]
        public IHttpActionResult ValidateToken(string token)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    var validToken = db.promoCodes.Where(pro => pro.code == token).FirstOrDefault();
                    if (validToken == null)
                    {
                        return Ok(new { status = "success", data = "wrong promo code." });
                    }
                    else if (validToken.isUsed == false)
                    {
                        return Ok(new { status = "success", data = "valid promo code." });
                    }
                    else if (validToken.isUsed == true)
                    {
                        return Ok(new { status = "success", data = "invalid promo code." });
                    }
                    else
                    {
                        return Ok(new { status = "error", data = "something went wrong." });

                    }
                }
                catch (Exception ex)
                {


                    return Ok(ex);

                }
                


            }

        }
        [HttpGet]
        public IHttpActionResult GetpromoCodesforAdmin()
        {
            using (Entities db = new Entities())
            {
                var query = (from pm in db.promoCodes
                             where pm.isUser != true
                             select new
                             {
                                 pm.promocodeId,
                                 pm.validity,
                                 pm.discountPercentage,
                                 pm.maximumLimit,
                                 pm.code,
                             }).ToList();
                return Ok(query);
            }

        }


        public IHttpActionResult GetpromoCodes()
        {
            using (Entities db = new Entities())
            {
                var query = (from pm in db.promoCodes
                             where  pm.validity >= DateTime.Now
                             select new
                             {
                                 pm.promocodeId,
                                 pm.validity,
                                 pm.discountPercentage,
                                 pm.maximumLimit,
                                 pm.code,
                             }).ToList();
                return Ok(query);
            }

        }

        [HttpGet]
        public IHttpActionResult GetpromoCode(string code)
        {
            using (Entities db = new Entities())
            {
                var checkCode = db.promoCodes.Where(pro => pro.code == code).Select(pm=> new 
                {
                    pm.promocodeId,
                    pm.validity,
                    pm.discountPercentage,
                    pm.maximumLimit,
                    pm.code,
                    pm.isUsed,
                    pm.isUser,
                }).FirstOrDefault();
                if (checkCode != null)
                {
                    if (checkCode.validity == null)
                    {
                        if (checkCode.isUsed == false)
                        {
                            return Ok(new { status = "success", data = checkCode });
                        }
                       
                            return Ok(new { status = "success", data = "invalid promo code." });
                       
                    }
                    else
                    {
                        var query = (from pm in db.promoCodes
                                     where pm.code == code && pm.validity >= DateTime.Now
                                     select new
                                     {
                                         pm.promocodeId,
                                         pm.validity,
                                         pm.discountPercentage,
                                         pm.maximumLimit,
                                         pm.code,
                                         pm.isUsed,
                                         pm.isUser
                                     }).ToList();
                        return Ok(new { status = "success", data = query });

                    }
                }
                else 
                {
                     return Ok(new { status = "success", data = "invalid promo code." });

                }



            }
        }

        // GET: api/promoCodes/5
        //[ResponseType(typeof(promoCode))]
        //public IHttpActionResult GetpromoCode(long id)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        var query = (from pm in db.promoCodes
        //                     where pm.promocodeId == id
        //                     select new
        //                     {
        //                         pm.promocodeId,
        //                         pm.validity,
        //                         pm.discountPercentage,
        //                         pm.maximumLimit,

        //                     }).ToList();
        //        return Ok(query);
        //    }
        //}

        // PUT: api/promoCodes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutpromoCode(long id, promoCode promoCode)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (id != promoCode.promocodeId)
                {
                    return BadRequest();
                }

                db.Entry(promoCode).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!promoCodeExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }

        // POST: api/promoCodes
        [ResponseType(typeof(promoCode))]
        public IHttpActionResult PostpromoCode(promoCode promoCode)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.promoCodes.Add(promoCode);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {


                    return Ok(ex);

                }
            }
            Added st = new Added();
            return Ok(st);
        }

        // DELETE: api/promoCodes/5
        [ResponseType(typeof(promoCode))]
        public IHttpActionResult DeletepromoCode(long id)
        {
            using (Entities db = new Entities())
            {
                promoCode promoCode = db.promoCodes.Find(id);
                if (promoCode == null)
                {
                    return NotFound();
                }

                db.promoCodes.Remove(promoCode);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {


                    return Ok(ex);

                }

            }

            Deleted st = new Deleted();
            return Ok(st);
        }



        private bool promoCodeExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.promoCodes.Count(e => e.promocodeId == id) > 0;
            }
        }
    }
}