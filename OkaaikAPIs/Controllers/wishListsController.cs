﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class wishListsController : ApiController
    {

        [HttpPost]

        public IHttpActionResult GetwishList(DeviceViewModel vm)
        {
            using (Entities db = new Entities())
            {
                try
                {

                    var query = (from ws in db.wishLists
                                 where ws.userId == vm.userid || ws.deviceId == vm.DeviceId
                                 join pro in db.products
                                 on ws.productId equals pro.productId
                                 select new
                                 {
                                     ws.wishId,
                                     ws.productId,
                                     ws.userId,
                                     ws.deviceId,
                                     pro.title,
                                     pro.price,
                                     pro.categoryId,
                                     pro.description,
                                     pro.size,
                                     pro.vendorId,
                                     pro.persons,
                                     pro.weight,
                                     pro.approvedOn,
                                     pro.isApproved,
                                     pro.coverImg,
                                     pro.minimumTime,
                                     pro.perLayerCost,
                                 }).ToList();
                    return Ok(query);



                    //if (vm.userid != null)
                    //{
                    //    var query = (from ws in db.wishLists
                    //                 where ws.userId == vm.userid
                    //                 join pro in db.products
                    //                 on ws.productId equals pro.productId
                    //                 select new
                    //                 {
                    //                     ws.wishId,
                    //                     ws.productId,
                    //                     ws.userId,
                    //                     ws.deviceId,
                    //                     pro.title,
                    //                     pro.price,
                    //                     pro.categoryId,
                    //                     pro.description,
                    //                     pro.size,
                    //                     pro.vendorId,
                    //                     pro.persons,
                    //                     pro.weight,
                    //                     pro.approvedOn,
                    //                     pro.isApproved,
                    //                     pro.coverImg,
                    //                     pro.minimumTime,
                    //                     pro.perLayerCost,
                    //                 }).ToList();
                    //    return Ok(query);
                    //}


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

        public IHttpActionResult GetwishListbyDevice(string id)
        {
            using (Entities db = new Entities())
            {

                var query = (from ws in db.wishLists
                             where ws.deviceId == id
                             join pro in db.products
                             on ws.productId equals pro.productId
                             select new
                             {
                                 ws.wishId,
                                 ws.productId,
                                 ws.userId,
                                 ws.deviceId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.perLayerCost,
                             }).ToList();

                if (query == null)
                {
                    return NotFound();
                }

                return Ok(query);
            }

        }


        [HttpPut]
        [Route("api/wishLists/updateWishlist/{id}")]
        public IHttpActionResult UpdatewishList(long id, wishList wishList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities()
)
            {
                if (id != wishList.wishId)
                {
                    return BadRequest();
                }

                db.Entry(wishList).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!wishListExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }

        [HttpPost]
        [Route("api/wishLists/addWishlist")]
        public IHttpActionResult PostwishList(wishList wishList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.wishLists.Add(wishList);

                try
                {
                int check = db.SaveChanges();
                    if (check > 0)
                    {
                      var wishlists =  db.wishLists.Where(x => x.userId == wishList.userId).Select(x=> new 
                      {
                      x.wishId,
                      x.deviceId,
                      x.productId,
                      x.userId,
                      }).ToList();
                        return Ok(new { status = "success", data = wishlists });
                    }
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }

                Added st = new Added();
                return Ok(st);

            }
        }


        [HttpDelete]
        [Route("api/wishLists/removeWishlist/{id}")]
        public IHttpActionResult DeletewishList(long id)
        {
            using (Entities db = new Entities())
            {
                wishList wishList = db.wishLists.Find(id);
                if (wishList == null)
                {
                    return NotFound();
                }

                db.wishLists.Remove(wishList);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }


        private bool wishListExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.wishLists.Count(e => e.wishId == id) > 0;

            }
        }
    }
}