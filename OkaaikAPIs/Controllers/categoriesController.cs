﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class categoriesController : ApiController
    {

        [HttpGet]
        [Route("api/categories/getwithSubCategories")]
        public IHttpActionResult getwithSubCategories()
        {
            using (Entities db = new Entities())
            {
                var query = (from cat in db.categories
                             join sub in db.subCategories
                             on cat.categoryId equals sub.categoryId
                             into cati
                             from Subcategory in cati.DefaultIfEmpty()
                             select new
                             {
                                 cat.categoryId,
                                 cat.productId,
                                 cat.Image,
                                 cat.Name,
                                 subCategoryId = Subcategory.subCategoryId,
                                 subCategoryName = Subcategory.name,
                                 subCategoryImage = Subcategory.image,


                             }).ToList();
                return Ok(query);
            }
        }
        [HttpGet]
        [Route("api/categories/CategoriesForDropDown")]
        public IHttpActionResult CategoriesForDropDown()
        {
            using (Entities db = new Entities())
            {
                var query = (from cat in db.categories
                             where cat.Name != "Most popular/Trending"
                             select new
                             {
                                 cat.categoryId,
                                 cat.productId,
                                 cat.Image,
                                 cat.Name
                             }).ToList();
                var subCategory = (from sub in db.subCategories
                                   join cat in db.categories
                                   on sub.categoryId equals cat.categoryId
                                   select new
                                   {
                                       sub.subCategoryId,
                                       sub.name,
                                       sub.image,
                                       sub.categoryId,
                                       cat.Name
                                   }).ToList();
                return Ok(new { query, subCategory });
            }
        }


        [HttpGet]
        [Route("api/categories/getCategories")]
        public IHttpActionResult Getcategories()
        {
            using (Entities db = new Entities())
            {
                var query = (from cat in db.categories
                             select new
                             {
                                 cat.categoryId,
                                 cat.productId,
                                 cat.Image,
                                 cat.Name
                             }).ToList();
                var subCategory = (from sub in db.subCategories
                                   join cat in db.categories
                                   on sub.categoryId equals cat.categoryId
                                   select new
                                   {
                                       sub.subCategoryId,
                                       sub.name,
                                       sub.image,
                                       sub.categoryId,
                                       cat.Name
                                   }).ToList();
                return Ok(new { query, subCategory });
            }
        }
        [HttpGet]
        public IHttpActionResult Search(string city,string text)
        {
            using (Entities db = new Entities())
            {

                //var cat = db.categories.Where(p => p.Name.Contains(text)).FirstOrDefault();

                //var categories = (from cat in db.categories

                //                  where cat.Name.ToLower().Contains(text) /*&& pro.title.ToLower().Contains(text) */
                //                  select new
                //                  {
                //                      cat.categoryId,
                //                      cat.productId,
                //                      cat.Image,
                //                      cat.Name,


                //                  }).ToList();

                //var subCategories = (from cat in db.subCategories

                //                  where cat.name.ToLower().Contains(text) /*&& pro.title.ToLower().Contains(text) */
                //                  select new
                //                  {
                //                      cat.subCategoryId,
                //                      cat.categoryId,
                //                      cat.name,
                //                      cat.image,



                //                  }).ToList();
                var myproduct = db.vendors.Join(db.products, pro => pro.vendorId, ven => ven.vendorId, (ven, pro) => new { ven, pro })
                                           .Where(u => u.ven.city.ToLower().Contains(city.ToLower())).Select(u => new { 
                                               u.pro.productId,
                                               u.pro.title,
                                               u.pro.price,
                                               u.pro.categoryId,
                                               u.pro.description,
                                               u.pro.vendorId,
                                               u.pro.approvedOn,
                                               u.pro.isApproved,
                                               u.pro.coverImg,
                                               u.pro.minimumTime,
                                               u.pro.totalLayer,
                                               u.pro.keywords,
                                               u.pro.subCategoryId,
                                               u.ven.city,
                                           }).ToList();
                // myproduct = myproduct.Where(pro => pro.title.ToLower().Contains(text) ).ToList();
                try
                {
                    var searches = (from pro in myproduct

                                   where pro.keywords.ToString().ToLower().Contains(text) || pro.title.ToLower().Contains(text)

                                    select new 
                                    {
                                        pro.productId,
                                        pro.title,
                                        pro.price,
                                        pro.categoryId,
                                        pro.description,
                                        pro.vendorId,
                                        pro.approvedOn,
                                        pro.isApproved,
                                        pro.coverImg,
                                        pro.minimumTime,
                                        pro.totalLayer,
                                        pro.keywords,
                                        pro.subCategoryId,
                                        
                                    }).ToList();
                    return Ok(new { searches });
                }
                catch (Exception ex)
                {
                    
                    return Ok(new { ex });
                }

                //var products = (from pro in db.products
                //                join ven in db.vendors
                //                on pro.vendorId equals ven.vendorId
                //                where (ven.city.ToLower() == city) && pro.title.Contains(text) || pro.keywords.Contains(text)
                //                select new
                //                {


                //                    productid = pro.productId,
                //                    pro.title,
                //                    pro.price,
                //                    productCategory = pro.categoryId,
                //                    pro.description,

                //                    pro.vendorId,

                //                    pro.approvedOn,
                //                    pro.isApproved,
                //                    pro.coverImg,
                //                    pro.minimumTime,
                //                    pro.totalLayer,
                //                    pro.keywords,
                //                    pro.subCategoryId
                //                }).ToList();

                //List<object> myproductlist = new List<object>();

                //if (products.Count() != 0)
                //{
                //    products = products.Where(pro => pro.title.Contains(text) || pro.keywords.Contains(text)).ToList();
                //    foreach (var item in products)
                //    {
                //        myproductlist.Add(item);
                //    }
                //    return Ok(new { myproductlist });
                //}

                //else 
                //{
                //    return Ok(new { myproductlist });
                //}
                //var cat = db.categories.Where(p => p.Name.Contains(text)).FirstOrDefault();
                //if (cat == null)
                //{
                //    var pro = db.products.Where(p => p.title.Contains(text) == text).FirstOrDefault();
                //    if (pro == null)
                //    {
                //        return NotFound();
                //    }
                //    else 
                //    {
                //        string title = pro.title;
                //        var query = (from mypro in db.products

                //                     select new
                //                     {
                //                         pro.productId,
                //                         pro.title,
                //                         pro.price,
                //                         pro.categoryId,
                //                         pro.description,
                //                         pro.size,
                //                         pro.vendorId,
                //                         pro.persons,
                //                         pro.weight,
                //                         pro.approvedOn,
                //                         pro.isApproved,
                //                         pro.coverImg,
                //                         pro.minimumTime,
                //                         pro.perLayerCost,


                //                     }).ToList();

                //        return Ok(query);
                //    }
                //}
                //else 
                //{
                //    long? id = cat.categoryId; 
                //    var categories = (from mycat in db.categories
                //                     join jpro in db.products
                //                     on mycat.categoryId equals jpro.categoryId
                //                      where jpro.categoryId == id  
                //                      select new
                //                         {
                //                          jpro.productId,
                //                          jpro.title,
                //                          jpro.price,
                //                          jpro.categoryId,
                //                          jpro.description,
                //                          jpro.size,
                //                          jpro.vendorId,
                //                          jpro.persons,
                //                          jpro.weight,
                //                          jpro.approvedOn,
                //                          jpro.isApproved,
                //                          jpro.coverImg,
                //                          jpro.minimumTime,
                //                          jpro.perLayerCost,

                //                      }).ToList();

                //    return Ok(categories);
                //}

                //var query = (from cat in db.categories
                //             join pro in db.products
                //             on cat.productId equals pro.productId
                //             where cat.Name == text || pro.title == text
                //             select new
                //             {
                //                 cat.categoryId,
                //                 cat.productId,
                //                 cat.Image,
                //                 cat.Name,

                //                 productid =  pro.productId,
                //                 pro.title,
                //                 pro.price,
                //                 productCategory = pro.categoryId,
                //                 pro.description,
                //                 pro.size,
                //                 pro.vendorId,
                //                 pro.persons,
                //                 pro.weight,
                //                 pro.approvedOn,
                //                 pro.isApproved,
                //                 pro.coverImg,
                //                 pro.minimumTime,
                //                 pro.perLayerCost,
                //             }).ToList();
                //return Ok(query);
               

            }
        }
        //[HttpGet]
        //[Route("api/categories/getCategoryforMob/{id}")]
        //public IHttpActionResult GetcategoryforMob(long id)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        var query = (from cat in db.categories
        //                     join pro in db.products
        //                   on cat.productId equals pro.productId
        //                   where cat.categoryId == id   
        //                   //join proimg in db.productImgs
        //                     //on pro.productImgId equals proimg.productImgId
        //                     select new
        //                     {
        //                         cat.categoryId,
        //                         cat.Name ,
        //                         pro.productId,
        //                         //proimg.imagePath,

        //                     }).ToList();
        //        var qu = query[0].productId;
        //        var queryforImage = (from pro in db.products
        //                             join proImg in db.productImgs
        //                             on pro.productImgId equals proImg.productImgId
        //                             where pro.productImgId == qu 

        //        return Ok(query);
        //    }
        //}



        // GET: api/categories/5
        [ResponseType(typeof(category))]
        [HttpGet]
        [Route("api/categories/getCategory/{id}")]
        public IHttpActionResult Getcategory(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from cat in db.categories
                             where cat.categoryId == id
                             select new
                             {
                                 cat.categoryId,
                                 //cat.productId,
                                 cat.Image,
                                 cat.Name
                             }).ToList();
                if (query == null)
                {
                    return NotFound();
                }

                return Ok(query);
            }
        }

        // PUT: api/categories/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/categories/updateCategory/{id}")]
        public IHttpActionResult Putcategory(long id, category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.categoryId)
            {
                return BadRequest();
            }
            using (Entities db = new Entities())
            {
                db.Entry(category).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!categoryExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }

        // POST: api/categories
        [ResponseType(typeof(category))]
        [HttpPost]
        [Route("api/categories/addCategory")]
        public IHttpActionResult Postcategory(category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.categories.Add(category);
                db.SaveChanges();
            }
            Added st = new Added();
            return Ok(st);
        }


        [ResponseType(typeof(category))]
        [HttpDelete]
        [Route("api/categories/removeCategory/{id}")]
        public IHttpActionResult Deletecategory(long id)
        {
            using (Entities db = new Entities())
            {
                category category = db.categories.Find(id);
                if (category == null)
                {
                    return NotFound();
                }
                db.categories.Remove(category);
                int check;
                try
                {
                    check = db.SaveChanges();
                }
                catch (Exception ex)
                {

                    Error er = new Error();
                    return Ok(ex);

                }
                // var parent = db.categories.Include(p => p.products)
                // .SingleOrDefault(p => p.categoryId == id);
                // var pid = parent.productId;


                // var parent1 = db.products.Include(p => p.favourities)
                //.SingleOrDefault(p => p.productId == pid);

                // foreach (var child1 in parent1.favourities.ToList())
                //     db.favourities.Remove(child1);

                // db.SaveChanges();

                // foreach (var child in parent.products.ToList())
                //     db.products.Remove(child);

                // int check = db.SaveChanges();

                // if (check > 0 || check == 0)
                // {

                //     if (category == null)
                //     {
                //         return NotFound();
                //     }

                //     db.categories.Remove(category);
                //     db.SaveChanges();
                // }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }



        private bool categoryExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.categories.Count(e => e.categoryId == id) > 0;
            }
        }
    }
}