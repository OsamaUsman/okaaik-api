﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace Okaaikapis.controllers
{

    public class flavourscontroller : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetAllActive(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.flavours
                             where bn.vendorId == id && bn.Status.ToLower() == "in stock"
                             select new
                             {
                                 bn.flavourId,
                                 bn.Name,
                                 bn.Status,
                                 bn.vendorId
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }




        [HttpGet]
        [Route("api/flavours/getallflavours")]
        public IHttpActionResult getallflavours()
        {
            using (Entities db = new Entities())
            {
                var query = (from flv in db.flavours

                             select new
                             {
                                 flv.flavourId,
                                 flv.Name,
                                 flv.Status,
                                 flv.vendorId


                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }

        }

        [HttpGet]
       
        public IHttpActionResult getflavours(long id,int pageNo)
        {
            using (Entities db = new Entities())
            {


                int page = 10;
                if (pageNo == 1)
                {
                    page = 0;
                }
                else
                {
                    page = (pageNo - 1) * page;
                }

                var flavours = db.flavours.Where(x => x.vendorId == id).Select(x => new { x.flavourId, x.Name, x.Status , x.vendorId}).OrderBy(x => x.flavourId).Skip(page).Take(10).ToList();
              
                var totalRows = db.flavours.Where(x => x.vendorId == id).Count();
                


                return Ok(new { status = "success", flavours, totalRows });

            }

        }
        [HttpGet]

        public IHttpActionResult getflavourbystatus(long id, string status)
        {
            using (Entities db = new Entities())
            {
                var query = (from flv in db.flavours
                             where flv.vendorId == id &&
                              flv.Status == status
                             select new
                             {
                                 flv.flavourId,
                                 flv.Name,
                                 flv.Status,
                                 flv.vendorId

                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);


            }

        }

        // put: api/flavours/5

        //// post: api/flavours
        //[responsetype(typeof(flavour))]
        public IHttpActionResult postflavour(flavour flavour)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {


                db.flavours.Add(flavour);

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (FlavourExists(flavour.flavourId))
                    {
                        return Conflict();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Added st = new Added();
            return Ok(st);
        }

        [HttpPut]
        public IHttpActionResult PutFlavour(long id, flavour flavour)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != flavour.flavourId)
                {
                    return BadRequest();
                }

                db.Entry(flavour).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FlavourExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        //// delete: api/flavours/5
        [ResponseType(typeof(flavour))]
        public IHttpActionResult deleteflavour(long id)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    flavour flavour = db.flavours.Find(id);
                    long vendorId = (long)flavour.vendorId;
                if (flavour == null)
                {
                    return NotFound();
                }


                    db.flavours.Remove(flavour);

                    int check;
               
                    check = db.SaveChanges();
                    var remainingFlavours = db.flavours.Where(x => x.vendorId == vendorId).Count();
                    return Ok(new { status = "success", remainingFlavours });
                }
                catch (Exception ex)
                {


                    return Ok(ex);

                }
               
               
            }

        }

        //protected override void dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.dispose();
        //    }
        //    base.dispose(disposing);
        //}

        private bool FlavourExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.flavours.Count(e => e.flavourId == id) > 0;
            }
        }
    }
}