﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{

    public class bannersController : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.banners
                             where bn.validityDate >= DateTime.Today
                             select new
                             {
                                 bn.bannerId,
                                 bn.categoryId,
                                 bn.image,
                                 bn.validityDate
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpGet]
        public IHttpActionResult GetAllForAdmin()
        {
            using (Entities db = new Entities())
            {
                var banners = (from bn in db.banners
                               select new
                               {
                                   bn.bannerId,
                                   bn.categoryId,
                                   bn.image,
                                   bn.validityDate
                               }).ToList();
                var categories = (from ca in db.categories
                                  select new
                                  {
                                      ca.categoryId,
                                      ca.Name
                                  }).ToList();
               
                return Ok(new {status="success", banners, categories });
            }


        }


        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.banners
                             where bn.bannerId == id
                             select new
                             {
                                 bn.bannerId,
                                 bn.categoryId,
                                 bn.image,
                                 bn.validityDate
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, banner banner)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != banner.bannerId)
                {
                    return BadRequest();
                }

                db.Entry(banner).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!bannerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(banner banner)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.banners.Add(banner);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                banner banner = db.banners.Find(id);
                if (banner == null)
                {
                    return NotFound();
                }

                db.banners.Remove(banner);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool bannerExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.banners.Count(e => e.bannerId == id) > 0;
            }
        }
    }
}
