﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class keywordTemplatesController : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.keywordTemplates
                            
                             select new
                             {
                                 bn.templateId,
                                 bn.title
                              
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(string title)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.keywordTemplates
                             where bn.title == title
                             select new
                             {
                             bn.templateId,
                             bn.title,
                             bn.keywords
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, keywordTemplate keywordTemplate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != keywordTemplate.templateId)
                {
                    return BadRequest();
                }

                db.Entry(keywordTemplate).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!keywordTemplateExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(keywordTemplate keywordTemplate)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (keywordTemplate != null)
                {
                    var row = db.keywordTemplates.Where(t => t.title == keywordTemplate.title).FirstOrDefault();
                    if (row == null)
                    {

                        db.keywordTemplates.Add(keywordTemplate);
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            return Ok(ex);
                        }
                        Added st = new Added();
                        return Ok(st);
                    }
                    else
                    {
                        return Ok(new { data = "Not Added", status = "exist" });
                    }

                }
                else 
                {
                 return Ok(new { data = "something went wrong!", status = "error" });
                }
            }

        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                keywordTemplate keywordTemplate = db.keywordTemplates.Find(id);
                if (keywordTemplate == null)
                {
                    return NotFound();
                }

                db.keywordTemplates.Remove(keywordTemplate);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool keywordTemplateExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.keywordTemplates.Count(e => e.templateId == id) > 0;
            }
        }
    }
}
