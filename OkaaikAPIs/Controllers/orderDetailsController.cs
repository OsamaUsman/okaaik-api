﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class orderDetailsController : ApiController
    {

        [HttpGet]
        [Route("api/orderDetails/getorderDetails")]
        public IHttpActionResult GetorderDetails()
        {
            using (Entities db = new Entities())
            {

                var query = (from od in db.orderDetails
                             select new
                             {
                                 od.orderDetailsid,
                                 od.orderId,
                                 od.productId,
                                 od.vendorId,
                                 od.addressId,
                                 od.date,
                                 od.status,
                                 od.size,
                                 od.sizeId,
                                 od.cost,
                                 od.deliveryMethod,
                                 od.deliverToMe,
                                 od.message,
                                 od.longitude,
                                 od.latitude,
                                 od.flavourId,
                                 od.flavourName,
                                 od.layerId,
                                 od.layerName,
                                 od.addressTitle,
                                 od.address,
                                 od.productName,
                                 od.pickupTime,
                                 od.contactNo,
                                 od.name,
                                 od.streetNo,
                                 od.note
                             }).ToList();
                return Ok(query);
            }
        }

        // GET: api/orderDetails/5
        [ResponseType(typeof(orderDetail))]
        [HttpGet]
        [Route("api/orderDetails/getorderDetail/{id}")]
        public IHttpActionResult GetorderDetail(long id)
        {
            using (Entities db = new Entities())
            {

                var query = (from od in db.orderDetails
                             where od.orderDetailsid == id
                             select new
                             {
                                 od.orderDetailsid,
                                 od.orderId,
                                 od.productId,
                                 od.vendorId,
                                 od.addressId,
                                 od.date,
                                 od.status,
                                 od.deliverToMe,
                                 od.contactNo,
                                 od.name,
                                 od.streetNo
                             }).ToList();

                if (query == null)
                {
                    return NotFound();
                }

                return Ok(query);
            }
        }

        // PUT: api/orderDetails/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/orderDetails/updateorderDetail/{id}")]
        public IHttpActionResult PutorderDetail(long id, orderDetail orderDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != orderDetail.orderDetailsid)
                {
                    return BadRequest();
                }

                db.Entry(orderDetail).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!orderDetailExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);

        }

        // POST: api/orderDetails
        [ResponseType(typeof(orderDetail))]
        [HttpPost]
        [Route("api/orderDetails/addorderDetail")]

        public IHttpActionResult PostorderDetail(orderDetail orderDetail)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.orderDetails.Add(orderDetail);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }

        protected static object orderDetailList<orderDetail>(IEnumerable<orderDetail> list)
        {


            Entities db = new Entities();
            db.orderDetails.AddRange((IEnumerable<Model.orderDetail>)list);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex;
            }
            Added st = new Added();
            return st;

        }

        [HttpPost]
        public IHttpActionResult PostOrderDetailList(IEnumerable<orderDetail> list)
        {

            var row = orderDetailList(list);

            return Ok(row);

        }




        // DELETE: api/orderDetails/5
        [ResponseType(typeof(orderDetail))]
        [HttpDelete]
        [Route("api/orderDetails/removeorderDetail/{id}")]

        public IHttpActionResult DeleteorderDetail(long id)
        {
            using (Entities db = new Entities())
            {
                orderDetail orderDetail = db.orderDetails.Find(id);
                if (orderDetail == null)
                {
                    return NotFound();
                }

                db.orderDetails.Remove(orderDetail);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);

            }
        }



        private bool orderDetailExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.orderDetails.Count(e => e.orderDetailsid == id) > 0;
            }
        }
    }
}