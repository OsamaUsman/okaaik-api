﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class usersController : ApiController
    {
       
        [HttpPost]
        public IHttpActionResult AuthenticateUser(loginViewModel vm)
        {
            using (Entities db = new Entities())
            {
                if (vm.email == null || vm.email == "" || vm.password == "" || vm.password == null)
                {
                    return Ok(new { message = "email or password cannot be null", status = "success" });
                }
                else 
                {
                    string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(vm.password);
                    var row = db.users.Where(us => us.email == vm.email && us.password == encpass).Select(us=> new 
                    {
                    us.userId,
                    us.userName,
                    us.email,
                    us.firstName,
                    us.lastName,
                    us.mobileno,
                    us.roles,
                    us.status,
                    us.suspendedTill,
                    us.verificationCode,
                    }).FirstOrDefault();
                   
                    if (row == null)
                    {
                        return Ok(new { data = "not exist", status = "success" });
                    }
                    else
                    {
                        return Ok(new { data = row, status = "success" });
                    }
                }
                
            }


            //var identity = (ClaimsIdentity)User.Identity;
            //var  ClaimsArray = identity.Claims.Select(m => m.Value).ToArray();
            //long userId = long.Parse(ClaimsArray[1]) ;
            //using (Entities db = new Entities())
            //{
            //    var row = db.users.Where(us => us.userId == userId).Select(us=> new
            //    {
            //    us.userId,
            //    us.userName,
            //    us.mobileno,
            //    us.email,
            //    us.roles,
            //    us.status,
            //    }).FirstOrDefault();

            //    if (row == null)
            //    {
            //        return Ok(new { data = "notExist", status = "success" });
            //    }
            //    else
            //    {
            //        return Ok(new { data = row, status = "success" });
            //    }
            //}



            //using (Entities db = new Entities())
            //{


            //    if (row == null)
            //    {
            //        return Ok(new { data = "notExist", status = "success" });
            //    }
            //    else
            //    {
            //        return Ok(new { data = "isExist", status = "error" });
            //    }
            //}


        }


        [HttpGet]
        public IHttpActionResult UserExist(string email)
        {
            using (Entities db = new Entities())
            {
                var row = db.users.Where(us => us.email == email).FirstOrDefault();

                if (row == null)
                {
                    return Ok(new { data = "notExist", status = "success" });
                }
                else
                {
                    return Ok(new { data = "isExist", status = "error" });
                }
            }


        }

        [HttpPost]
        public IHttpActionResult CheckUserForProfile(CheckProfileViewModel vm)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var checkEmail = db.users.Where(us => us.email == vm.email && us.userId != vm.userId).FirstOrDefault();

                var checkMobile = db.users.Where(us => us.mobileno == vm.mobileno && us.userId != vm.userId).FirstOrDefault();

                if (checkEmail != null && checkMobile != null)
                {
                    return Ok(new { data = "These email and mobile number are already exist.", status = "success" });
                }
                else if (checkEmail != null && checkMobile == null)
                {
                    return Ok(new { data = "This email is already exist.", status = "success" });
                }
                else if (checkEmail == null && checkMobile != null)
                {
                    return Ok(new { data = "This mobile number is already exist.", status = "success" });
                }
                else if (checkEmail == null && checkMobile == null)
                {
                    int _min = 1000;
                    int _max = 9999;
                    Random rand = new Random(DateTime.Now.Millisecond);
                    long code = rand.Next(_min, _max);


                    return Ok(new { data = code, status = "success" });
                }
                else 
                {
                    return Ok(new { data = "something went wrong.", status = "error" });
                }
            }


        }

        [ResponseType(typeof(user))]
        [HttpGet]
        [Route("api/users/getUser/{id}")]
        public IHttpActionResult Getuser(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from us in db.users
                             where us.userId == id && us.roles == "user"
                             select new
                             {
                                 us.userId,
                                 us.firstName,
                                 us.lastName,
                                 us.email,
                                 us.mobileno,
                                 us.roles,
                                 us.suspendedTill,
                                 us.status
                             }).ToList();

                if (query == null)
                {
                    return NotFound();
                }

                return Ok(query);
            }

        }

        [HttpPost]
        [Route("api/users/LoginVendorNew")]
        public IHttpActionResult LoginVendorNew(LoginVendorNewViewModel vm) 
        {
            try
            {
                using (Entities db = new Entities())
                {
                    if (string.IsNullOrEmpty(vm.email) == false && string.IsNullOrEmpty(vm.password) == false)
                    {
                        string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(vm.password);
                        var row = db.users.Where(u => u.email == vm.email && u.password == encpass && u.roles == "vendor").Select(x=> new 
                        {
                            x.deviceId,
                            x.email,
                            x.firstName,
                            x.lastName,
                            x.mobileno,
                            x.roles,
                            x.status,
                            x.userId,
                            x.userName,
                            x.verificationCode,
                        }).FirstOrDefault();

                        if (row != null)
                        {
                            var myuser = db.users.Where(u => u.userId == row.userId).FirstOrDefault();
                            var vendor = db.vendors.Where(u => u.userId == row.userId).Select(x=> new {
                                x.bakeryName,
                                x.logo,
                                x.vendorId,
                            }).FirstOrDefault();

                            myuser.deviceId = vm.deviceId; 
                            db.Entry(myuser).State = EntityState.Modified;
                            db.SaveChanges();
                            return Ok(new { status = "success", data = row , vendor });
                        }

                        else 
                        {
                        return Ok(new { status = "success", data = "Your email or password is incorrect." });
                        }

                    }
                    else 
                    {
                      return Ok(new { status = "success", data = "Email and password is required." });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        [Route("api/users/Loginvendor/")]
        public IHttpActionResult Loginvendor(Asp asp)
        {
            using (Entities db = new Entities())
            {
                //myName == email myAge == password 
                if (asp.myName != "" && asp.myAge != "")
                {
                    string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(asp.myAge);
                    var row = db.users.Where(u => u.email == asp.myName && u.password == encpass ).FirstOrDefault();

                    if (row != null)
                    {
                        if (row.roles == "vendor")
                        {
                            if (row.status == "declined")
                            {
                                return Ok(new { user = "declined", status = "declined" });
                            }
                            else
                            {
                                var vendorrow = db.vendors.Where(ven => ven.userId == row.userId).FirstOrDefault();
                                if (vendorrow != null)
                                {
                                    return Ok(new { user = row.email, row.userId, row.userName, status = row.status, role = row.roles, vendorId = vendorrow.vendorId, logo = vendorrow.logo, bakery = vendorrow.bakeryName, vendorrow.city });
                                }
                                else
                                {
                                    return Ok(new { data = "something went wrong", status = "error" });

                                }
                            }                        
                        }
                        else if (row.roles == "admin")
                        {
                            return Ok(new { user = row.email, row.userId, row.userName, status = row.status, role = row.roles });
                        }
                        else
                        {
                            return Ok(new { user = "notallowed", status = "error" });
                        }

                    }
                    else
                    {
                        return Ok(new { user = "this user is not registerd!", status = "notexist" });
                    }

                }
                else
                {
                    return Ok(new { user = "something went wrong!", status = "error" });
                }


            }

        }


        //[ResponseType(typeof(user))]
        //[HttpGet]
        //[Route("api/users/mobloginUser/{mob}")]
        //public IHttpActionResult MobLoginuser(string mob)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        var row = db.users.Where(u => u.mobileno == mob).FirstOrDefault();
        //        if (row == null)
        //        {
        //            //string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(user.password);
        //            //user.password = encpass;
        //            //Verification Code
        //            user us = new user();
        //            int _min = 1000;
        //            int _max = 9999;
        //            Random rand = new Random(DateTime.Now.Millisecond);
        //            rand.Next(_min, _max);
        //            us.verificationCode = rand.Next(_min, _max);
        //            us.mobileno = mob;
        //            us.roles = "user";
        //            db.users.Add(us);
        //            int check= db.SaveChanges();

        //            if (check > 0) 
        //            {
        //                SmsViewModel sms = new SmsViewModel();
        //                sms.Code = us.verificationCode.ToString();
        //                sms.To = us.mobileno;
        //                return Ok(usersController.SendMsg(sms));
        //            }

        //            return Ok(us);



        //        }
        //        else 
        //        {

        //            var get = db.users.Find(row.userId);

        //            int _min = 1000;
        //            int _max = 9999;
        //            Random rand = new Random(DateTime.Now.Millisecond);
        //            rand.Next(_min, _max);
        //            get.userId = row.userId;
        //            get.verificationCode = rand.Next(_min, _max);
        //            get.mobileno = row.mobileno;
        //            get.roles = row.roles;
        //            //db.Entry(myus).State = EntityState.Detached;
        //            db.Entry(get).State = EntityState.Modified;
        //            //db.users.Attach(myus);

        //            try
        //            {
        //              int check = db.SaveChanges();
        //                if (check > 0) 
        //                {
        //                    SmsViewModel sms = new SmsViewModel();
        //                    sms.Code = get.verificationCode.ToString();
        //                    sms.To = get.mobileno;
        //                    usersController.SendMsg(sms);
        //                }

        //            }

        //            catch (DbUpdateConcurrencyException)
        //            {
        //                throw;
        //            }

        //            var query = (from us in db.users
        //                         where  us.userId == get.userId
        //                         select new
        //                         {
        //                             us.userId,
        //                             us.mobileno,
        //                             us.verificationCode
        //                         }).ToList();
        //            return Ok(query);
        //        }


        //    }

        //}
        [HttpPost]
        public IHttpActionResult MobLoginuser(mobileloginViewModel vm)
        {
            using (Entities db = new Entities())
            {
                var row = db.users.Where(u => u.mobileno == vm.mobile).FirstOrDefault();
                if (row != null)
                {
                    int _min = 1000;
                    int _max = 9999;
                    Random rand = new Random(DateTime.Now.Millisecond);
                    rand.Next(_min, _max);

                    row.verificationCode = rand.Next(_min, _max);
                    //db.Entry(myus).State = EntityState.Detached;
                    db.Entry(row).State = EntityState.Modified;
                    //db.users.Attach(myus);

                    try
                    {
                        int check = db.SaveChanges();
                        if (check > 0)
                        {
                            //SmsViewModel sms = new SmsViewModel();
                            //sms.Code = row.verificationCode.ToString();
                            //sms.To = vm.mobile;
                            //var msg = usersController.SendMsg(sms);

                            return Ok(new
                            {
                                status = "success",
                                //msg = msg,
                                Id = row.userId,
                                Name = row.userName,
                                Mobile = row.mobileno,
                                code = row.verificationCode
                            });
                        }
                        else
                        {
                            return Ok(new
                            {
                                status = "error",
                                data = "something went wrong!"
                            });
                        }

                    }

                    catch (DbUpdateConcurrencyException)
                    {
                        throw;
                    }


                }
                else
                {
                    return Ok(new
                    {
                        status = "error",
                        data = "this user is not registered."
                    });
                }
            }
        }


        public static object SendMsg(SmsViewModel vm)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://sms.fastech.pk/Sms/Registration/");

                //HTTP POST
                var postTask = client.PostAsJsonAsync<SmsViewModel>("vm", vm);
                postTask.Wait();

                var result = postTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    return (result.IsSuccessStatusCode);
                }
                return (result.IsSuccessStatusCode);
            }
        }



        [HttpPut]
        public IHttpActionResult Putuser(long id, user user)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                if (id != user.userId)
                {
                    return BadRequest();
                }
                string Userpassword = db.users.Where(us => us.userId == user.userId).Select(us => us.password).FirstOrDefault();
                user.password = Userpassword;
                db.Entry(user).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!userExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            Updated st = new Updated();
            return Ok(st);

        }

        [HttpPost]
        public IHttpActionResult CheckCurrentPassword(CurrentPasswordViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(vm.password);
                    var user = db.users.Where(us => us.userId == vm.userId && us.password == encpass).FirstOrDefault();
                    if (user != null)
                    {
                        int _min = 1000;
                        int _max = 9999;
                        Random rand = new Random(DateTime.Now.Millisecond);
                        long code = rand.Next(_min, _max);
                        return Ok(new { status = "success", data = code });
                    }
                    else
                    {
                        return Ok(new { status = "success", data = "not exist" });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        [HttpPost]
        public IHttpActionResult NewPassword(CurrentPasswordViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(vm.password);
                    var user = db.users.Where(us => us.userId == vm.userId).FirstOrDefault();

                    if (user != null)
                    {
                        user.password = encpass;
                        db.Entry(user).State = EntityState.Modified;
                        int check = db.SaveChanges();
                        if (check > 0)
                        {
                            return Ok(new { status = "success", data = "updated." });
                        }
                        else
                        {
                            return Ok(new { status = "error", data = "something went wrong." });
                        }

                    }
                    else
                    {
                        return Ok(new { status = "success", data = "not exist" });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        
        }

        [ResponseType(typeof(user))]
        [HttpPost]
        [Route("api/users/addUser")]
        public IHttpActionResult addUser(user user)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }
                    //Password
                    var check = db.users.Where(us => us.email == user.email).FirstOrDefault();
                    if (check != null)
                    {
                        return Ok(new { status = "success", data = "this email is already exist." });
                    }
                    else 
                    {
                        string encpass = OkaaikAPIs.EncryptDecrypt.Encrypt(user.password);
                        user.password = encpass;
                        user.roles = "user";
                        //Verification Code

                        db.users.Add(user);
                        int chec = db.SaveChanges();
                        if (chec > 0)
                        {
                            var registeredUser = db.users.Where(us => us.userId == user.userId).Select(us => new
                            {
                                us.userId,
                                us.userName,
                                us.email,
                                us.firstName,
                                us.lastName,
                                us.mobileno,
                                us.roles,
                                us.status,
                                us.suspendedTill,
                                us.verificationCode,
                            }).FirstOrDefault();
                           // registeredUser.password = null;
                            return Ok(new { status = "success", data = registeredUser });
                        }
                        else
                        {
                            return Ok(new { status = "error", data = "something went wrong." });
                        }
                    }
                  
                }
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [HttpGet]
        public IHttpActionResult CheckEmail(string email)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var row = db.users.Where(us => us.email == email).FirstOrDefault();
                    if (row != null)
                    {
                        return Ok(new { status = "success", data = "exist" });
                    }
                    else
                    {
                        return Ok(new { status = "success", data = "not exist" });
                    }

                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public IHttpActionResult SendOtp(OtpViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    int _min = 1000;
                    int _max = 9999;
                    Random rand = new Random(DateTime.Now.Millisecond);
                    long code = rand.Next(_min, _max);

                    var row = db.users.Where(us => us.mobileno == vm.mobileno).FirstOrDefault();
                    if (row != null)
                    {
                        return Ok(new { status = "success", data = "exist" });

                    }
                    else
                    {
                        if (vm.type == "mobileno")
                        {
                            return Ok(new { status = "success", data = code });

                        }
                        else if (vm.type == "email")
                        {
                            return Ok(new { status = "success", data = code });
                        }
                        else
                        {
                            return Ok(new { status = "error", message = "please select sending type email or mobileno", data = "" });
                        }


                    }



                }

            }
            catch (Exception)
            {

                throw;
            }
        }
        [HttpPost]
        public IHttpActionResult RegisterMobuser(RegisterMobileViewModel vm)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var row = db.users.Where(u => u.mobileno == vm.mobileNo).FirstOrDefault();
                if (row == null)
                {
                    user us = new user();
                    us.userName = vm.name;
                    us.mobileno = vm.mobileNo;
                    us.roles = "user";
                    int _min = 1000;
                    int _max = 9999;
                    Random rand = new Random(DateTime.Now.Millisecond);
                    rand.Next(_min, _max);
                    us.verificationCode = rand.Next(_min, _max);
                    db.users.Add(us);
                    try
                    {
                        int check = db.SaveChanges();
                        if (check > 0)
                        {

                            //SmsViewModel sms = new SmsViewModel();
                            //sms.Code = us.verificationCode.ToString();
                            //sms.To = us.mobileno;
                            //var msg = usersController.SendMsg(sms);
                            return Ok(new
                            {
                                status = "success",
                                data = "added",
                                //msg = msg,
                                id = us.userId,
                                mobile = us.mobileno,
                                name = us.userName,
                                code = us.verificationCode
                            });
                        }
                        else
                        {
                            Ok(new
                            {
                                status = "error",
                                data = "something went wrong!"
                            });

                        }

                    }

                    catch (DbUpdateConcurrencyException)
                    {
                        throw;
                    }
                }
                else
                {
                    return Ok(new
                    {
                        data = "This user is already exsist..",
                        status = "error"
                    });
                }


                return Ok();



            }

        }

        [ResponseType(typeof(user))]
        [HttpDelete]
        [Route("api/users/removeUser/{id}")]
        public IHttpActionResult Deleteuser(long id)
        {
            using (Entities db = new Entities())
            {
                user user = db.users.Find(id);
                if (user == null)
                {
                    return NotFound();
                }

                db.users.Remove(user);
                int check;
                try
                {
                    check = db.SaveChanges();
                }
                catch (Exception ex)
                {

                    return Ok(ex);

                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }



        private bool userExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.users.Count(e => e.userId == id) > 0;

            }
        }
    }
}