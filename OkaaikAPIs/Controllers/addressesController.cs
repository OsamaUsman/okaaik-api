﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Content
{

    public class addressesController : ApiController
    {

        // GET: api/addresses
        [HttpGet]
        [Route("api/addresses/getAddresses")]

        public IHttpActionResult Getaddresses()
        {
            using (Entities db = new Entities())
            {
                var query = (from ad in db.addresses

                             select new
                             {
                                 ad.addressId,
                                 ad.customerId,
                                 ad.title,
                                 ad.date,
                                 ad.deviceId,
                                 ad.latitude,
                                 ad.longitude,
                                 ad.userId,
                                 ad.mapAddress,
                                 ad.mobilePhone,
                                 ad.name,
                                 ad.addressOne,
                                 ad.city

                             }).ToList();
                return Ok(query);
            }

        }

        [HttpGet]
        [Route("api/addresses/GetUseraddress/{id}")]

        public IHttpActionResult GetUseraddress(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from ad in db.addresses
                             where ad.userId == id
                             select new
                             {
                                 ad.addressId,
                                 ad.customerId,
                                 ad.title,
                                 ad.date,
                                 ad.deviceId,
                                 ad.latitude,
                                 ad.longitude,
                                 ad.userId,
                                 ad.mapAddress,
                                 ad.mobilePhone,
                                 ad.name,
                                 ad.addressOne,
                                 ad.city

                             }).ToList();
                return Ok(query);
            }

        }

        // GET: api/addresses/5

        [HttpPost]
        [Route("api/addresses/getAddress/")]

        public IHttpActionResult Getaddress(DeviceViewModel vm)
        {
            using (Entities db = new Entities())
            {
                try
                {



                    var query = (from ad in db.addresses
                                 where ad.userId == vm.userid || ad.deviceId == vm.DeviceId
                                 select new
                                 {
                                     ad.addressId,
                                     ad.customerId,
                                     ad.title,
                                     ad.date,
                                     ad.deviceId,
                                     ad.latitude,
                                     ad.longitude,
                                     ad.userId,
                                     ad.mapAddress,
                                     ad.mobilePhone,
                                     ad.name,
                                     ad.addressOne,
                                     ad.city
                                 }).ToList();
                    return Ok(query);




                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
        }

        // PUT: api/addresses/5

        [HttpPut]
        [Route("api/addresses/updateAddress/{id}")]
        public IHttpActionResult Putaddress(long id, address address)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                if (id != address.addressId)
                {
                    return BadRequest();
                }
                //address myaddress = db.addresses.Find(id);

                if (address != null)
                {
                    var row = db.orderDetails.Where(o => o.addressId == id).FirstOrDefault();
                    if (row == null)
                    {
                        db.Entry(address).State = EntityState.Modified;

                        try
                        {
                            db.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException)
                        {
                            if (!addressExists(id))
                            {
                                return NotFound();
                            }
                            else
                            {
                                throw;
                            }
                        }
                        Updated st = new Updated();
                        return Ok(st);
                    }
                    else
                    {
                        var check = row.status;
                        if (check == "Delivered")
                        {
                            db.Entry(address).State = EntityState.Modified;

                            try
                            {
                                db.SaveChanges();
                            }
                            catch (DbUpdateConcurrencyException)
                            {
                                if (!addressExists(id))
                                {
                                    return NotFound();
                                }
                                else
                                {
                                    throw;
                                }
                            }
                            Updated st = new Updated();
                            return Ok(st);

                            //var parent = db.addresses.Include(p => p.orderDetails)
                            //.SingleOrDefault(p => p.addressId == id);

                            //foreach (var child in parent.orderDetails.ToList())
                            //    db.orderDetails.Remove(child);
                        }
                        else
                        {
                            return Ok("Order in process.");
                        }
                    }




                }
                else
                {
                    return NotFound();
                }
                //if (!ModelState.IsValid)
                //{
                //    return BadRequest(ModelState);
                //}

                //if (id != address.addressId)
                //{
                //    return BadRequest();
                //}

                //db.Entry(address).State = EntityState.Modified;

                //try
                //{
                //    db.SaveChanges();
                //}
                //catch (DbUpdateConcurrencyException)
                //{
                //    if (!addressExists(id))
                //    {
                //        return NotFound();
                //    }
                //    else
                //    {
                //        throw;
                //    }
                //}
                //Updated st = new Updated();
                //return Ok(st);
            }
        }

        // POST: api/addresses

        [HttpPost]
        [Route("api/addresses/addAddress")]
        public IHttpActionResult Postaddress(address address)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.addresses.Add(address);
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateException)
                {
                    if (addressExists(address.addressId))
                    {
                        return Conflict();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Added st = new Added();
            return Ok(st);
        }

        // DELETE: api/addresses/5

        [HttpDelete]
        [Route("api/addresses/removeAddress/{id}")]
        public IHttpActionResult Deleteaddress(long id)
        {
            using (Entities db = new Entities())
            {
                address address = db.addresses.Find(id);

                if (address != null)
                {
                    var row = db.orderDetails.Where(o => o.addressId == id).FirstOrDefault();
                    if (row == null)
                    {
                        db.addresses.Remove(address);
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {


                            return Ok(ex);

                        }
                        Deleted st = new Deleted();
                        return Ok(st);
                    }
                    else
                    {
                        var check = row.status;
                        if (check == "Delivered")
                        {
                            db.addresses.Remove(address);
                            try
                            {
                                db.SaveChanges();
                            }
                            catch (Exception ex)
                            {


                                return Ok(ex);

                            }
                            Deleted st = new Deleted();
                            return Ok(st);

                            //var parent = db.addresses.Include(p => p.orderDetails)
                            //.SingleOrDefault(p => p.addressId == id);

                            //foreach (var child in parent.orderDetails.ToList())
                            //    db.orderDetails.Remove(child);
                        }
                        else
                        {
                            return Ok("Order in process.");
                        }
                    }

                    //    var row = db.orderDetails.Where(o => o.addressId == id).FirstOrDefault();
                    //    var check = row.status;
                    //    if (check == "Delivered")
                    //    {

                    //        var parent = db.addresses.Include(p => p.orderDetails)
                    //        .SingleOrDefault(p => p.addressId == id);

                    //        foreach (var child in parent.orderDetails.ToList())
                    //            db.orderDetails.Remove(child);
                    //    }
                    //    else
                    //    {
                    //        return Ok("This record is not delivered at the moment");
                    //    }
                    //}

                    // var parent = db.addresses.Include(p => p.orderDetails)
                    // .SingleOrDefault(p => p.addressId == id);

                    // foreach (var child in parent.orderDetails.ToList())
                    //     db.orderDetails.Remove(child);

                    //int check = db.SaveChanges();

                    // if (check > 0 || check == 0)
                    // {

                    //     if (address == null)
                    //     {
                    //         return NotFound();
                    //     }

                    //  
                    //     db.SaveChanges();
                    // }


                }
                else
                {
                    return NotFound();
                }
            }
           
        }


        private bool addressExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.addresses.Count(e => e.addressId == id) > 0;
            }
        }


    }
}