﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class productsController : ApiController
    {
        [HttpPost]
        public IHttpActionResult UpdateProduct(NewProductViewModel vm)
        {
            using (Entities db = new Entities())
            {

                if (vm.product.skuNo != "" && vm.product.skuNo != null &&  vm.product.productId != 0 && vm.product.vendorId != 0)
                {
                    try
                    {
                        var product = db.products.Where(pro => pro.skuNo == vm.product.skuNo && pro.productId != vm.product.productId).FirstOrDefault();
                        if (product != null)
                        {
                            return Ok(new { status = "exist", message = "this sku no is already exist." });
                        }
                        else 
                        {
                            List<decimal> prices = new List<decimal>();
                            foreach (var item in vm.cakeLayers)
                            {
                                prices.Add((decimal)item.price);
                            }
                            vm.product.minimumPrice = prices.Min();
                            vm.product.price = prices.Min();
                            db.Entry(vm.product).State = EntityState.Modified;
                            int check = db.SaveChanges();

                            if (check > 0)
                            {
                                long proId = vm.product.productId;

                                try
                                {
                                    if (true)
                                    {
                                       

                                    }


                                    
                                    if (true)
                                    {
                                        try
                                        {


                                          




                                           

                                            if (true)
                                            {
                                                try
                                                {
                                                   
                                                    if (true)
                                                    {
                                                       
                                                        db.cakeLayers.RemoveRange(db.cakeLayers.Where(x => x.productId == vm.product.productId));
                                                        db.SaveChanges();
                                                        if (vm.product.totalLayer > 1)
                                                        {
                                                            foreach (var item in vm.cakeLayers)
                                                            {
                                                                item.productId = vm.product.productId;
                                                                item.vendorId = vm.product.vendorId;
                                                            }

                                                            db.cakeLayers.AddRange(vm.cakeLayers);

                                                            int cl = db.SaveChanges();
                                                            if (cl > 0)
                                                            {
                                                                if (vm.cakeImages.images.Length != 0)
                                                                {
                                                                    try
                                                                    {
                                                                        db.productImgs.RemoveRange(db.productImgs.Where(x => x.productId == proId));
                                                                        db.SaveChanges();

                                                                        for (int i = 0; i < vm.cakeImages.images.Length; i++)
                                                                        {
                                                                            productImg f = new productImg();
                                                                            f.productId = proId;
                                                                            f.imagePath = vm.cakeImages.images[i];
                                                                            db.productImgs.Add(f);

                                                                        }

                                                                        int a = db.SaveChanges();
                                                                        if (a > 0)
                                                                        {

                                                                            return Ok(new { data = "updated", status = "success" });
                                                                        }
                                                                        else
                                                                        {
                                                                            return Ok(new { data = "something went worng on updating images", status = "error" });
                                                                        }
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        return Ok(ex);
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    return Ok(new { data = "data updated without images", status = "success" });
                                                                }

                                                            }

                                                        }
                                                        else
                                                        {
                                                            if (vm.cakeImages.images.Length != 0)
                                                            {
                                                                try
                                                                {
                                                                    db.productImgs.RemoveRange(db.productImgs.Where(x => x.productId == proId));
                                                                    db.SaveChanges();

                                                                    for (int i = 0; i < vm.cakeImages.images.Length; i++)
                                                                    {
                                                                        productImg f = new productImg();
                                                                        f.productId = proId;
                                                                        f.imagePath = vm.cakeImages.images[i];
                                                                        db.productImgs.Add(f);

                                                                    }

                                                                    int a = db.SaveChanges();
                                                                    if (a > 0)
                                                                    {

                                                                        return Ok(new { data = "updated", status = "success" });
                                                                    }
                                                                    else
                                                                    {
                                                                        return Ok(new { data = "something went worng on updating images", status = "error" });
                                                                    }
                                                                }
                                                                catch (Exception ex)
                                                                {
                                                                    return Ok(ex);
                                                                }
                                                            }
                                                            else
                                                            {
                                                                return Ok(new { data = "data updated without images", status = "success" });
                                                            }
                                                        }



                                                    }


                                                }
                                                catch (Exception)
                                                {
                                                    throw;
                                                }





                                            }
                                      

                                        }
                                        catch (Exception)
                                        {

                                            throw;
                                        }
                                    }
                                    

                                }
                                catch (Exception ex)
                                {
                                    return Ok(ex);
                                }
                            }
                            else
                            {
                                return Ok(new { data = "something went worng on updating product", status = "error" });
                            }
                        }
                    
                    }


                    catch (DbUpdateConcurrencyException)
                    {
                        if (!productExists(vm.product.productId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    return Ok(new { data = "something went wrong", status = "error" });
                }
                return Ok(new { data = "something went wrong", status = "error" });
            }


        }
        protected static object cakeSizesList<cakeSize>(IEnumerable<cakeSize> list)
        {


            Entities db = new Entities();

            try
            {
                db.cakeSizes.AddRange((IEnumerable<Model.cakeSize>)list);

                int check = db.SaveChanges();
                if (check > 0)
                {
                    return new { data = "added", status = "success" };
                }
                else
                {
                    return new { data = "something went wrong", status = "error" };
                }
            }
            catch (Exception ex)
            {
                return ex;
            }


        }

        [HttpPost]
        public IHttpActionResult AddProduct(NewProductViewModel vm)
        {
            using (Entities db = new Entities())
            {

                if (vm.product.skuNo != "" && vm.product.skuNo != null && vm.product.productId == 0 && vm.product.vendorId != 0)
                {
                    try
                    {
                        var product = db.products.Where(pro => pro.skuNo == vm.product.skuNo).FirstOrDefault();
                        if (product != null)
                        {
                            return Ok(new { status = "exist", message = "this sku no is already exist." });
                        }
                        else 
                        {
                            vm.product.isApproved = "pending";
                            vm.product.approvedOn = DateTime.Now;

                            if (vm.product.keywords == null)
                            {
                                vm.product.keywords = "";
                            }
                            List<decimal> prices = new  List<decimal>();
                            foreach (var item in vm.cakeLayers)
                            {
                                prices.Add((decimal)item.price);
                            }
                            vm.product.minimumPrice = prices.Min();
                            vm.product.price = prices.Min();
                            db.products.Add(vm.product);

                            int check = db.SaveChanges();

                            if (check > 0)
                            {
                                long proId = vm.product.productId;
                                var myproduct = db.products.Where(x => x.productId == proId).FirstOrDefault();

                                myproduct.url = "https://okaaik-289b0.web.app?product=" + myproduct.productId;
                                db.Entry(myproduct).State = EntityState.Modified;
                                db.SaveChanges();

                                try
                                {
                                    if (proId != 0)
                                    {
                                        try
                                        {
                                            
                                            if (true)
                                            {
                                                try
                                                {
                                                    if (proId != 0)
                                                    {
                                                        



                                                        if (true)
                                                        {
                                                            try
                                                            {
                                                                

                                                                
                                                                if (true)
                                                                {



                                                                    if (vm.product.totalLayer > 1)
                                                                    {
                                                                        foreach (var item in vm.cakeLayers)
                                                                        {
                                                                            item.productId = proId;
                                                                            item.vendorId = vm.product.vendorId;

                                                                        }


                                                                        db.cakeLayers.AddRange(vm.cakeLayers);

                                                                        int chc = db.SaveChanges();
                                                                        if (chc > 0)
                                                                        {
                                                                            if (vm.cakeImages.images.Length != 0)
                                                                            {
                                                                                try
                                                                                {
                                                                                    vm.product.coverImg = vm.cakeImages.images[0];
                                                                                    db.SaveChanges();
                                                                                    for (int i = 0; i < vm.cakeImages.images.Length; i++)
                                                                                    {
                                                                                        productImg f = new productImg();
                                                                                        f.productId = proId;
                                                                                        f.imagePath = vm.cakeImages.images[i];
                                                                                        db.productImgs.Add(f);

                                                                                    }

                                                                                    int a = db.SaveChanges();
                                                                                    if (a > 0)
                                                                                    {

                                                                                        return Ok(new { data = "added", status = "success" });
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        return Ok(new { data = "something went worng on adding images", status = "error" });
                                                                                    }
                                                                                }
                                                                                catch (Exception ex)
                                                                                {
                                                                                    return Ok(ex);
                                                                                }
                                                                            }
                                                                            else
                                                                            {
                                                                                return Ok(new { data = "data added with layer without images", status = "success" });
                                                                            }

                                                                        }

                                                                    }
                                                                    else
                                                                    {
                                                                        if (vm.cakeImages.images.Length != 0)
                                                                        {
                                                                            try
                                                                            {
                                                                                vm.product.coverImg = vm.cakeImages.images[0];
                                                                                db.SaveChanges();
                                                                                for (int i = 0; i < vm.cakeImages.images.Length; i++)
                                                                                {
                                                                                    productImg f = new productImg();
                                                                                    f.productId = proId;
                                                                                    f.imagePath = vm.cakeImages.images[i];
                                                                                    db.productImgs.Add(f);

                                                                                }

                                                                                int a = db.SaveChanges();
                                                                                if (a > 0)
                                                                                {

                                                                                    return Ok(new { data = "added", status = "success" });
                                                                                }
                                                                                else
                                                                                {
                                                                                    return Ok(new { data = "something went worng on adding images", status = "error" });
                                                                                }
                                                                            }
                                                                            catch (Exception ex)
                                                                            {
                                                                                return Ok(ex);
                                                                            }
                                                                        }
                                                                        else
                                                                        {
                                                                            return Ok(new { data = "data added without images", status = "success" });
                                                                        }
                                                                    }






                                                                }
                                                               
                                                            }
                                                            catch (Exception)
                                                            {
                                                                throw;
                                                            }





                                                        }

                                                    }
                                                }
                                                catch (Exception)
                                                {

                                                    throw;
                                                }
                                            }
                                           
                                        }
                                        catch (Exception)
                                        {

                                            throw;
                                        }

                                    }





                                }
                                catch (Exception ex)
                                {
                                    return Ok(ex);
                                }
                            }
                            else
                            {
                                return Ok(new { data = "something went worng on adding product", status = "error" });
                            }
                        }
                       
                    }


                    catch (DbUpdateConcurrencyException)
                    {
                        if (!productExists(vm.product.productId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }
                else
                {
                    return Ok(new { data = "something went wrong", status = "error" });
                }
                return Ok(new { data = "something went wrong", status = "error" });
            }

        }

        [HttpGet]
        public IHttpActionResult GetBySKU(string sku) 
        {
            using (Entities db = new Entities())
            {
                var product = db.products.Where(pro => pro.skuNo == sku).Select(product => product.productId).FirstOrDefault();
                if (product != 0)
                {
                    return Ok(new { status = "success", data=product});
                }
                else
                {
                    return Ok(new { status = "not exist", data = "this sku no is not exist." });

                }
            }
            
        }

        [HttpGet]
        public IHttpActionResult GetSizes()
        {
            using (Entities db = new Entities())
            {
                var product = db.cakeSizes.Select(x=> x.size).Distinct().ToList();
                if (product != null)
                {
                    return Ok(new { status = "success", data = product });
                }
                else
                {
                    return Ok(new { status = "not exist", data = "this sku no is not exist." });

                }
            }

        }

        [HttpGet]
        public IHttpActionResult GetByOccasion(long id) 
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                             where pro.occasionId == id
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.subCategoryId,
                                 pro.skuNo
                             }).ToList();


                return  Ok(new {status="success",data=query });
            }
        }


        [HttpGet]
        public IHttpActionResult Paggination(int page)
        {
            Entities db = new Entities();
            {
             var products  =  db.products.Select(x => new {x.productId  }).OrderBy(x=> x.productId).Skip(page).Take(10).ToList();
                return Ok( new { status = "success", data = products });
            }
        }



        [HttpPut]
        public IHttpActionResult productStatus(PStatusViewModel vm)
        {
            using (Entities db = new Entities())
            {
                var row = db.products.AsNoTracking().Where(pro => pro.productId == vm.id).FirstOrDefault();
                if (row != null)
                {
                    if (vm.status == "true")
                    {
                        row.isApproved = "true";

                        try
                        {
                            db.Entry(row).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });

                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    else if (vm.status == "false")
                    {
                        row.isApproved = "false";
                        row.reviewNote = vm.note;
                        try
                        {
                            db.Entry(row).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    else if (vm.status == "suspended")
                    {
                        row.isApproved = "suspended";
                        try
                        {
                            db.Entry(row).State = EntityState.Modified;
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { data = "updated", status = "success" });
                            }
                            else
                            {
                                return Ok(new { data = "something went wrong", status = "error" });
                            }
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                    else
                    {
                        return Ok(new { data = "something went wrong", status = "error" });
                    }

                }
                else
                {
                    return NotFound();
                }


            }
        }


        [HttpPost]
        public IHttpActionResult FilterWithWishList(FilterWishListVieModel vm)
        {

            DateTime day = vm.Date;
            DateTime dt = DateTime.Today;
            //DateTime dt2 = DateTime.Today.AddDays(day);
            int mini = (day - dt).Days;



            Entities db = new Entities();
            {
                var filter = (from pro in db.products
                              where pro.productId == -1
                              select new
                              {
                                  pro.productId,
                                  pro.title,
                                  pro.price,
                                  pro.categoryId,
                                  pro.description,
                                  pro.size,
                                  pro.vendorId,
                                  pro.persons,
                                  pro.weight,
                                  pro.approvedOn,
                                  pro.isApproved,
                                  pro.coverImg,
                                  pro.minimumTime,
                                  pro.perLayerCost,
                                  pro.dietryId,
                              });

                if (vm.Date != null && vm.Date.Year > 2020)
                {
                    //filter by date 

                    var bydate = (from pro in db.products
                                  where pro.minimumTime <= mini
                                  select new
                                  {
                                      pro.productId,
                                      pro.title,
                                      pro.price,
                                      pro.categoryId,
                                      pro.description,
                                      pro.size,
                                      pro.vendorId,
                                      pro.persons,
                                      pro.weight,
                                      pro.approvedOn,
                                      pro.isApproved,
                                      pro.coverImg,
                                      pro.minimumTime,
                                      pro.perLayerCost,
                                      pro.dietryId,

                                  });


                    filter = bydate;

                   

                    if (vm.Cities != null)
                    {
                        //filter by date  city
                        var byDCity = (from pro in bydate
                                       join vn in db.vendors
                                       on pro.vendorId equals vn.vendorId
                                       where vm.Cities.Contains(vn.city)
                                       select new
                                       {
                                           pro.productId,
                                           pro.title,
                                           pro.price,
                                           pro.categoryId,
                                           pro.description,
                                           pro.size,
                                           pro.vendorId,
                                           pro.persons,
                                           pro.weight,
                                           pro.approvedOn,
                                           pro.isApproved,
                                           pro.coverImg,
                                           pro.minimumTime,
                                           pro.perLayerCost,
                                           pro.dietryId,

                                       });
                        filter = byDCity;

                      

                        if (vm.Categories != null)
                        {
                            //filter by date  category city
                            var byDCCategory = (from pro in byDCity
                                                join ct in db.categories
                                                on pro.categoryId equals ct.categoryId
                                                where vm.Categories.Contains(ct.Name)
                                                select new
                                                {
                                                    pro.productId,
                                                    pro.title,
                                                    pro.price,
                                                    pro.categoryId,
                                                    pro.description,
                                                    pro.size,
                                                    pro.vendorId,
                                                    pro.persons,
                                                    pro.weight,
                                                    pro.approvedOn,
                                                    pro.isApproved,
                                                    pro.coverImg,
                                                    pro.minimumTime,
                                                    pro.perLayerCost,
                                                    pro.dietryId,

                                                });
                            filter = byDCCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by date  category city bakery
                                var byDCCBakery = (from pro in byDCCategory
                                                   join vn in db.vendors
                                                   on pro.vendorId equals vn.vendorId
                                                   where vm.Bakeries.Contains(vn.bakeryName)
                                                   select new
                                                   {
                                                       pro.productId,
                                                       pro.title,
                                                       pro.price,
                                                       pro.categoryId,
                                                       pro.description,
                                                       pro.size,
                                                       pro.vendorId,
                                                       pro.persons,
                                                       pro.weight,
                                                       pro.approvedOn,
                                                       pro.isApproved,
                                                       pro.coverImg,
                                                       pro.minimumTime,
                                                       pro.perLayerCost,
                                                       pro.dietryId,

                                                   });
                                filter = byDCCBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byDCCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                                }
                            }
                    }
                    else
                    {


                        if (vm.Categories != null)
                        {
                            //filter by date category 
                            var byDCategory = (from pro in bydate
                                               join ct in db.categories
                                               on pro.categoryId equals ct.categoryId
                                               where vm.Categories.Contains(ct.Name)
                                               select new
                                               {
                                                   pro.productId,
                                                   pro.title,
                                                   pro.price,
                                                   pro.categoryId,
                                                   pro.description,
                                                   pro.size,
                                                   pro.vendorId,
                                                   pro.persons,
                                                   pro.weight,
                                                   pro.approvedOn,
                                                   pro.isApproved,
                                                   pro.coverImg,
                                                   pro.minimumTime,
                                                   pro.perLayerCost,
                                                   pro.dietryId,

                                               });
                            filter = byDCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by date  category  bakery
                                var byDCBakery = (from pro in byDCategory
                                                  join vn in db.vendors
                                                  on pro.vendorId equals vn.vendorId
                                                  where vm.Bakeries.Contains(vn.bakeryName)
                                                  select new
                                                  {
                                                      pro.productId,
                                                      pro.title,
                                                      pro.price,
                                                      pro.categoryId,
                                                      pro.description,
                                                      pro.size,
                                                      pro.vendorId,
                                                      pro.persons,
                                                      pro.weight,
                                                      pro.approvedOn,
                                                      pro.isApproved,
                                                      pro.coverImg,
                                                      pro.minimumTime,
                                                      pro.perLayerCost,
                                                      pro.dietryId,

                                                  });
                                filter = byDCBakery;


                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in bydate
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }
                                }
                        }
                        else
                        {
                            if (vm.Bakeries != null)
                            {
                                //filter by date and  bakery
                                var byDBakery = (from pro in bydate
                                                 join vn in db.vendors
                                                 on pro.vendorId equals vn.vendorId
                                                 where vm.Bakeries.Contains(vn.bakeryName)
                                                 select new
                                                 {
                                                     pro.productId,
                                                     pro.title,
                                                     pro.price,
                                                     pro.categoryId,
                                                     pro.description,
                                                     pro.size,
                                                     pro.vendorId,
                                                     pro.persons,
                                                     pro.weight,
                                                     pro.approvedOn,
                                                     pro.isApproved,
                                                     pro.coverImg,
                                                     pro.minimumTime,
                                                     pro.perLayerCost,
                                                     pro.dietryId,

                                                 });
                                filter = byDBakery;



                            }
                            else
                            {
                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in bydate
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }
                            }
                        }
                    }
                }
                else //NO DATE
                {
                    if (vm.Cities != null)
                    {
                        //filter by  city
                        var byCity = (from pro in db.products
                                      join vn in db.vendors
                                      on pro.vendorId equals vn.vendorId
                                      where vm.Cities.Contains(vn.city)
                                      select new
                                      {
                                          pro.productId,
                                          pro.title,
                                          pro.price,
                                          pro.categoryId,
                                          pro.description,
                                          pro.size,
                                          pro.vendorId,
                                          pro.persons,
                                          pro.weight,
                                          pro.approvedOn,
                                          pro.isApproved,
                                          pro.coverImg,
                                          pro.minimumTime,
                                          pro.perLayerCost,
                                          pro.dietryId,

                                      });
                        filter = byCity;



                        if (vm.Categories != null)
                        {
                            //filter by   category city
                            var byCCategory = (from pro in byCity
                                               join ct in db.categories
                                               on pro.categoryId equals ct.categoryId
                                               where vm.Categories.Contains(ct.Name)
                                               select new
                                               {
                                                   pro.productId,
                                                   pro.title,
                                                   pro.price,
                                                   pro.categoryId,
                                                   pro.description,
                                                   pro.size,
                                                   pro.vendorId,
                                                   pro.persons,
                                                   pro.weight,
                                                   pro.approvedOn,
                                                   pro.isApproved,
                                                   pro.coverImg,
                                                   pro.minimumTime,
                                                   pro.perLayerCost,
                                                   pro.dietryId,

                                               });
                            filter = byCCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by category city bakery
                                var byCCBakery = (from pro in byCCategory
                                                  join vn in db.vendors
                                                  on pro.vendorId equals vn.vendorId
                                                  where vm.Bakeries.Contains(vn.bakeryName)
                                                  select new
                                                  {
                                                      pro.productId,
                                                      pro.title,
                                                      pro.price,
                                                      pro.categoryId,
                                                      pro.description,
                                                      pro.size,
                                                      pro.vendorId,
                                                      pro.persons,
                                                      pro.weight,
                                                      pro.approvedOn,
                                                      pro.isApproved,
                                                      pro.coverImg,
                                                      pro.minimumTime,
                                                      pro.perLayerCost,
                                                      pro.dietryId,

                                                  });
                                filter = byCCBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byCCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }

                        }
                    }
                    else
                    {


                        if (vm.Categories != null)
                        {
                            //filter by  category 
                            var byCategory = (from pro in db.products
                                              join ct in db.categories
                                              on pro.categoryId equals ct.categoryId
                                              where vm.Categories.Contains(ct.Name)
                                              select new
                                              {
                                                  pro.productId,
                                                  pro.title,
                                                  pro.price,
                                                  pro.categoryId,
                                                  pro.description,
                                                  pro.size,
                                                  pro.vendorId,
                                                  pro.persons,
                                                  pro.weight,
                                                  pro.approvedOn,
                                                  pro.isApproved,
                                                  pro.coverImg,
                                                  pro.minimumTime,
                                                  pro.perLayerCost,
                                                  pro.dietryId,

                                              });
                            filter = byCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by   category  bakery
                                var byCBakery = (from pro in byCategory
                                                 join vn in db.vendors
                                                 on pro.vendorId equals vn.vendorId
                                                 where vm.Bakeries.Contains(vn.bakeryName)
                                                 select new
                                                 {
                                                     pro.productId,
                                                     pro.title,
                                                     pro.price,
                                                     pro.categoryId,
                                                     pro.description,
                                                     pro.size,
                                                     pro.vendorId,
                                                     pro.persons,
                                                     pro.weight,
                                                     pro.approvedOn,
                                                     pro.isApproved,
                                                     pro.coverImg,
                                                     pro.minimumTime,
                                                     pro.perLayerCost,
                                                     pro.dietryId,

                                                 });
                                filter = byCBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }
                        }
                        else
                        {
                            if (vm.Bakeries != null)
                            {
                                //filter by    bakery
                                var byBakery = (from pro in db.products
                                                join vn in db.vendors
                                                on pro.vendorId equals vn.vendorId
                                                where vm.Bakeries.Contains(vn.bakeryName)
                                                select new
                                                {
                                                    pro.productId,
                                                    pro.title,
                                                    pro.price,
                                                    pro.categoryId,
                                                    pro.description,
                                                    pro.size,
                                                    pro.vendorId,
                                                    pro.persons,
                                                    pro.weight,
                                                    pro.approvedOn,
                                                    pro.isApproved,
                                                    pro.coverImg,
                                                    pro.minimumTime,
                                                    pro.perLayerCost,
                                                    pro.dietryId,

                                                });
                                filter = byBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }


                            }
                            if (vm.dietryId != null)
                            {
                                var byDietry = (from pro in db.products
                                                where vm.dietryId.Contains(pro.dietryId)
                                                select new
                                                {
                                                    pro.productId,
                                                    pro.title,
                                                    pro.price,
                                                    pro.categoryId,
                                                    pro.description,
                                                    pro.size,
                                                    pro.vendorId,
                                                    pro.persons,
                                                    pro.weight,
                                                    pro.approvedOn,
                                                    pro.isApproved,
                                                    pro.coverImg,
                                                    pro.minimumTime,
                                                    pro.perLayerCost,
                                                    pro.dietryId

                                                });
                                filter = byDietry;
                            }
                        }
                    }
                }

                var query = (from ws in db.wishLists
                             where ws.userId == vm.userid || ws.deviceId == vm.DeviceId
                             join pro in db.products
                             on ws.productId equals pro.productId
                             select new
                             {
                                 ws.wishId,
                                 ws.productId,
                                 ws.userId,
                                 ws.deviceId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.perLayerCost,
                                 pro.dietryId,
                             }).ToList();



                if (filter.ToList().Count > 0)
                {
                    return Ok(new { status = "success", products = filter.ToList() , wishList = query });
                }
                else
                {
                    return Ok(new { status = "Not Found", wishList = query });
                }


            }





        }


        [HttpPost]
        public IHttpActionResult FilterProducts(FilterViewModel vm)
        {

            DateTime day = vm.Date;
            DateTime dt = DateTime.Today;
            //DateTime dt2 = DateTime.Today.AddDays(day);
            int mini = (day - dt).Days;



            Entities db = new Entities();
            {
                var filter = (from pro in db.products
                              where pro.productId == -1
                              select new
                              {
                                  pro.productId,
                                  pro.title,
                                  pro.price,
                                  pro.categoryId,
                                  pro.description,
                                  pro.size,
                                  pro.vendorId,
                                  pro.persons,
                                  pro.weight,
                                  pro.approvedOn,
                                  pro.isApproved,
                                  pro.coverImg,
                                  pro.minimumTime,
                                  pro.perLayerCost,
                                  pro.dietryId,
                              });

                if (vm.Date != null && vm.Date.Year > 2020)
                {
                    //filter by date 

                    var bydate = (from pro in db.products
                                  where pro.minimumTime <= mini
                                  select new
                                  {
                                      pro.productId,
                                      pro.title,
                                      pro.price,
                                      pro.categoryId,
                                      pro.description,
                                      pro.size,
                                      pro.vendorId,
                                      pro.persons,
                                      pro.weight,
                                      pro.approvedOn,
                                      pro.isApproved,
                                      pro.coverImg,
                                      pro.minimumTime,
                                      pro.perLayerCost,
                                      pro.dietryId,


                                  });


                    filter = bydate;

                    if (vm.Cities != null)
                    {
                        //filter by date  city
                        var byDCity = (from pro in bydate
                                       join vn in db.vendors
                                       on pro.vendorId equals vn.vendorId
                                       where vm.Cities.Contains(vn.city)
                                       select new
                                       {
                                           pro.productId,
                                           pro.title,
                                           pro.price,
                                           pro.categoryId,
                                           pro.description,
                                           pro.size,
                                           pro.vendorId,
                                           pro.persons,
                                           pro.weight,
                                           pro.approvedOn,
                                           pro.isApproved,
                                           pro.coverImg,
                                           pro.minimumTime,
                                           pro.perLayerCost,
                                           pro.dietryId,


                                       });
                        filter = byDCity;

                        if (vm.Categories != null)
                        {
                            //filter by date  category city
                            var byDCCategory = (from pro in byDCity
                                                join ct in db.categories
                                                on pro.categoryId equals ct.categoryId
                                                where vm.Categories.Contains(ct.Name)
                                                select new
                                                {
                                                    pro.productId,
                                                    pro.title,
                                                    pro.price,
                                                    pro.categoryId,
                                                    pro.description,
                                                    pro.size,
                                                    pro.vendorId,
                                                    pro.persons,
                                                    pro.weight,
                                                    pro.approvedOn,
                                                    pro.isApproved,
                                                    pro.coverImg,
                                                    pro.minimumTime,
                                                    pro.perLayerCost,
                                                    pro.dietryId,


                                                });
                            filter = byDCCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by date  category city bakery
                                var byDCCBakery = (from pro in byDCCategory
                                                   join vn in db.vendors
                                                   on pro.vendorId equals vn.vendorId
                                                   where vm.Bakeries.Contains(vn.bakeryName)
                                                   select new
                                                   {
                                                       pro.productId,
                                                       pro.title,
                                                       pro.price,
                                                       pro.categoryId,
                                                       pro.description,
                                                       pro.size,
                                                       pro.vendorId,
                                                       pro.persons,
                                                       pro.weight,
                                                       pro.approvedOn,
                                                       pro.isApproved,
                                                       pro.coverImg,
                                                       pro.minimumTime,
                                                       pro.perLayerCost,
                                                       pro.dietryId,



                                                   });
                                filter = byDCCBakery;


                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byDCCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }


                        }
                    }
                    else
                    {


                        if (vm.Categories != null)
                        {
                            //filter by date category 
                            var byDCategory = (from pro in bydate
                                               join ct in db.categories
                                               on pro.categoryId equals ct.categoryId
                                               where vm.Categories.Contains(ct.Name)
                                               select new
                                               {
                                                   pro.productId,
                                                   pro.title,
                                                   pro.price,
                                                   pro.categoryId,
                                                   pro.description,
                                                   pro.size,
                                                   pro.vendorId,
                                                   pro.persons,
                                                   pro.weight,
                                                   pro.approvedOn,
                                                   pro.isApproved,
                                                   pro.coverImg,
                                                   pro.minimumTime,
                                                   pro.perLayerCost,
                                                   pro.dietryId,

                                               });
                            filter = byDCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by date  category  bakery
                                var byDCBakery = (from pro in byDCategory
                                                  join vn in db.vendors
                                                  on pro.vendorId equals vn.vendorId
                                                  where vm.Bakeries.Contains(vn.bakeryName)
                                                  select new
                                                  {
                                                      pro.productId,
                                                      pro.title,
                                                      pro.price,
                                                      pro.categoryId,
                                                      pro.description,
                                                      pro.size,
                                                      pro.vendorId,
                                                      pro.persons,
                                                      pro.weight,
                                                      pro.approvedOn,
                                                      pro.isApproved,
                                                      pro.coverImg,
                                                      pro.minimumTime,
                                                      pro.perLayerCost,
                                                      pro.dietryId,


                                                  });
                                filter = byDCBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byDCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }
                        }
                        else
                        {
                            if (vm.Bakeries != null)
                            {
                                //filter by date and  bakery
                                var byDBakery = (from pro in bydate
                                                 join vn in db.vendors
                                                 on pro.vendorId equals vn.vendorId
                                                 where vm.Bakeries.Contains(vn.bakeryName)
                                                 select new
                                                 {
                                                     pro.productId,
                                                     pro.title,
                                                     pro.price,
                                                     pro.categoryId,
                                                     pro.description,
                                                     pro.size,
                                                     pro.vendorId,
                                                     pro.persons,
                                                     pro.weight,
                                                     pro.approvedOn,
                                                     pro.isApproved,
                                                     pro.coverImg,
                                                     pro.minimumTime,
                                                     pro.perLayerCost,
                                                     pro.dietryId,

                                                 });
                                filter = byDBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byDBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }



                            }
                            else 
                            {
                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in db.products
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }
                        }
                    }
                }
                else //NO DATE
                {
                    if (vm.Cities != null)
                    {
                        //filter by  city
                        var byCity = (from pro in db.products
                                      join vn in db.vendors
                                      on pro.vendorId equals vn.vendorId
                                      where vm.Cities.Contains(vn.city)
                                      select new
                                      {
                                          pro.productId,
                                          pro.title,
                                          pro.price,
                                          pro.categoryId,
                                          pro.description,
                                          pro.size,
                                          pro.vendorId,
                                          pro.persons,
                                          pro.weight,
                                          pro.approvedOn,
                                          pro.isApproved,
                                          pro.coverImg,
                                          pro.minimumTime,
                                          pro.perLayerCost,
                                          pro.dietryId,

                                      });
                        filter = byCity;

                        if (vm.Categories != null)
                        {
                            //filter by   category city
                            var byCCategory = (from pro in byCity
                                               join ct in db.categories
                                               on pro.categoryId equals ct.categoryId
                                               where vm.Categories.Contains(ct.Name)
                                               select new
                                               {
                                                   pro.productId,
                                                   pro.title,
                                                   pro.price,
                                                   pro.categoryId,
                                                   pro.description,
                                                   pro.size,
                                                   pro.vendorId,
                                                   pro.persons,
                                                   pro.weight,
                                                   pro.approvedOn,
                                                   pro.isApproved,
                                                   pro.coverImg,
                                                   pro.minimumTime,
                                                   pro.perLayerCost,
                                                   pro.dietryId,


                                               });
                            filter = byCCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by category city bakery
                                var byCCBakery = (from pro in byCCategory
                                                  join vn in db.vendors
                                                  on pro.vendorId equals vn.vendorId
                                                  where vm.Bakeries.Contains(vn.bakeryName)
                                                  select new
                                                  {
                                                      pro.productId,
                                                      pro.title,
                                                      pro.price,
                                                      pro.categoryId,
                                                      pro.description,
                                                      pro.size,
                                                      pro.vendorId,
                                                      pro.persons,
                                                      pro.weight,
                                                      pro.approvedOn,
                                                      pro.isApproved,
                                                      pro.coverImg,
                                                      pro.minimumTime,
                                                      pro.perLayerCost,
                                                      pro.dietryId,


                                                  });
                                filter = byCCBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byCCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }
                        }
                    }
                    else
                    {


                        if (vm.Categories != null)
                        {
                            //filter by  category 
                            var byCategory = (from pro in db.products
                                              join ct in db.categories
                                              on pro.categoryId equals ct.categoryId
                                              where vm.Categories.Contains(ct.Name)
                                              select new
                                              {
                                                  pro.productId,
                                                  pro.title,
                                                  pro.price,
                                                  pro.categoryId,
                                                  pro.description,
                                                  pro.size,
                                                  pro.vendorId,
                                                  pro.persons,
                                                  pro.weight,
                                                  pro.approvedOn,
                                                  pro.isApproved,
                                                  pro.coverImg,
                                                  pro.minimumTime,
                                                  pro.perLayerCost,
                                                  pro.dietryId,

                                              });
                            filter = byCategory;

                            if (vm.Bakeries != null)
                            {
                                //filter by   category  bakery
                                var byCBakery = (from pro in byCategory
                                                 join vn in db.vendors
                                                 on pro.vendorId equals vn.vendorId
                                                 where vm.Bakeries.Contains(vn.bakeryName)
                                                 select new
                                                 {
                                                     pro.productId,
                                                     pro.title,
                                                     pro.price,
                                                     pro.categoryId,
                                                     pro.description,
                                                     pro.size,
                                                     pro.vendorId,
                                                     pro.persons,
                                                     pro.weight,
                                                     pro.approvedOn,
                                                     pro.isApproved,
                                                     pro.coverImg,
                                                     pro.minimumTime,
                                                     pro.perLayerCost,
                                                     pro.dietryId,

                                                 });
                                filter = byCBakery;


                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byCBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }

                            }
                        }
                        else
                        {
                            if (vm.Bakeries != null)
                            {
                                //filter by    bakery
                                var byBakery = (from pro in db.products
                                                join vn in db.vendors
                                                on pro.vendorId equals vn.vendorId
                                                where vm.Bakeries.Contains(vn.bakeryName)
                                                select new
                                                {
                                                    pro.productId,
                                                    pro.title,
                                                    pro.price,
                                                    pro.categoryId,
                                                    pro.description,
                                                    pro.size,
                                                    pro.vendorId,
                                                    pro.persons,
                                                    pro.weight,
                                                    pro.approvedOn,
                                                    pro.isApproved,
                                                    pro.coverImg,
                                                    pro.minimumTime,
                                                    pro.perLayerCost,
                                                    pro.dietryId,

                                                });
                                filter = byBakery;

                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in byBakery
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }



                            }
                            else 
                            {
                                if (vm.dietryId != null)
                                {
                                    //filter by date dietry
                                    var byDietry = (from pro in db.products
                                                    where vm.dietryId.Contains(pro.dietryId)
                                                    select new
                                                    {
                                                        pro.productId,
                                                        pro.title,
                                                        pro.price,
                                                        pro.categoryId,
                                                        pro.description,
                                                        pro.size,
                                                        pro.vendorId,
                                                        pro.persons,
                                                        pro.weight,
                                                        pro.approvedOn,
                                                        pro.isApproved,
                                                        pro.coverImg,
                                                        pro.minimumTime,
                                                        pro.perLayerCost,
                                                        pro.dietryId

                                                    });
                                    filter = byDietry;
                                }
                            }
                        }
                    }
                }
                

               
                   

                
             
                if (filter.ToList().Count > 0)
                {
                    return Ok(new { status = "success", products = filter.ToList() });
                }
                else
                {
                    return Ok(new { status = "Not Found" });
                }
               

            }





        }

        [HttpPost]
        public IHttpActionResult NewFilter(mNewFilter data)
        {
            Entities db = new Entities();
            {
                if (data.cost > 0 && data.size != null)
                {

                    var prosIDs = (from us in db.cakeSizes
                                 where us.size == data.size
                                 && us.cost <= data.cost
                                   select us.productId).Distinct().ToList();

                    var filter = (from pro in db.products                                  
                                  where prosIDs.Contains(pro.productId)  
                                  select new
                                  {
                                      pro.productId,
                                      pro.title,
                                      price = db.cakeSizes.Where(x=> x.productId == pro.productId && x.size == data.size).Select(y=> y.cost).FirstOrDefault(),
                                      pro.categoryId,
                                      pro.description,
                                      pro.size,
                                      pro.vendorId,
                                      pro.persons,
                                      pro.weight,
                                      pro.approvedOn,
                                      pro.isApproved,
                                      pro.coverImg,
                                      pro.minimumTime,
                                      pro.perLayerCost,
                                      pro.dietryId,
                                  });

                    if (filter.ToList().Count > 0)
                    {
                        return Ok(new { status = "success", products = filter.ToList() });
                    }
                    else
                    {
                        return Ok(new { status = "Not Found" });
                    }
                }
                else if (data.cost > 0 && data.size == null) //By Cost
                {
                    var prosIDs = (from us in db.cakeSizes                           
                                   where 
                                   us.cost <= data.cost
                                   select us.productId).Distinct().ToList();

                    var filter = (from pro in db.products
                                  where prosIDs.Contains(pro.productId)
                                  select new
                                  {
                                      pro.productId,
                                      pro.title,
                                      price = db.cakeSizes.Where(x => x.productId == pro.productId).Select(y => y.cost).FirstOrDefault(),
                                      pro.categoryId,
                                      pro.description,
                                      pro.size,
                                      pro.vendorId,
                                      pro.persons,
                                      pro.weight,
                                      pro.approvedOn,
                                      pro.isApproved,
                                      pro.coverImg,
                                      pro.minimumTime,
                                      pro.perLayerCost,
                                      pro.dietryId,
                                  });

                    if (filter.ToList().Count > 0)
                    {
                        return Ok(new { status = "success", products = filter.ToList() });
                    }
                    else
                    {
                        return Ok(new { status = "Not Found" });
                    }
                }
                else if (data.cost == 0 && data.size != null) //By Size
                {
                    var prosIDs = (from us in db.cakeSizes
                                   where us.size == data.size                                   
                                   select us.productId).Distinct().ToList();

                    var filter = (from pro in db.products
                                  where prosIDs.Contains(pro.productId)
                                  select new
                                  {
                                      pro.productId,
                                      pro.title,
                                      price = db.cakeSizes.Where(x => x.productId == pro.productId && x.size == data.size).Select(y => y.cost).FirstOrDefault(),
                                      pro.categoryId,
                                      pro.description,
                                      pro.size,
                                      pro.vendorId,
                                      pro.persons,
                                      pro.weight,
                                      pro.approvedOn,
                                      pro.isApproved,
                                      pro.coverImg,
                                      pro.minimumTime,
                                      pro.perLayerCost,
                                      pro.dietryId,
                                  });

                    if (filter.ToList().Count > 0)
                    {
                        return Ok(new { status = "success", products = filter.ToList() });
                    }
                    else
                    {
                        return Ok(new { status = "Not Found" });
                    }
                }
                return Ok(new { status = "No filter matched." });

            }

        }



        //"2021-06-19T00:00:00+05:00"

        public static List<long> GetLists()
        {
            Entities db = new Entities();
            {
                var result = (from s in db.orderDetails
                              join pro in db.products
                              on s.productId equals pro.productId
                              // define the data source
                              group s by s.productId into g  // group all items by status
                              orderby g.Count() descending // order by count descending
                              select new
                              {

                                  g.Key,

                              }) // cast the output
                .Take(5) // take just 5 items
                .ToList();
                List<long> ids = new List<long>();

                for (int i = 0; i < result.Count; i++)
                {

                    ids.Add(result[i].Key.Value);
                }


                return ids;
            }





            //var _result1 = from pro in db.products join i in ids on pro.productId equals i select new
            //{
            //    pro.productId

            //};
            //var _result2 = (db.products.Where(p => ids.Contains(p.productId))).ToList();

            // List<product> _result3 = new List<product>();

            //foreach (long k in ids) { _result3.Add(db.products.Where(j => j.productId == k).SingleOrDefault()); };




        }

        [HttpGet]
        public IHttpActionResult PopularSearches()
        {

            Entities db = new Entities();
            {
                var row = GetLists();
                var products = (from pro in db.products
                                where row.Contains(pro.productId)                         
                                select new
                                {
                                    pro.productId,
                                    pro.title,
                                    pro.price,
                                    pro.categoryId,
                                    pro.description,
                                    pro.size,
                                    pro.vendorId,
                                    pro.persons,
                                    pro.weight,
                                    pro.approvedOn,
                                    pro.isApproved,
                                    pro.coverImg,
                                    pro.minimumTime,
                                    pro.subCategoryId


                                }).AsEnumerable().OrderBy(pro => row.IndexOf(pro.productId)).ToList();

                

                var category = (from bn in db.pSearches

                                select new
                                {
                                    bn.psId,
                                    bn.categoryId,
                                    bn.category

                                }).ToList();

                return Ok(new { status = "success", products = products, category = category });
            }

            //var _result2 = (db.products.Where(p => ids.Contains(p.productId))).ToList();

            // List<product> _result3 = new List<product>();

            //foreach (long k in ids) { _result3.Add(db.products.Where(j => j.productId == k).SingleOrDefault()); };




        }


        [HttpGet]
        public IHttpActionResult Getproductforid(long Id)
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                             where pro.productId == Id
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.subCategoryId,
                                 pro.skuNo
                             }).ToList();


                return Ok(query);
            }
        }



        [HttpGet]
        [Route("api/products/getProducts")]
        public IHttpActionResult Getproducts()
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                             join cat in db.categories
                             on pro.categoryId equals cat.categoryId
                             join ven in db.vendors
                             on pro.vendorId equals ven.vendorId
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.perLayerCost,
                                 cat.Name,
                                 vendorName = ven.bakeryName,
                                 pro.subCategoryId
                             }).ToList();


                return Ok(query);
            }
        }
        [HttpGet]

        public IHttpActionResult GetproductbyId(long id,long vendorId)
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                             join ven in db.vendors
                             on pro.vendorId equals ven.vendorId
                             join cat in db.categories
                             on pro.categoryId equals cat.categoryId
                             where pro.productId == id
                             select new
                             {
                                 ven.bakeryName,
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.vendorId,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.totalLayer,
                                 pro.minimumPrice,
                                 pro.fillingName,
                                 pro.keywords,
                                 cat.Name,
                                 pro.subCategoryId,
                                 pro.skuNo
                             }).ToList();
                //var images = db.productImgs.Where(im => im.productId == id).Select(p => new { p.productImgId, p.productId, p.imagePath }).ToList();

                //var flavours = db.cakeFlavours.Where(im => im.productId == id).Select(p => new { p.cakeFlavourId, p.productId, p.flavourId }).ToList();

                //var cakesize = db.cakeSizes.Where(im => im.productId == id).Select(p => new { p.cakeSizesId, p.productId, p.size ,p.cost}).ToList();

                //var cakeLayer = db.cakeLayers.Where(im => im.productId == id).Select(p => new { p.cakeLayerId, p.cakeLayer1, p.dumyLayer, p.productId,p.vendorId,p.price }).ToList();

                var images = (from pro in db.productImgs
                              where pro.productId == id
                              select new
                              {
                                  pro.productImgId,
                                  pro.productId,
                                  pro.imagePath
                              }).ToList();

                var flavours = (from cf in db.flavours
                                where cf.vendorId == vendorId
                                select new
                                {
                                    cf.flavourId,
                                    flavourName = cf.Name
                                }).ToList();
               
                var cakeLayer = (from cl in db.cakeLayers
                                 where cl.productId == id
                                 select new
                                 {
                                     cl.cakeLayerId,
                                     cl.cakeLayer1,
                                     cl.dumyLayer,
                                     cl.productId,
                                     cl.vendorId,
                                     cl.price,
                                     cl.size,
                                 }).ToList();
                var cakeFilling = (from cl in db.Fillings
                                 where cl.vendorId == vendorId
                                 select new
                                 {
                                 cl.fillingId,
                                 cl.name,
                                 }).ToList();


                return Ok(new
                {
                    data = query,
                    images = images,
                    flavours = flavours,
                    cakelayer = cakeLayer,
                    cakeFilling = cakeFilling
                });
            }
        }
        //[HttpGet]

        //public IHttpActionResult GetproductbyIdforCheck(long id)
        //{
        //    using ( Entities db = new Entities())
        //    {
        //        var query = (from pro in db.myproducts
        //                     where pro.productId == id
        //                     select new
        //                     {
        //                         pro.productId,
        //                         pro.title,
        //                         pro.price,
        //                         pro.categoryId,
        //                         pro.description,
        //                         pro.vendorId,
        //                         pro.approvedOn,
        //                         pro.isApproved,
        //                         pro.coverImg,
        //                         pro.minimumTime,
        //                         pro.totalLayer,
        //                         pro.fillingName,
        //                         pro.subCategoryId,
        //                         pro.keywords,
        //                         pro.Name,
                               
        //                     }).ToList();

        //        var flavours = (from cf in db.mycakeFlavours
        //                       where cf.productId == id
        //                       select new
        //                       {
        //                           cf.cakeFlavourId,
        //                           cf.productId,
        //                           cf.flavourId,
        //                           cf.Name
        //                       }).ToList();

        //        var images = (from pro in db.mycakeImages
        //                      where pro.productId == id
        //                      select new
        //                      {
        //                          pro.productImgId,
        //                          pro.productId,
        //                          pro.imagePath
        //                      }).ToList();
                
        //        var cakesize = (from cs in db.mycakeSizezs
        //                       where cs.productId == id
        //                       select new 
        //                       {
        //                           cs.cakeSizesId,
        //                           cs.productId,
        //                           cs.size,
        //                           cs.cost
        //                       }).ToList();

        //        var cakeLayer = (from cl in db.mycakeLayers
        //                        where cl.productId == id
        //                        select new 
        //                        {
        //                            cl.cakeLayerId,
        //                            cl.cakeLayer,
        //                            cl.dumyLayer,
        //                            cl.productId,
        //                            cl.vendorId,
        //                            cl.price
        //                        }).ToList();
        
                
        //        return Ok(new
        //        {
        //            data = query,
        //            images = images,
        //            flavours = flavours,
        //            cakesize = cakesize,
        //            cakelayer = cakeLayer
        //        });
        //    }
        //}


        //[HttpGet]

        //public IHttpActionResult GetproductbyIdForCheck(long id)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        var query = (from pro in db.products
        //                     join cat in db.categories
        //                     on pro.categoryId equals cat.categoryId
        //                     join img in db.productImgs
        //                    on pro.productId equals img.productId
        //                     join flv in db.cakeFlavours
        //                    on pro.productId equals flv.productId
        //                     join siz in db.cakeSizes
        //                    on pro.productId equals siz.productId

        //                     join siz in db.cakeSizes
        //                on pro.productId equals siz.productId
        //                     where pro.productId == id
        //                     select new
        //                     {
        //                         pro.productId,
        //                         pro.title,
        //                         pro.price,
        //                         pro.categoryId,
        //                         pro.description,
        //                         pro.vendorId,
        //                         pro.approvedOn,
        //                         pro.isApproved,
        //                         pro.coverImg,
        //                         pro.minimumTime,
        //                         pro.totalLayer,   
        //                         pro.fillingName,
        //                         pro.keywords,
        //                         cat.Name,


        //                     }).ToList();


        //        return Ok(new
        //        {
        //            data = query,

        //        });
        //    }
        //}
        [HttpPost]
        public IHttpActionResult GetproductsWishlists(long id, string city, DeviceViewModel vm)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    var products = (from pro in db.products
                                    join ven in db.vendors
                                    on pro.vendorId equals ven.vendorId
                                    where pro.categoryId == id && pro.isApproved == "true" && ven.city.ToLower().Contains(city.ToLower())
                                    select new
                                    {
                                        pro.productId,
                                        pro.title,
                                        pro.price,
                                        pro.categoryId,
                                        pro.description,
                                        pro.vendorId,
                                        pro.approvedOn,
                                        pro.isApproved,
                                        pro.coverImg,
                                        pro.minimumTime,
                                        pro.perLayerCost,
                                        pro.keywords,
                                        pro.totalLayer,
                                        pro.subCategoryId


                                    }).ToList();


                    var wishlist = (from ws in db.wishLists
                                    where ws.userId == vm.userid || ws.deviceId == vm.DeviceId
                                    join pro in db.products
                                    on ws.productId equals pro.productId
                                    select new
                                    {
                                        ws.wishId,
                                        ws.productId,
                                        ws.userId,
                                        ws.deviceId,
                                        pro.title,
                                        pro.price,
                                        pro.categoryId,
                                        pro.description,
                                        pro.size,
                                        pro.vendorId,
                                        pro.persons,
                                        pro.weight,
                                        pro.approvedOn,
                                        pro.isApproved,
                                        pro.coverImg,
                                        pro.minimumTime,
                                        pro.perLayerCost,
                                    }).ToList();


                    return Ok(new
                    {
                        products,
                        wishlist
                    });


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }




        [HttpGet]
        public IHttpActionResult GetproductsbyCategory(long id ,string city)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    var products = (from pro in db.products
                             join ven in db.vendors
                             on pro.vendorId equals ven.vendorId
                             where pro.categoryId == id && pro.isApproved == "true" && ven.city.ToLower().Contains(city)
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.vendorId,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.perLayerCost,
                                 pro.keywords,
                                 pro.totalLayer,
                                 pro.subCategoryId
                             }).ToList();
                
                    return Ok(new
                    {
                        products
                       
                    });


                }
                catch (Exception ex)
                {
                    throw ex;
                }
               
            }
        }
        [HttpGet]
        public IHttpActionResult GetproductsbySubCategory(long id, string city)
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                             join ven in db.vendors
                             on pro.vendorId equals ven.vendorId
                             where pro.subCategoryId == id && pro.isApproved == "true" && ven.city.ToLower().Contains(city)
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.vendorId,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.perLayerCost,
                                 pro.keywords,
                                 pro.totalLayer,
                                 pro.subCategoryId


                             }).ToList();
                return Ok(new {data = query});
            }
        }


        [HttpGet]
        public IHttpActionResult GetproductsbyVendor(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                                 //join cat in db.categories
                                 //on pro.productId equals cat.productId
                             where pro.vendorId == id && pro.isDeleted != true 
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.subCategoryId,
                                 pro.keywords,
                                 pro.status,
                                 pro.isDeleted,

                                 //cat.Name

                             }).ToList();

                return Ok(new
                {
                    data = query,

                });
            }
        }

        [HttpGet]
        public IHttpActionResult RecentlyDeletedProducts(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                                 //join cat in db.categories
                                 //on pro.productId equals cat.productId
                             where pro.vendorId == id && pro.isDeleted == true
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.coverImg,
                                 pro.minimumTime,
                                 pro.subCategoryId,
                                 pro.keywords,
                                 pro.status,
                                 pro.isDeleted,

                                 //cat.Name

                             }).ToList();

                return Ok(new
                {
                    data = query,

                });
            }
        }

        [HttpPost]
        [Route("api/products/AddProductsImage/")]
        public IHttpActionResult AddProductsImage(CakeImagesViewModel vm)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }


                try
                {
                    var totalCost = (from us in db.cakeSizes
                                     where us.productId == vm.pId
                                     select us.cost.Value).Sum();


                    var totalProducts = (from us in db.cakeSizes
                                         where us.productId == vm.pId
                                         select us).Count();

                    decimal cost = totalCost / totalProducts;

                    var pro = (from us in db.products
                               where us.productId == vm.pId
                               select us).FirstOrDefault();
                    if (pro != null)
                    {
                        pro.price = cost;
                        pro.minimumTime = 3;
                        db.SaveChanges();
                    }


                }
                catch (Exception x)
                { }




                try
                {
                    for (int i = 0; i < vm.images.Length; i++)
                    {
                        productImg f = new productImg();
                        f.productId = vm.pId;
                        f.imagePath = vm.images[i];
                        db.productImgs.Add(f);

                    }
                    int a = db.SaveChanges();
                    //if (a > 0) 
                    //{

                    //    cakeLayer ck = new cakeLayer();
                    //    ck.layer
                    //}
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }




        [HttpGet]
        [Route("api/products/getProductsImage/{id}")]
        public IHttpActionResult GetproductsImage(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from pimg in db.productImgs

                             where pimg.productId == id
                             select new
                             {

                                 pimg.productImgId,
                                 pimg.productId,
                                 pimg.imagePath,


                             }).ToList();


                return Ok(query);
            }
        }


        // GET: api/products/5
        [ResponseType(typeof(product))]
        [HttpGet]
        [Route("api/products/getProduct/{id}")]
        public IHttpActionResult Getproduct(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from pro in db.products
                             join vn in db.vendors
                             on pro.vendorId equals vn.vendorId
                             where pro.productId == id
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.minimumTime,
                                 pro.coverImg,
                                 pro.keywords,
                                 pro.subCategoryId
                             }).ToList();
                if (query != null)
                {
                    return Ok(query);

                }
            }


            return NotFound();
        }
        [HttpPost]
        [Route("api/products/ActiveAll")]
        public IHttpActionResult ActiveAll(ActiveAllViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var products = db.products.Where(x => vm.productIds.Contains(x.productId)).ToList();
                    foreach (var item in products)
                    {
                        item.status = vm.status;
                        db.Entry(item).State = EntityState.Modified;
                      
                    }
                    int check = db.SaveChanges();
                    if (check > 0)
                    {
                        return Ok(new { status = "success", data = "updated" });
                    }
                    else
                    {
                        return Ok(new { status = "error", data = "something went wrong." });
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
        }



        //[HttpGet]
        //[Route("api/products/getProductforMob/{id}")]
        //public IHttpActionResult GetproductorMob(long id)
        //{
        //    using (Entities db = new Entities())
        //    {
        //        var query = (from pro in db.products
        //                     join Pro in db.products
        //                   on cat.productId equals pro.productId
        //                     where cat.categoryId == id
        //                     //join proimg in db.productImgs
        //                     //on pro.productImgId equals proimg.productImgId
        //                     select new
        //                     {
        //                         cat.categoryId,
        //                         cat.Name,
        //                         pro.productId,
        //                         //proimg.imagePath,

        //                     }).ToList();
        //        var qu = query[0].productId;
        //        var queryforImage = (from pro in db.products
        //                             join proImg in db.productImgs
        //                             on pro.productImgId equals proImg.productImgId
        //                             where pro.productImgId == qu

        //        return Ok(query);
        //    }

        [ResponseType(typeof(product))]
        [HttpGet]
        [Route("api/products/getProductbyVendor/{id}")]
        public IHttpActionResult getProductbyVendor(long id)
        {
            using (Entities db = new Entities())
            {

                var query = (from pro in db.products
                             join vn in db.vendors
                             on pro.vendorId equals vn.vendorId
                             where pro.vendorId == id && pro.isApproved == "true"
                             select new
                             {
                                 pro.productId,
                                 pro.title,
                                 pro.price,
                                 pro.categoryId,
                                 pro.description,
                                 pro.size,
                                 pro.vendorId,
                                 pro.persons,
                                 pro.weight,
                                 pro.approvedOn,
                                 pro.isApproved,
                                 pro.minimumTime,
                                 pro.coverImg,
                                 pro.subCategoryId,
                                 pro.keywords
                             }).ToList();
                if (query != null)
                {
                    return Ok(query);

                }


                //if (approved == null)
                //{
                //    var query = (from pro in db.products
                //                 join vn in db.vendors
                //                 on pro.vendorId equals vn.vendorId
                //                 where pro.vendorId == id
                //                 select new
                //                 {
                //                     pro.productId,
                //                     pro.title,
                //                     pro.price,
                //                     pro.categoryId,
                //                     pro.description,
                //                     pro.size,
                //                     pro.vendorId,
                //                     pro.persons,
                //                     pro.weight,
                //                     pro.approvedOn,
                //                     pro.isApproved,
                //                     pro.minimumTime,
                //                     pro.coverImg,
                //                     pro.perLayerCost
                //                 }).ToList();
                //    if (query != null)
                //    {
                //        return Ok(query);

                //    }

                //}

                //if (approved != null)
                //{
                //    var query = (from pro in db.products
                //                 join vn in db.vendors
                //                 on pro.vendorId equals vn.vendorId
                //                 where pro.isApproved == approved
                //                 && pro.vendorId == id
                //                 select new
                //                 {
                //                     pro.productId,
                //                     pro.title,
                //                     pro.price,
                //                     pro.categoryId,
                //                     pro.description,
                //                     pro.size,
                //                     pro.vendorId,
                //                     pro.persons,
                //                     pro.weight,
                //                     pro.approvedOn,
                //                     pro.isApproved,
                //                     pro.minimumTime,
                //                     pro.coverImg,
                //                     pro.perLayerCost
                //                 }).ToList();

                //    return Ok(query);


                //}


                return Ok();
            }


        }
        // PUT: api/products/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/products/updateProduct/{id}")]
        public IHttpActionResult Putproduct(long id, product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != product.productId)
                {
                    return BadRequest();
                }

                db.Entry(product).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!productExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }

        // POST: api/products
        [ResponseType(typeof(product))]
        [HttpPost]
        [Route("api/products/addProduct")]
        public IHttpActionResult Postproduct(product product)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                product.isApproved = "pending";
                db.products.Add(product);
                try
                {
                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                long Id = product.productId;


                return Ok(new
                {
                    status = "success",
                    data = "added",
                    Id = Id
                });


            }
        }



        [HttpDelete]
        public IHttpActionResult Deleteproduct(long id)
        {
            using (Entities db = new Entities())
            {


                product product = db.products.Find(id);

                if (product == null)
                {
                    return NotFound();
                }


                try
                {
                    db.productImgs.RemoveRange(db.productImgs.Where(x => x.productId == id));
                    db.SaveChanges();

                    db.cakeFlavours.RemoveRange(db.cakeFlavours.Where(x => x.productId == id));
                    db.SaveChanges();

                    db.cakeSizes.RemoveRange(db.cakeSizes.Where(x => x.productId == id));
                    db.SaveChanges();

                    db.cakeLayers.RemoveRange(db.cakeLayers.Where(x => x.productId == id));
                    db.SaveChanges();

                    db.products.Remove(product);
                    int check = db.SaveChanges();
                    if (check > 0)
                    {
                        return Ok(new { data = "deleted", status = "success" });

                    }
                    else
                    {
                        return Ok(new { data = "deleted", status = "success" });

                    }
                }
                catch (Exception ex)
                {

                    return Ok(ex.Message);

                }

            }
        }

        [HttpPut]
        public IHttpActionResult RecentlyDelete(DeletedViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var checkOrders = db.orderDetails.Where(x => vm.productIds.Contains((long)x.productId)).Select(x => x.productName).Distinct().ToArray();
                    if (vm.productIds.Count() == 1)
                    {
                       
                        if (checkOrders.Count() == 0)
                        {
                            long id = vm.productIds[0];
                            var myproduct = db.products.Where(x => x.productId == id).FirstOrDefault();
                            myproduct.status = "deleted";
                            myproduct.isDeleted = true;
                            db.Entry(myproduct).State = EntityState.Modified;
                            db.SaveChanges();
                            return Ok(new { status = "success", data = "deleted" });
                        }
                        else 
                        {
                            return Ok(new { status = "exist", data = string.Join(", ", checkOrders) });
                        }

                       

                    }
                    else if (vm.productIds.Count() > 1)
                    {
                        if (checkOrders.Count() == 0)
                        {
                            var products = db.products.Where(x => vm.productIds.Contains(x.productId)).ToList();
                            foreach (var item in products)
                            {
                                var myproduct = db.products.Where(x => x.productId == item.productId).FirstOrDefault();
                                
                                myproduct.status = "deleted";
                                myproduct.isDeleted = true;
                                db.Entry(myproduct).State = EntityState.Modified;

                            }
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { status = "success", data = "deleted" });
                            }
                            else
                            {
                                return Ok(new { status = "error", data = "something went worng" });
                            }
                        }
                        else 
                        {
                            return Ok(new { status = "exist", data = string.Join(", ", checkOrders) });
                        }
                       
                    }
                    else 
                    {
                            return Ok(new { status = "error", data = "something went worng" });
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPut]
        public IHttpActionResult RecoverDeleted(DeletedViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    if (vm.productIds.Count() == 1)
                    {

                       
                            long id = vm.productIds[0];
                            var myproduct = db.products.Where(x => x.productId == id).FirstOrDefault();
                            myproduct.status = "inactive";
                            myproduct.isDeleted = false;
                            db.Entry(myproduct).State = EntityState.Modified;
                            db.SaveChanges();
                            return Ok(new { status = "success", data = "recoverd" });
                        



                    }
                    else if (vm.productIds.Count() > 1)
                    {
                       
                            var products = db.products.Where(x => vm.productIds.Contains(x.productId)).ToList();
                            foreach (var item in products)
                            {
                                var myproduct = db.products.Where(x => x.productId == item.productId).FirstOrDefault();

                                myproduct.status = "inactive";
                                myproduct.isDeleted = false;
                                db.Entry(myproduct).State = EntityState.Modified;

                            }
                            int check = db.SaveChanges();
                            if (check > 0)
                            {
                                return Ok(new { status = "success", data = "deleted" });
                            }
                            else
                            {
                                return Ok(new { status = "error", data = "something went worng" });
                            }
                       

                    }
                    else
                    {
                        return Ok(new { status = "error", data = "something went worng" });
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> AddRecentSearch(recentSearch recent) 
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var check = await db.recentSearches.Where(x => x.productId == recent.productId && x.vendorId == recent.vendorId).FirstOrDefaultAsync();
                    if (check == null)
                    {
                        DateTime timeUtc = DateTime.Now;
                        var info = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                        var uaeTime = TimeZoneInfo.ConvertTime(timeUtc, info);
                        recent.date = uaeTime;
                        db.recentSearches.Add(recent);

                        int check1 = await db.SaveChangesAsync();
                        if (check1 > 0)
                        {
                            var query = (from pro in db.products
                                         join ven in db.vendors
                                         on pro.vendorId equals ven.vendorId
                                         join cat in db.categories
                                         on pro.categoryId equals cat.categoryId
                                         where pro.productId == recent.productId
                                         select new
                                         {
                                             ven.bakeryName,
                                             pro.productId,
                                             pro.title,
                                             pro.price,
                                             pro.categoryId,
                                             pro.description,
                                             pro.vendorId,
                                             pro.approvedOn,
                                             pro.isApproved,
                                             pro.coverImg,
                                             pro.minimumTime,
                                             pro.totalLayer,
                                             pro.fillingName,
                                             pro.keywords,
                                             cat.Name,
                                             pro.subCategoryId,
                                             pro.skuNo
                                         }).ToList();

                            var images = (from pro in db.productImgs
                                          where pro.productId == recent.productId
                                          select new
                                          {
                                              pro.productImgId,
                                              pro.productId,
                                              pro.imagePath
                                          }).ToList();

                            var flavours = (from cf in db.cakeFlavours
                                            join flv in db.flavours
                                      on cf.flavourId equals flv.flavourId
                                            where cf.productId == recent.productId
                                            select new
                                            {
                                                cf.cakeFlavourId,
                                                cf.productId,
                                                cf.flavourId,
                                                flavourName = flv.Name
                                            }).ToList();
                            var cakesize = (from cs in db.cakeSizes
                                            where cs.productId == recent.productId
                                            select new
                                            {
                                                cs.cakeSizesId,
                                                cs.productId,
                                                cs.size,
                                                cs.cost
                                            }).ToList();
                            var cakeLayer = (from cl in db.cakeLayers
                                             where cl.productId == recent.productId
                                             select new
                                             {
                                                 cl.cakeLayerId,
                                                 cl.cakeLayer1,
                                                 cl.dumyLayer,
                                                 cl.productId,
                                                 cl.vendorId,
                                                 cl.price
                                             }).ToList();
                            var cakeFilling = (from cl in db.products
                                               where cl.productId == recent.productId
                                               select new
                                               {

                                                   cl.fillingName,
                                               }).ToList();


                            return Ok(new
                            {
                                status = "success",
                                data = query,
                                images = images,
                                flavours = flavours,
                                cakesize = cakesize,
                                cakelayer = cakeLayer,
                                cakeFilling = cakeFilling
                            });
                        }
                        else 
                        {
                            var query = (from pro in db.products
                                         join ven in db.vendors
                                         on pro.vendorId equals ven.vendorId
                                         join cat in db.categories
                                         on pro.categoryId equals cat.categoryId
                                         where pro.productId == recent.productId
                                         select new
                                         {
                                             ven.bakeryName,
                                             pro.productId,
                                             pro.title,
                                             pro.price,
                                             pro.categoryId,
                                             pro.description,
                                             pro.vendorId,
                                             pro.approvedOn,
                                             pro.isApproved,
                                             pro.coverImg,
                                             pro.minimumTime,
                                             pro.totalLayer,
                                             pro.fillingName,
                                             pro.keywords,
                                             cat.Name,
                                             pro.subCategoryId,
                                             pro.skuNo
                                         }).ToList();

                            var images = (from pro in db.productImgs
                                          where pro.productId == recent.productId
                                          select new
                                          {
                                              pro.productImgId,
                                              pro.productId,
                                              pro.imagePath
                                          }).ToList();

                            var flavours = (from cf in db.cakeFlavours
                                            join flv in db.flavours
                                      on cf.flavourId equals flv.flavourId
                                            where cf.productId == recent.productId
                                            select new
                                            {
                                                cf.cakeFlavourId,
                                                cf.productId,
                                                cf.flavourId,
                                                flavourName = flv.Name
                                            }).ToList();
                            var cakesize = (from cs in db.cakeSizes
                                            where cs.productId == recent.productId
                                            select new
                                            {
                                                cs.cakeSizesId,
                                                cs.productId,
                                                cs.size,
                                                cs.cost
                                            }).ToList();
                            var cakeLayer = (from cl in db.cakeLayers
                                             where cl.productId == recent.productId
                                             select new
                                             {
                                                 cl.cakeLayerId,
                                                 cl.cakeLayer1,
                                                 cl.dumyLayer,
                                                 cl.productId,
                                                 cl.vendorId,
                                                 cl.price
                                             }).ToList();
                            var cakeFilling = (from cl in db.products
                                               where cl.productId == recent.productId
                                               select new
                                               {

                                                   cl.fillingName,
                                               }).ToList();


                            return Ok(new
                            {
                                status = "success",
                                data = query,
                                images = images,
                                flavours = flavours,
                                cakesize = cakesize,
                                cakelayer = cakeLayer,
                                cakeFilling = cakeFilling
                            });
                        }

                       

                    }
                    else 
                    {
                        DateTime timeUtc = DateTime.Now;
                        var info = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                        var uaeTime = TimeZoneInfo.ConvertTime(timeUtc, info);
                        check.date = uaeTime;

                        db.Entry(check).State = EntityState.Modified;
                        await db.SaveChangesAsync();

                        var query = (from pro in db.products
                                     join ven in db.vendors
                                     on pro.vendorId equals ven.vendorId
                                     join cat in db.categories
                                     on pro.categoryId equals cat.categoryId
                                     where pro.productId == recent.productId
                                     select new
                                     {
                                         ven.bakeryName,
                                         pro.productId,
                                         pro.title,
                                         pro.price,
                                         pro.categoryId,
                                         pro.description,
                                         pro.vendorId,
                                         pro.approvedOn,
                                         pro.isApproved,
                                         pro.coverImg,
                                         pro.minimumTime,
                                         pro.totalLayer,
                                         pro.fillingName,
                                         pro.keywords,
                                         cat.Name,
                                         pro.subCategoryId,
                                         pro.skuNo
                                     }).ToList();

                        var images = (from pro in db.productImgs
                                      where pro.productId == recent.productId
                                      select new
                                      {
                                          pro.productImgId,
                                          pro.productId,
                                          pro.imagePath
                                      }).ToList();

                        var flavours = (from cf in db.cakeFlavours
                                        join flv in db.flavours
                                  on cf.flavourId equals flv.flavourId
                                        where cf.productId == recent.productId
                                        select new
                                        {
                                            cf.cakeFlavourId,
                                            cf.productId,
                                            cf.flavourId,
                                            flavourName = flv.Name
                                        }).ToList();
                        var cakesize = (from cs in db.cakeSizes
                                        where cs.productId == recent.productId
                                        select new
                                        {
                                            cs.cakeSizesId,
                                            cs.productId,
                                            cs.size,
                                            cs.cost
                                        }).ToList();
                        var cakeLayer = (from cl in db.cakeLayers
                                         where cl.productId == recent.productId
                                         select new
                                         {
                                             cl.cakeLayerId,
                                             cl.cakeLayer1,
                                             cl.dumyLayer,
                                             cl.productId,
                                             cl.vendorId,
                                             cl.price
                                         }).ToList();
                        var cakeFilling = (from cl in db.products
                                           where cl.productId == recent.productId
                                           select new
                                           {

                                               cl.fillingName,
                                           }).ToList();


                        return Ok(new
                        {
                            status = "success",
                            data = query,
                            images = images,
                            flavours = flavours,
                            cakesize = cakesize,
                            cakelayer = cakeLayer,
                            cakeFilling = cakeFilling
                        });
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpGet]
        public async Task<IHttpActionResult> GetRecentSearch(long id) 
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var recentSearhes = await db.recentSearches.Where(x => x.vendorId == id).Select(x => new 
                    {
                        x.productId ,
                        title = db.products.Where(pro=> pro.productId == x.productId).Select(pro=> pro.title).FirstOrDefault(),
                        coverImg = db.products.Where(pro => pro.productId == x.productId).Select(pro => pro.coverImg).FirstOrDefault(),
                        x.date
                    }).OrderByDescending(x=>x.date).ToListAsync();

                    return Ok(new { status = "success", data = recentSearhes });

                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> VendorSearch(string text,long id) 
        {
            try
            {
                using (Entities db = new Entities())
                {
                  var products = await db.products.Where(x => x.vendorId == id && x.title.Contains(text)).Select(x => new { x.title, x.productId , x.coverImg}).ToListAsync();
                    return Ok(new { status = "success", data = products });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private bool productExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.products.Count(e => e.productId == id) > 0;
            }
        }
    }

    public class mNewFilter
    {       
        public string size { get; set; }
        public decimal cost { get; set; }

    }
}