﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class subCategoriesController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.subCategories

                             select new
                             {
                                 bn.subCategoryId,
                                 bn.categoryId,
                                 bn.image,
                                 bn.name
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.subCategories
                             where bn.subCategoryId == id
                             select new
                             {
                                 bn.subCategoryId,
                                 bn.categoryId,
                                 bn.image,
                                 bn.name
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, subCategory subCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != subCategory.subCategoryId)
                {
                    return BadRequest();
                }

                db.Entry(subCategory).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!subCategoryExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(subCategory subCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.subCategories.Add(subCategory);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                subCategory subCategory = db.subCategories.Find(id);
                if (subCategory == null)
                {
                    return NotFound();
                }

                db.subCategories.Remove(subCategory);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool subCategoryExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.subCategories.Count(e => e.subCategoryId == id) > 0;
            }
        }
    }
}
