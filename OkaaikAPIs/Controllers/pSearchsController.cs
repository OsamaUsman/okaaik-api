﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class pSearchsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.pSearches

                             select new
                             {
                                 bn.psId,
                                 bn.categoryId,

                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.pSearches
                             where bn.psId == id
                             select new
                             {
                                 bn.psId,
                                 bn.categoryId,

                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, pSearch pSearch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != pSearch.psId)
                {
                    return BadRequest();
                }

                db.Entry(pSearch).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!bannerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(pSearch pSearch)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.pSearches.Add(pSearch);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                pSearch pSearch = db.pSearches.Find(id);
                if (pSearch == null)
                {
                    return NotFound();
                }

                db.pSearches.Remove(pSearch);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool bannerExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.pSearches.Count(e => e.psId == id) > 0;
            }
        }
    }
}
