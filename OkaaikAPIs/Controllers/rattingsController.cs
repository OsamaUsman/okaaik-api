﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class rattingsController : ApiController
    {

        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from rate in db.rattings
                             select new
                             {
                                 rate.rattingId,
                                 rate.count,
                                 rate.avgRatting,
                                 rate.orderId,
                                 rate.userId,
                                 rate.vendorId,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from rate in db.rattings
                             where rate.rattingId == id
                             select new
                             {
                                 rate.rattingId,
                                 rate.count,
                                 rate.avgRatting,
                                 rate.orderId,
                                 rate.userId,
                                 rate.vendorId,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, ratting ratting)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != ratting.rattingId)
                {
                    return BadRequest();
                }

                db.Entry(ratting).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!bannerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(ratting ratting)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.rattings.Add(ratting);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                ratting ratting = db.rattings.Find(id);
                if (ratting == null)
                {
                    return NotFound();
                }

                db.rattings.Remove(ratting);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool bannerExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.rattings.Count(e => e.rattingId == id) > 0;
            }
        }
    }
}
