﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{
    public class chatsController : ApiController
    {
        private Entities db = new Entities();

        [HttpGet]
        public IHttpActionResult GetAllThreats()
        {
            var query = (from ch in db.chatThreats
                         join us in db.users
                         on ch.userId equals us.userId
                         select new
                         {
                             ch.threatId,
                             ch.orderId,
                             ch.userId,
                             ch.vendorId,
                             ch.date,
                             
                             us.userName,
                         }).OrderByDescending(ch => ch.date).ToList();
            if (query.Count() == 0)
            {
                return NotFound();
            }

            return Ok(query);
        }


        [HttpGet]
        public IHttpActionResult GetThreatsbyvendor(long id)
        {
            var query = (from ch in db.chatThreats
                         join us in db.users
                         on ch.userId equals us.userId
                         where ch.vendorId == id
                         select new
                         {
                             ch.threatId,
                             ch.orderId,
                             ch.userId,
                             ch.vendorId,
                             ch.date,
                             us.userName,
                         }).OrderByDescending(ch => ch.date).ToList();
            if (query.Count() == 0)
            {
                return NotFound();
            }

            return Ok(query);
        }


        [ResponseType(typeof(chat))]
        [Route("api/chats/getchats")]
        [HttpGet]
        public IHttpActionResult Getchats()
        {
            var query = (from ch in db.chats

                         select new
                         {
                             ch.chatId,
                             ch.orderId,
                             ch.userId,
                             ch.sentBy,
                             ch.vendorId,
                             ch.message,
                             ch.type,
                             ch.status
                         }).ToList();
            if (query == null)
            {
                return Ok(query);
            }

            return Ok(query);
        }

        //[HttpGet]
        //public IHttpActionResult Getchatbyvendor(long id)
        //{
        //    var query = (from or in db.oOrders
        //                 join ch in db.chats
        //                 on or.orderId equals ch.orderId
        //                 where ch.vendorId == id

        //                 select new
        //                 {
        //                     ch.orderId,

        //                 }).Distinct().ToList();

        // var result = from chats

        //    //foreach (var item in query)
        //    //{

        //    //} 

        //    if (query == null)
        //    {
        //        return NotFound();
        //    }

        //    return Ok(query);
        //}




        // GET: api/chats/5

        [Route("api/chats/getchatsbyorder/{id}")]
        [HttpGet]
        public IHttpActionResult Getchatbyorder(long id)
        {
            var query = (from ch in db.chats
                         join user in db.users
                         on ch.userId equals user.userId
                         join ven in db.vendors
                         on ch.vendorId equals ven.vendorId
                         where ch.orderId == id
                         select new
                         {
                             ch.chatId,
                             ch.orderId,
                             ch.userId,
                             ch.sentBy,
                             ch.vendorId,
                             ch.message,
                             ch.type,
                             ch.date,
                             ch.status,
                             ven.bakeryName,
                             ven.logo,
                             user.userName
                         }).OrderBy(ch => ch.date).ToList();
            if (query.Count() == 0)
            {
                return Ok(query);
            }

            return Ok(query);
        }


        // POST: api/chats
        [Route("api/chats/addchat")]
        [HttpPost]
        public IHttpActionResult Postchat(chat chat)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            chat.status = "sent";
            chat.date = DateTime.Now;



            db.chats.Add(chat);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
            Added st = new Added();
            return Ok(st);
        }

        // DELETE: api/chats/5
        [Route("api/chats/removechat/{id}")]
        [HttpDelete]
        public IHttpActionResult Deletechat(long id)
        {
            chat chat = db.chats.Find(id);
            if (chat == null)
            {
                return NotFound();
            }

            db.chats.Remove(chat);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
            Deleted st = new Deleted();
            return Ok(st);
        }


        private bool chatExists(long id)
        {
            return db.chats.Count(e => e.chatId == id) > 0;
        }
    }
}