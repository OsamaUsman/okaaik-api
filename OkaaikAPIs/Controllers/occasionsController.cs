﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class occasionsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.occasions
                             select new
                             {
                                 bn.occasionId,
                                 bn.title,
                                 bn.image,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.occasions
                             where bn.occasionId == id
                             select new
                             {
                                 bn.occasionId,
                                 bn.title,
                                 bn.image,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, occasion occasion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != occasion.occasionId)
                {
                    return BadRequest();
                }

                db.Entry(occasion).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!bannerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(occasion occasion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.occasions.Add(occasion);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    var checkReminder = db.reminders.Where(x => x.occasionId == id).FirstOrDefault();
                    if (checkReminder == null)
                    {
                        occasion occasion = db.occasions.Find(id);

                        db.occasions.Remove(occasion);
                        int check = db.SaveChanges();
                        if (check > 0)
                        {
                            return Ok(new { status = "success", data = "deleted" });
                        }
                        else 
                        {
                            return Ok(new { status = "error", data = "something went wrong" });
                        }
                    }
                    else 
                    {
                            return Ok(new { status = "error", data = "This occasion has reminder." });
                    }
                }
                catch (Exception)
                {

                    throw;
                }
                
         
            }
        }







        private bool bannerExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.occasions.Count(e => e.occasionId == id) > 0;
            }
        }
    }
}
