﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class oOrdersController : ApiController
    {
        [HttpPut]
        public IHttpActionResult UpdateOrderStatus(long id, string check)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var row = db.oOrders.Where(or => or.orderId == id).FirstOrDefault();

                    if (check.ToLower() == "archive") 
                    {
                        row.isArchive = true;
                        db.Entry(row).State = EntityState.Modified;
                        int mycheck1 = db.SaveChanges();
                        if (mycheck1 > 0)
                        {
                            return Ok(new { status = "success", data = "updated" });
                        }
                        else
                        {
                            return Ok(new { status = "error", data = "something went wrong." });
                        }
                    }
                    notification noti = new notification();
                    if (check == "Under progress" || check == "Reject")
                    {
                        try
                        {
                            row.status = check;
                            db.Entry(row).State = EntityState.Modified;
                            int mycheck = db.SaveChanges();

                            if (row.status == "Under progress")
                            {
                                noti.message = "Dear Customer your order has been accepted by bakery.";
                            }
                            else if (row.status == "Reject")
                            {
                                noti.message = "Dear Customer your order has been rejected by bakery.";
                            }
                            var deviceId = db.users.Where(x => x.userId == row.userId).Select(x => x.deviceId).ToArray();
                            var productId = db.orderDetails.Where(x => x.orderId == row.orderId).Select(x => x.productId).FirstOrDefault();
                            var product = db.products.Where(x => x.productId == productId).Select(x => new { x.title, x.coverImg, x.price }).FirstOrDefault();


                            noti.date = OkaaikAPIs.TimeZone.TimeZone.UaeDateWithTime();
                            noti.userId = row.userId;
                            noti.picture = product.coverImg;
                            noti.title = product.title;
                            noti.price = product.price;
                            db.notifications.Add(noti);
                            db.SaveChanges();
                            NotificationViewModel nvm = new NotificationViewModel();
                            nvm.playerId = deviceId;
                            nvm.title = noti.title;
                            nvm.message = noti.message;
                            nvm.tag = "Order";
                            var response = OkaaikAPIs.Notifications.PushNotification.Send_Notification(nvm);

                            return Ok(new { status = "success", data = "updated" });

                        }
                        catch (Exception)
                        {
                            return Ok(new { status = "success", data = "updated" });
                        }

                    }
                    else 
                    {
                        row.status = check;
                        db.Entry(row).State = EntityState.Modified;
                        int mycheck = db.SaveChanges();

                        return Ok(new { status = "success", data = "updated" });
                    }



                }
            }
            catch (Exception)
            {

                throw;
            }
           
        }
       
        [HttpGet]
        public IHttpActionResult getoOrdersbyfilter(long id, DateTime fromdate, DateTime todate)
        {
            using (Entities db = new Entities())
            {
                if (fromdate != null && todate != null )
                {
                    // var addedDate = DateTime.Now.Date.AddDays(10);
                    // DateTime? addday = todate;
                    DateTime mytodate = todate.AddDays(1);
                    // var addday = todate.AddDay(1);
                    //object p = addday.AddDays(1);
                    var query = (from ord in db.oOrders

                                 join us in db.users
                                 on ord.userId equals us.userId
                                 where ord.vendorId == id && ord.date >= fromdate && ord.date  <= mytodate

                                 select new
                                 {

                                     us.userName,
                                     ord.orderId,
                                     ord.bill,
                                     ord.discount,
                                     ord.modeOfpayment,
                                     ord.vendorId,
                                     ord.deliveryCharges,
                                     ord.date,
                                     ord.netBill,
                                     ord.totalBill,
                                     ord.status,
                                     ord.vat,
                                     ord.transactionId,
                                     ord.url,
                                     ord.promoCodeId,
                                     ord.note,
                                     ord.deliveryModeId,
                                     ord.paidAmount,
                                     ord.userId
                                 }).ToList();
                    if (query.Count() == 0)
                    {
                        return NotFound();
                    }
                    return Ok(new {status="success", query } );
                }
                else 
                {
                    var query = (from ord in db.oOrders

                                 join us in db.users
                                 on ord.userId equals us.userId
                                 where ord.vendorId == id 

                                 select new
                                 {

                                     us.userName,
                                     ord.orderId,
                                     ord.bill,
                                     ord.discount,
                                     ord.modeOfpayment,
                                     ord.vendorId,
                                     ord.deliveryCharges,
                                     ord.date,
                                     ord.netBill,
                                     ord.totalBill,
                                     ord.status,
                                     ord.vat,
                                     ord.transactionId,
                                     ord.url,
                                     ord.promoCodeId,
                                     ord.note,
                                     ord.deliveryModeId,
                                     ord.paidAmount,
                                     ord.userId
                                 }).ToList();
                    if (query.Count() == 0)
                    {
                        return NotFound();
                    }
                    return Ok(query);
                }
                
            }
        }
        [HttpGet]
        public IHttpActionResult GetNotifications(long id)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var notifications = db.notifications.Where(x => x.userId == id).ToList();
                    return Ok(new { status = "success", data = notifications });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetOrdersByStatus(long id, string status) 
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var orders = await db.oOrders.Where(x => x.vendorId == id && x.status == status && x.isArchive != true).Select(x => new
                    {
                        userName = db.users.Where(x => x.userId == x.userId).Select(x => x.userName).FirstOrDefault(),
                        x.orderId,
                        x.bill,
                        x.discount,
                        x.modeOfpayment,
                        x.vendorId,
                        x.deliveryCharges,
                        x.date,
                        x.netBill,
                        x.totalBill,
                        x.status,
                        x.vat,
                        x.transactionId,
                        x.url,
                        x.promoCodeId,
                        x.note,
                        x.deliveryModeId,
                        x.paidAmount,
                        x.userId,
                        x.time,
                    }).ToListAsync();
                    DateTime timeUtc = DateTime.Now;
                    var info = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                    var uaeTime = TimeZoneInfo.ConvertTime(timeUtc, info);
                    var requiresActions = db.oOrders.Where(x => x.status == "Requires actions" && x.vendorId == id).Count();
                    var readyForDelivery = db.oOrders.Where(x => x.status == "Ready for delivery" && x.vendorId == id).Count();
                    var underProgress = db.oOrders.Where(x => x.status == "Under progress" && x.vendorId == id).Count();

                    return Ok(new { status = "success",  orders , uaeTime , requiresActions, readyForDelivery , underProgress });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpGet]
        [Route("api/oOrders/getoOrdersbyvendor")]
        public IHttpActionResult getoOrdersbyvendor(long id)
        {
            try
            {
                using (Entities db = new Entities())
                {

                    var query = (from ord in db.oOrders
                                 join us in db.users
                                 on ord.userId equals us.userId
                                 where ord.vendorId == id && ord.isArchive != true

                                 select new
                                 {

                                     us.userName,
                                     ord.orderId,
                                     ord.bill,
                                     ord.discount,
                                     ord.modeOfpayment,
                                     ord.vendorId,
                                     ord.deliveryCharges,
                                     ord.date,
                                     ord.netBill,
                                     ord.totalBill,
                                     ord.status,
                                     ord.vat,
                                     ord.transactionId,
                                     ord.url,
                                     ord.promoCodeId,
                                     ord.note,
                                     ord.deliveryModeId,
                                     ord.paidAmount,
                                     ord.userId,
                                     ord.time,
                                 }).ToList();
                    long readyForDelivery = (from od in db.oOrders
                                             where od.vendorId == id && od.status == "Ready for delivery"
                                             select new
                                             {
                                                 od
                                             }).Count();
                    long requiresActions = (from od in db.oOrders
                                            where od.vendorId == id && od.status == "Requires actions"
                                            select new
                                            {
                                                od
                                            }).Count();
                    long underProgress = (from od in db.oOrders
                                          where od.vendorId == id && od.status == "Under progress"
                                          select new
                                          {
                                              od
                                          }).Count();
                    DateTime timeUtc = DateTime.Now;
                    var info = TimeZoneInfo.FindSystemTimeZoneById("Arabian Standard Time");
                    var uaeTime = TimeZoneInfo.ConvertTime(timeUtc, info);

                    if (query.Count() == 0)
                    {
                        return NotFound();
                    }
                    return Ok(new { status = "success", query, uaeTime, requiresActions, readyForDelivery, underProgress });
                }
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        [HttpGet]
        [Route("api/oOrders/OrdersByVendor")]
        public IHttpActionResult OrdersByVendor(long id)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var query = (from ord in db.oOrders
                                 join us in db.users
                                 on ord.userId equals us.userId
                                 join det in db.orderDetails
                                 on ord.orderId equals det.orderId
                                 join pro in db.products
                                 on det.productId equals pro.productId
                                 where ord.vendorId == id && ord.isArchive != true
                                 select new
                                 {
                                     pro.title,
                                     pro.coverImg,
                                     us.userName,
                                     ord.orderId,
                                     ord.bill,
                                     ord.discount,
                                     ord.modeOfpayment,
                                     ord.vendorId,
                                     ord.deliveryCharges,
                                     ord.date,
                                     ord.netBill,
                                     ord.totalBill,
                                     ord.status,
                                     ord.vat,
                                     ord.transactionId,
                                     ord.url,
                                     ord.promoCodeId,
                                     ord.note,
                                     ord.deliveryModeId,
                                     ord.paidAmount,
                                     ord.userId,
                                     ord.time,
                                 }).OrderBy(x=> x.status == "Requires actions").OrderByDescending(x=> x.date).ToList();
                  
                    return Ok(new { status = "success", data = query});
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        [Route("api/oOrders/getArchiveOrdersbyVendor")]
        [HttpGet]
        public IHttpActionResult getArchiveOrdersbyVendor(long id)
        {
            try
            {
                using (Entities db = new Entities())
                {

                    var query = (from ord in db.oOrders

                                 join us in db.users
                                 on ord.userId equals us.userId
                                 where ord.vendorId == id && ord.isArchive == true

                                 select new
                                 {

                                     us.userName,
                                     ord.orderId,
                                     ord.bill,
                                     ord.discount,
                                     ord.modeOfpayment,
                                     ord.vendorId,
                                     ord.deliveryCharges,
                                     ord.date,
                                     ord.netBill,
                                     ord.totalBill,
                                     ord.status,
                                     ord.vat,
                                     ord.transactionId,
                                     ord.url,
                                     ord.promoCodeId,
                                     ord.note,
                                     ord.deliveryModeId,
                                     ord.paidAmount,
                                     ord.userId,
                                     ord.time,
                                 }).ToList();
                  

                   
                    return Ok(new { status = "success", query});
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public IHttpActionResult Detailsbyorders(long id)
        {
            using (Entities db = new Entities())
            {
                var orderDetails = (from det in db.orderDetails
                                    join ven in db.vendors
                                    on det.vendorId equals ven.vendorId
                                    join pro in db.products
                                    on det.productId equals pro.productId
                                    where det.orderId == id
                                    select new
                                    {
                                        ven.bakeryName,
                                        det.orderDetailsid,
                                        det.addressId,
                                        det.addressTitle,
                                        det.date,
                                        det.cost,
                                        det.deliverToMe,
                                        det.deliveryMethod,
                                        det.flavourId,
                                        det.flavourName,
                                        det.latitude,
                                        det.layerId,
                                        det.layerName,
                                        det.longitude,
                                        det.message,
                                        det.address,
                                        det.pickupTime,
                                        det.productId,
                                        det.productName,
                                        det.size,
                                        det.sizeId,
                                        det.orderId,
                                        detailStatus = det.status,
                                        detailVendorId = det.vendorId,
                                        det.contactNo,
                                        det.name,
                                        det.streetNo,
                                        det.note,

                                        pro.title,
                                        pro.price,
                                        pro.categoryId,
                                        pro.description,
                                        pro.vendorId,
                                        pro.persons,
                                        pro.weight,
                                        pro.approvedOn,
                                        pro.isApproved,
                                        pro.coverImg,
                                        pro.minimumTime,
                                        pro.perLayerCost,


                                    }).ToList();




                if (orderDetails.Count() == 0)
                {
                    return NotFound();
                }

                return Ok(new { orderDetails = orderDetails });
            }
        }

        [HttpGet]
        public IHttpActionResult OrderDetailsByVendor(long id)
        {
            using (Entities db = new Entities())
            {
                var orderDetails = (from det in db.orderDetails
                                    join ven in db.vendors
                                    on det.vendorId equals ven.vendorId
                                    join ord in db.oOrders
                                    on det.orderId equals ord.orderId
                                    join pro in db.products
                                    on det.productId equals pro.productId
                                    where det.orderId == id
                                    select new
                                    {
                                        ven.bakeryName,
                                        ord.time,
                                        det.orderDetailsid,
                                        det.addressId,
                                        det.addressTitle,
                                        det.date,
                                        det.cost,
                                        det.deliverToMe,
                                        det.deliveryMethod,
                                        det.flavourId,
                                        det.flavourName,
                                        det.latitude,
                                        det.layerId,
                                        det.layerName,
                                        det.longitude,
                                        det.message,
                                        det.address,
                                        det.pickupTime,
                                        det.productId,
                                        det.productName,
                                        det.size,
                                        det.sizeId,
                                        det.orderId,
                                        detailStatus = det.status,
                                        detailVendorId = det.vendorId,
                                        det.contactNo,
                                        det.name,
                                        det.streetNo,
                                        det.note,
                                        pro.title,
                                        pro.price,
                                        pro.categoryId,
                                        pro.description,
                                        pro.vendorId,
                                        pro.persons,
                                        pro.weight,
                                        pro.approvedOn,
                                        pro.isApproved,
                                        pro.coverImg,
                                        pro.minimumTime,
                                        pro.perLayerCost,
                                    }).ToList();

                return Ok(new { status="success" , data= orderDetails });
            }
        }

        [HttpGet]
        [Route("api/oOrders/getoOrders")]
        public IHttpActionResult GetoOrders()
        {
            using (Entities db = new Entities())
            {
                var query = (from ord in db.oOrders
                             join ven in db.vendors
                            on ord.vendorId equals ven.vendorId
                             join us in db.users
                             on ord.userId equals us.userId
                             select new
                             {
                                 ven.bakeryName,
                                 ord.orderId,
                                 ord.bill,
                                 ord.discount,
                                 ord.modeOfpayment,
                                 ord.vendorId,
                                 ord.deliveryCharges,
                                 ord.date,
                                 ord.netBill,
                                 ord.totalBill,
                                 ord.status,
                                 ord.vat,
                                 ord.transactionId,
                                 ord.url,
                                 ord.promoCodeId,
                                 ord.note,
                                 ord.deliveryModeId,
                                 ord.paidAmount,
                                 ord.userId,
                                 us.userName



                             }).OrderByDescending(ord=> ord.date).ToList();
                return Ok(query);
            }
        }
        [HttpGet]
        public IHttpActionResult GetoOrdersbyuser(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from ord in db.oOrders
                             where ord.userId == id
                             select new
                             {
                                 ord.orderId,
                                 ord.bill,
                                 ord.discount,
                                 ord.modeOfpayment,
                                 ord.vendorId,
                                 ord.deliveryCharges,
                                 ord.date,
                                 ord.netBill,
                                 ord.totalBill,
                                 ord.status,
                                 ord.vat,
                                 ord.transactionId,
                                 ord.url,
                                 ord.promoCodeId,
                                 ord.note,
                                 ord.deliveryModeId,
                                 ord.paidAmount,
                                 ord.userId

                             }).ToList();


                if (query.Count() == 0)
                {
                    return NotFound();
                }
                return Ok(query);
            }
        }

        [HttpGet]
        public IHttpActionResult notCompleteOrderbyUser(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from ord in db.oOrders
                             where ord.userId == id && ord.status != "Ready for delivery"
                             select new
                             {
                                 ord.orderId,
                                 ord.bill,
                                 ord.discount,
                                 ord.modeOfpayment,
                                 ord.vendorId,
                                 ord.deliveryCharges,
                                 ord.date,
                                 ord.netBill,
                                 ord.totalBill,
                                 ord.status,
                                 ord.vat,
                                 ord.transactionId,
                                 ord.url,
                                 ord.promoCodeId,
                                 ord.note,
                                 ord.deliveryModeId,
                                 ord.paidAmount,
                                 ord.userId
                             }).ToList();

                var orderDetails = (from ord in db.oOrders
                                    join det in db.orderDetails
                                    on ord.orderId equals det.orderId
                                    where ord.userId == id && ord.status != "Ready for delivery"
                                    select new
                                    {
                                        det.orderDetailsid,
                                        det.addressId,
                                        det.addressTitle,
                                        det.cost,
                                        det.deliverToMe,
                                        det.deliveryMethod,
                                        det.flavourId,
                                        det.flavourName,
                                        det.latitude,
                                        det.layerId,
                                        det.layerName,
                                        det.longitude,
                                        det.message,
                                        det.address,
                                        det.pickupTime,
                                        det.productId,
                                        det.productName,
                                        det.size,
                                        det.sizeId,
                                        det.contactNo,
                                        det.name,
                                        det.orderId,
                                        det.streetNo,
                                        det.note,
                                        det.qty,
                                        detailStatus = det.status,
                                        detailVendorId = det.vendorId
                                    }).ToList();

                var products = (from ord in db.oOrders
                                join det in db.orderDetails
                                on ord.orderId equals det.orderId
                                join pro in db.products
                                on det.productId equals pro.productId
                                where ord.userId == id && ord.status != "Ready for delivery"
                                select new
                                {
                                    pro.productId,
                                    pro.title,
                                    pro.price,
                                    pro.categoryId,
                                    pro.description,
                                    pro.size,
                                    pro.vendorId,
                                    pro.persons,
                                    pro.weight,
                                    pro.approvedOn,
                                    pro.isApproved,
                                    pro.coverImg,
                                    pro.minimumTime,
                                    pro.perLayerCost,
                                }).ToList().Distinct();


                if (query == null)
                {
                    return NotFound();
                }

                return Ok(new { orders = query, orderDetails = orderDetails, products = products });
            }
        }
        // GET: api/oOrders/5
        [HttpGet]
        public IHttpActionResult OrderbyUser(long id)
        {
            using (Entities db = new Entities())
            {
                //var query = (from ord in db.oOrders
                //             where ord.userId == id 
                //             select new
                //             {
                //                 ord.orderId,
                //                 ord.bill,
                //                 ord.discount,
                //                 ord.modeOfpayment,
                //                 ord.vendorId,
                //                 ord.deliveryCharges,
                //                 ord.date,
                //                 ord.netBill,
                //                 ord.totalBill,
                //                 ord.status,
                //                 ord.vat,
                //                 ord.transactionId,
                //                 ord.url,
                //                 ord.promoCodeId,
                //                 ord.note,
                //                 ord.deliveryModeId,
                //                 ord.paidAmount,
                //                 ord.userId
                //             }).ToList();

                var readyForDelivery = (from ord in db.oOrders
                                    join det in db.orderDetails
                                    on ord.orderId equals det.orderId
                                    join pro in db.products
                                    on det.productId equals pro.productId
                                    where ord.userId == id && ord.status.ToLower() == "ready for delivery"
                                    select new
                                    {
                                        order = new
                                        {
                                            ord.orderId,
                                            ord.bill,
                                            ord.discount,
                                            ord.modeOfpayment,
                                            ord.vendorId,
                                            ord.deliveryCharges,
                                            ord.date,
                                            ord.netBill,
                                            ord.totalBill,
                                            ord.status,
                                            ord.vat,
                                            ord.transactionId,
                                            ord.url,
                                            ord.promoCodeId,
                                            ord.note,
                                            ord.deliveryModeId,
                                            ord.paidAmount,
                                            ord.userId
                                        },
                                        orderDetail = new
                                        {
                                            det.orderDetailsid,
                                            det.addressId,
                                            det.addressTitle,
                                            det.cost,
                                            det.deliverToMe,
                                            det.deliveryMethod,
                                            det.flavourId,
                                            det.flavourName,
                                            det.latitude,
                                            det.layerId,
                                            det.layerName,
                                            det.longitude,
                                            det.message,
                                            det.address,
                                            det.pickupTime,
                                            det.productId,
                                            det.productName,
                                            det.size,
                                            det.sizeId,
                                            det.contactNo,
                                            det.name,
                                            det.orderId,
                                            det.streetNo,
                                            det.note,
                                            det.qty,
                                            detailStatus = det.status,
                                            detailVendorId = det.vendorId
                                        },
                                        product = new
                                        {
                                            pro.productId,
                                            pro.title,
                                            pro.price,
                                            pro.categoryId,
                                            pro.description,
                                            pro.size,
                                            pro.vendorId,
                                            pro.persons,
                                            pro.weight,
                                            pro.approvedOn,
                                            pro.isApproved,
                                            pro.coverImg,
                                            pro.fillingName,
                                            pro.minimumTime,
                                            pro.perLayerCost,
                                        },
                                    }).ToList();

                var  notReady  =        (from ord in db.oOrders
                                        join det in db.orderDetails
                                        on ord.orderId equals det.orderId
                                        join pro in db.products
                                        on det.productId equals pro.productId
                                        where ord.userId == id && ord.status != "Ready for delivery"
                                        select new
                                        {
                                            order = new
                                            {
                                                ord.orderId,
                                                ord.bill,
                                                ord.discount,
                                                ord.modeOfpayment,
                                                ord.vendorId,
                                                ord.deliveryCharges,
                                                ord.date,
                                                ord.netBill,
                                                ord.totalBill,
                                                ord.status,
                                                ord.vat,
                                                ord.transactionId,
                                                ord.url,
                                                ord.promoCodeId,
                                                ord.note,
                                                ord.deliveryModeId,
                                                ord.paidAmount,
                                                ord.userId
                                            },
                                            orderDetail = new
                                            {
                                                det.orderDetailsid,
                                                det.addressId,
                                                det.addressTitle,
                                                det.cost,
                                                det.deliverToMe,
                                                det.deliveryMethod,
                                                det.flavourId,
                                                det.flavourName,
                                                det.latitude,
                                                det.layerId,
                                                det.layerName,
                                                det.longitude,
                                                det.message,
                                                det.address,
                                                det.pickupTime,
                                                det.productId,
                                                det.productName,
                                                det.size,
                                                det.sizeId,
                                                det.contactNo,
                                                det.name,
                                                det.orderId,
                                                det.streetNo,
                                                det.note,
                                                det.qty,
                                                detailStatus = det.status,
                                                detailVendorId = det.vendorId
                                            },
                                            product = new 
                                            {
                                                pro.productId,
                                                pro.title,
                                                pro.price,
                                                pro.categoryId,
                                                pro.description,
                                                pro.size,
                                                pro.vendorId,
                                                pro.persons,
                                                pro.weight,
                                                pro.approvedOn,
                                                pro.isApproved,
                                                pro.coverImg,
                                                pro.fillingName,
                                                pro.minimumTime,
                                                pro.perLayerCost,
                                            },
                                        }).ToList();

                //var orderDetails = (from ord in db.oOrders
                //                    join det in db.orderDetails
                //                    on ord.orderId equals det.orderId
                //                    join pro in db.products
                //                    on det.productId equals pro.productId
                //                    where ord.userId == id && ord.status == "Ready for delivery"
                //                    select new
                //                    {
                //                        ord,
                //                        det,
                //                        pro
                //                    }).ToList();


                //var products = (from ord in db.oOrders
                //                join det in db.orderDetails
                //                on ord.orderId equals det.orderId
                //                join pro in db.products
                //                on det.productId equals pro.productId
                //                where ord.userId == id && ord.status == "Ready for delivery"
                //                select new
                //                {

                //                    pro.productId,
                //                    pro.title,
                //                    pro.price,
                //                    pro.categoryId,
                //                    pro.description,
                //                    pro.size,
                //                    pro.vendorId,
                //                    pro.persons,
                //                    pro.weight,
                //                    pro.approvedOn,
                //                    pro.isApproved,
                //                    pro.coverImg,
                //                    pro.fillingName,
                //                    pro.minimumTime,
                //                    pro.perLayerCost,
                //                }).ToList().Distinct();


                //if (query == null)
                //{
                //    return NotFound();
                //}

                return Ok(new { readyForDelivery , notReady });
            }
        }

        // PUT: api/oOrders/5
        [ResponseType(typeof(void))]
        [HttpPut]
        [Route("api/oOrders/updateoOrder/{id}")]
        public IHttpActionResult PutoOrder(long id, oOrder oOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != oOrder.orderId)
                {
                    return BadRequest();
                }

                db.Entry(oOrder).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }

                catch (DbUpdateConcurrencyException)
                {
                    if (!oOrderExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);

        }
        protected static object orderList<oOrder>(IEnumerable<oOrder> list)
        {


            Entities db = new Entities();
            db.oOrders.AddRange((IEnumerable<Model.oOrder>)list);
            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex;
            }
            Added st = new Added();
            return st;

        }

        [HttpPost]
        public IHttpActionResult PostOrderList(IEnumerable<oOrder> list)
        {
            var row = orderList(list);

            return Ok(row);

        }

        [HttpPost]
        public IHttpActionResult PostoOrder(OrderViewModel vm)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }

                    if (vm.orderlist[0].addressId == null)
                    {
                        address add = new address();
                        add.title = vm.orderlist[0].addressTitle;
                        add.city = vm.orderlist[0].addressTitle;
                        add.date = vm.orderlist[0].date;
                        add.userId = vm.userId;
                        add.latitude = vm.orderlist[0].latitude.ToString();
                        add.longitude = vm.orderlist[0].longitude.ToString();
                        add.mapAddress = vm.orderlist[0].address;
                        add.addressOne = vm.orderlist[0].address;
                        db.addresses.Add(add);
                        db.SaveChanges();
                        vm.orderlist[0].addressId = add.addressId;
                    }


                    oOrder od = new oOrder();
                    od.bill = vm.bill;
                   // od.date = vm.date;
                    od.deliveryCharges = vm.deliveryCharges;
                    od.deliveryModeId = vm.deliveryModeId;
                    od.discount = vm.discount;
                    od.modeOfpayment = vm.modeOfpayment;
                    od.netBill = vm.netBill;
                    od.note = vm.note;
                    od.orderId = vm.orderId;
                    od.paidAmount = vm.paidAmount;
                    od.promoCodeId = vm.promoCodeId;
                    od.status = vm.status;
                    od.totalBill = vm.totalBill;
                    od.transactionId = vm.transactionId;
                    od.url = vm.url;
                    od.userId = vm.userId;
                    od.vat = vm.vat;
                    od.vendorId = vm.vendorId;
                    od.date = vm.orderlist[0].date;

                    db.oOrders.Add(od);


                    int check = db.SaveChanges();

                    if (check > 0)
                    {
                        long Id = od.orderId;
                        for (int i = 0; i < vm.orderlist.Count; i++)
                        {
                            
                            vm.orderlist[i].orderId = Id;
                            db.orderDetails.AddRange(vm.orderlist);
                        }
                        int check2 = db.SaveChanges();
                        
                        if (check2 > 0)
                        {
                            try
                            {
                                var userId = db.vendors.Where(x => x.vendorId == vm.vendorId).Select(x => x.userId).FirstOrDefault();
                                var deviceId = db.users.Where(x => x.userId == userId).Select(x => x.deviceId).ToArray();
                                long productId = (long)vm.orderlist[0].productId;
                                var product = db.products.Where(x => x.productId == productId).Select(x=> new 
                                {
                                x.title,
                                x.price,
                                x.coverImg
                                }).FirstOrDefault();
                                NotificationViewModel nvm = new  NotificationViewModel();
                                nvm.playerId = deviceId;
                                nvm.title = product.title;
                                nvm.message = "You have received an order which include " + product.title + " ." ;
                                nvm.tag = "Order";

                                notification noti = new notification();
                                noti.date = OkaaikAPIs.TimeZone.TimeZone.UaeDateWithTime();
                                noti.message = nvm.message;
                                noti.userId = userId;
                                noti.picture = product.coverImg;
                                noti.title = product.title;
                                noti.price = product.price;
                                db.notifications.Add(noti);
                                db.SaveChanges();
                                var response =  OkaaikAPIs.Notifications.PushNotification.Send_Notification(nvm);

                                var userAddresses = db.addresses.Where(add => add.userId == od.userId).Select(add => new
                                {
                                    add.addressId,
                                    add.addressOne,
                                    add.city,
                                    add.date,
                                    add.deviceId,
                                    add.latitude,
                                    add.longitude,
                                    add.mapAddress,
                                    add.mobilePhone,
                                    add.name,
                                    add.title,
                                    add.userId,
                                }).ToList();
                                return Ok(new
                                {
                                    data = userAddresses,
                                    status = "success",

                                });

                            }
                            catch (Exception)
                            {

                                var userAddresses = db.addresses.Where(add => add.userId == od.userId).Select(add => new
                                {
                                    add.addressId,
                                    add.addressOne,
                                    add.city,
                                    add.date,
                                    add.deviceId,
                                    add.latitude,
                                    add.longitude,
                                    add.mapAddress,
                                    add.mobilePhone,
                                    add.name,
                                    add.title,
                                    add.userId,
                                }).ToList();
                                return Ok(new
                                {
                                    data = userAddresses,
                                    status = "success",

                                });
                                throw;
                            }


                        }
                        else
                        {
                            return Ok(new
                            {
                                data = "something went wrong.",
                                status = "error",

                            });
                        }

                    }
                    else 
                    {
                        return Ok(new
                        {
                            data = "something went wrong.",
                            status = "error",

                        });
                    }



                    
                }
            }
            catch (Exception)
            {

                throw;
            }
        
        }

        private bool oOrderExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.oOrders.Count(e => e.orderId == id) > 0;

            }
        }
    }
}