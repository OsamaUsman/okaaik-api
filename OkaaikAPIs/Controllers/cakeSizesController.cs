﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class cakeSizesController : ApiController
    {
        // GET: api/cakeSizes
        [Route("api/cakeSizes/getcakeSizes")]
        [HttpGet]
        public IHttpActionResult GetcakeSizes()
        {
            using (Entities db = new Entities())
            {
                var query = (from cs in db.cakeSizes
                             select new
                             {
                                 cs.cakeSizesId,
                                 cs.productId,
                                 cs.size,
                                 cs.cost,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [Route("api/cakeSizes/getByproductcakeSizes/{id}")]
        [HttpGet]
        public IHttpActionResult GetByProductcakeSizes(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from cs in db.cakeSizes
                             where cs.productId == id
                             select new
                             {
                                 cs.cakeSizesId,
                                 cs.productId,
                                 cs.size,
                                 cs.cost,

                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        // GET: api/cakeSizes/5
        [Route("api/cakeSizes/getcakeSize/{id}")]
        [HttpGet]
        public IHttpActionResult GetcakeSize(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from cs in db.cakeSizes
                             where cs.cakeSizesId == id
                             select new
                             {
                                 cs.cakeSizesId,
                                 cs.productId,
                                 cs.size,
                                 cs.cost,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }
        [HttpPut]
        public IHttpActionResult updatecakeSize(long id, cakeSize cakeSize)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != cakeSize.cakeSizesId)
                {
                    return BadRequest();
                }

                db.Entry(cakeSize).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!cakeSizeExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }





        [HttpPut]
        public IHttpActionResult updatedcakeSize(IEnumerable<cakeSize> list)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            using (Entities db = new Entities())
            {
                cakeSize c = list.ElementAtOrDefault(0);
                try
                {
                    if (c.productId != 0)
                    {

                        db.cakeSizes.RemoveRange(db.cakeSizes.Where(x => x.productId == c.productId));
                        db.SaveChanges();

                        var row = cakeSizesList(list);

                        return Ok(row);


                    }
                }
                catch (Exception)
                {

                    throw;
                }


            }
            Updated st = new Updated();
            return Ok(st);
        }

        protected static object cakeSizesList<cakeSize>(IEnumerable<cakeSize> list)
        {


            Entities db = new Entities();

            try
            {
                db.cakeSizes.AddRange((IEnumerable<Model.cakeSize>)list);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                return ex;
            }
            Added st = new Added();
            return st;

        }

        [HttpPost]
        public IHttpActionResult PostsizesList(IEnumerable<cakeSize> list)
        {
            var row = cakeSizesList(list);

            return Ok(row);

        }




        // POST: api/cakeSizes
        [Route("api/cakeSizes/addcakeSize")]
        [HttpPost]
        public IHttpActionResult PostcakeSize(cakeSize cakeSize)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.cakeSizes.Add(cakeSize);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }

        // DELETE: api/cakeSizes/5
        [Route("api/cakeSizes/removecakeSize/{id}")]
        [HttpDelete]
        public IHttpActionResult DeletecakeSize(long id)
        {
            using (Entities db = new Entities())
            {
                cakeSize cakeSize = db.cakeSizes.Find(id);
                if (cakeSize == null)
                {
                    return NotFound();
                }

                db.cakeSizes.Remove(cakeSize);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }



        private bool cakeSizeExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.cakeSizes.Count(e => e.cakeSizesId == id) > 0;
            }
        }
    }
}