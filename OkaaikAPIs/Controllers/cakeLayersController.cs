﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{

    public class cakeLayersController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.cakeLayers

                             select new
                             {
                                 bn.cakeLayerId,
                                 bn.dumyLayer,
                                 bn.cakeLayer1,
                                 bn.price,
                                 bn.vendorId,
                                 bn.productId,

                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.cakeLayers
                             where bn.cakeLayerId == id
                             select new
                             {
                                 bn.cakeLayerId,
                                 bn.dumyLayer,
                                 bn.cakeLayer1,
                                 bn.price,
                                 bn.vendorId,
                                 bn.productId,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, cakeLayer cakeLayer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != cakeLayer.cakeLayerId)
                {
                    return BadRequest();
                }

                db.Entry(cakeLayer).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!cakeLayerExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(cakeLayer cakeLayer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.cakeLayers.Add(cakeLayer);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                cakeLayer cakeLayer = db.cakeLayers.Find(id);
                if (cakeLayer == null)
                {
                    return NotFound();
                }

                db.cakeLayers.Remove(cakeLayer);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool cakeLayerExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.cakeLayers.Count(e => e.cakeLayerId == id) > 0;
            }
        }
    }
}
