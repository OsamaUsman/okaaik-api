﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class pickupSlotsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var query = (from bn in db.pickupSlots
                                 select new
                                 {
                                     bn.pickupSlotsId,
                                     bn.day,
                                     bn.slot,
                                 }).ToList();
                    if (query == null)
                    {
                        NotFound();
                    }

                    return Ok(query);
                }

            }
            catch (Exception)
            {

                throw;
            }
           

        }





        // GET: api/cakeSizes/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.pickupSlots
                             where bn.vendorId == id
                             select new
                             {
                                 bn.pickupSlotsId,
                                 bn.day,
                                 bn.slot,
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }

        [HttpGet]
        public IHttpActionResult ByDay(string day)
        {
            try
            {
                using (Entities db = new Entities())
                {
                    var query = (from bn in db.pickupSlots
                                 where bn.day == day
                                 select new
                                 {
                                     bn.pickupSlotsId,
                                     bn.day,
                                     bn.slot,
                                 }).ToList();
                    if (query == null)
                    {
                        NotFound();
                    }

                    return Ok(query);
                }
            }
            catch (Exception)
            {

                throw;
            }
          


        }
        [HttpPut]
        public IHttpActionResult Put(long id, pickupSlot pickupSlot)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != pickupSlot.pickupSlotsId)
                {
                    return BadRequest();
                }

                db.Entry(pickupSlot).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!pickupSlotExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(pickupSlot pickupSlot)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.pickupSlots.Add(pickupSlot);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                pickupSlot pickupSlot = db.pickupSlots.Find(id);
                if (pickupSlot == null)
                {
                    return NotFound();
                }

                db.pickupSlots.Remove(pickupSlot);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool pickupSlotExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.pickupSlots.Count(e => e.pickupSlotsId == id) > 0;
            }
        }
    }
}
