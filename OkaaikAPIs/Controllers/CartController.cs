﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class CartController : ApiController
    {
       
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var data = (from us in db.products
                            join cr in db.carts
                            on us.productId equals cr.cakeID
                            where cr.userID == id
                            && cr.qty > 0
                            select new
                            {
                                userID = id,
                                productID = us.productId,
                                coverImg = us.coverImg,
                                productName = us.title,
                                description = us.description,
                                qty = cr.qty,
                                price = cr.price,
                                minimumDays = us.minimumTime,
                                cart = cr,
                            }).ToList();
                if (data == null)
                {
                    NotFound();
                }

                decimal tt = 0;
                try
                {
                    var total = (from us in db.carts
                                 where us.userID == id
                                 select us.price).Sum();
                    tt = total.Value;
                }
                catch (Exception x)
                {
                }
               

                return Json(new { status = "success", data = data, count = data.Count, totalPrice = tt });
            }
        }

        [HttpPost]
        public IHttpActionResult Save(cart data)
        {            
            using (Entities db = new Entities())
            {
                var chk = (from us in db.carts
                           where us.userID == data.userID
                           && us.cakeID == data.cakeID
                           select us).FirstOrDefault();
                if (chk == null)
                {
                    if (data.qty > 0)
                    {
                        db.carts.Add(data);
                        db.SaveChanges();
                    }
                    
                }
                else
                {
                    chk.qty += data.qty;
                    if (chk.qty <= 0)
                    {
                        db.carts.Remove(chk);
                        db.SaveChanges();
                    }
                    else
                    {
                        chk.price = chk.price + (data.qty * data.price);
                        db.SaveChanges();
                    }
                    
                }
            }


            return Json(new { status = "success", data = "Saved" });
        }

        [HttpPost]
        public IHttpActionResult ClearCart(long userID)
        {
            using (Entities db = new Entities())
            {
                var chk = (from us in db.carts
                           where us.userID == userID                          
                           select us).ToList();
                if (chk != null)
                {
                    db.carts.RemoveRange(chk);
                    db.SaveChanges();
                }

                return Json(new { status = "success", data = "removed." });
            }
        }

        private IHttpActionResult Ok(object query)
        {
            throw new NotImplementedException();
        }

        private void NotFound()
        {
            throw new NotImplementedException();
        }
    }
}