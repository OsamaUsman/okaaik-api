﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using OkaaikAPIs.Model;

namespace OkaaikAPIs.Controllers
{

    public class remindersController : ApiController
    {

        [HttpPost]
        [Route("api/reminders/Getreminder/")]
        public IHttpActionResult Getreminder(DeviceViewModel vm)
        {
            using (Entities db = new Entities())
            {
                try
                {
                    var query = (from re in db.reminders
                                 where re.userId == vm.userid || re.deviceId == vm.DeviceId
                                 select new
                                 {
                                     re.reminderId,
                                     re.reminderTitle,
                                     re.date,
                                     re.deviceId,
                                     re.notification,
                                     re.userId,
                                     re.remindDate,
                                     re.occasionId,
                                     re.occasionTitle
                                 }).ToList();
                    return Ok(query);


                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

        [HttpPut]
        [Route("api/reminders/updateReminder/{id}")]
        public IHttpActionResult Updatereminder(long id, reminder reminder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != reminder.reminderId)
                {
                    return BadRequest();
                }

                db.Entry(reminder).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!reminderExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }

        [HttpPost]

        public IHttpActionResult Postreminder(reminder reminder)
        {
            using (Entities db = new Entities())
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                db.reminders.Add(reminder);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Added st = new Added();
                return Ok(st);
            }
        }

        [HttpDelete]
        [Route("api/reminders/removeReminderbyuser/{id}")]
        public IHttpActionResult Deletereminderbyuser(long id)
        {
            using (Entities db = new Entities())
            {

                db.reminders.RemoveRange(db.reminders.Where(r => r.userId == id));

                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);

            }

        }
        [HttpDelete]
        [Route("api/reminders/removeReminder/{id}")]
        public IHttpActionResult Deletereminder(long id)
        {
            using (Entities db = new Entities())
            {
                reminder reminder = db.reminders.Find(id);
                if (reminder == null)
                {
                    return NotFound();
                }



                db.reminders.Remove(reminder);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);

            }

        }


        private bool reminderExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.reminders.Count(e => e.reminderId == id) > 0;
            }
        }
    }
}