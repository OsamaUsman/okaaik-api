﻿using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OkaaikAPIs.Controllers
{
    public class FillingsController : ApiController
    {
        [HttpGet]
        public IHttpActionResult GetAll(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.Fillings
                             where bn.vendorId == id
                             select new
                             {
                                 bn.fillingId,
                                 bn.name,
                                 bn.status,
                                 bn.vendorId
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }

        [HttpGet]
        public IHttpActionResult GetAllActive(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.Fillings
                             where bn.vendorId == id && bn.status.ToLower() == "in stock"
                             select new
                             {
                                 bn.fillingId,
                                 bn.name,
                                 bn.status,
                                 bn.vendorId
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }



        // GET: api/cakeFillings/5
        [HttpGet]
        public IHttpActionResult Get(long id)
        {
            using (Entities db = new Entities())
            {
                var query = (from bn in db.Fillings
                             where bn.fillingId == id
                             select new
                             {
                                 bn.fillingId,
                                 bn.name,
                                 bn.status,
                                 bn.vendorId
                             }).ToList();
                if (query == null)
                {
                    NotFound();
                }

                return Ok(query);
            }


        }


        [HttpPut]
        public IHttpActionResult Put(long id, Filling Filling)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                if (id != Filling.fillingId)
                {
                    return BadRequest();
                }

                db.Entry(Filling).State = EntityState.Modified;

                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!FillingExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            Updated st = new Updated();
            return Ok(st);
        }


        [HttpPost]
        public IHttpActionResult Post(Filling Filling)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            using (Entities db = new Entities())
            {
                db.Fillings.Add(Filling);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
            }
            Added st = new Added();
            return Ok(st);
        }


        [HttpDelete]
        public IHttpActionResult Delete(long id)
        {
            using (Entities db = new Entities())
            {
                Filling Filling = db.Fillings.Find(id);
                if (Filling == null)
                {
                    return NotFound();
                }

                db.Fillings.Remove(Filling);
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    return Ok(ex);
                }
                Deleted st = new Deleted();
                return Ok(st);
            }
        }







        private bool FillingExists(long id)
        {
            using (Entities db = new Entities())
            {
                return db.Fillings.Count(e => e.fillingId == id) > 0;
            }
        }
    }
}
