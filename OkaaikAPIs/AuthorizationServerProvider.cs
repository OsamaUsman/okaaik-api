﻿using Microsoft.Owin.Security.OAuth;
using OkaaikAPIs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;

namespace OkaaikAPIs
{
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }
        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            using (Entities db = new Entities())
            {
                var pass = EncryptDecrypt.Encrypt(context.Password);
                var row = db.users.Where(s => s.email == context.UserName && s.password == pass).FirstOrDefault();
                var identity = new ClaimsIdentity(context.Options.AuthenticationType);


                if (row != null)
                {


                    if (context.UserName == row.email && pass == row.password)
                    {
                        if (row.roles == "Admin" || row.roles == "admin")
                        {
                            identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                            identity.AddClaim(new Claim("username", "admin"));
                            identity.AddClaim(new Claim(ClaimTypes.Name, "Hi Admin"));
                            context.Validated(identity);
                        }


                    }
                    else
                    {
                        //context.SetError( "invalid_grant");
                        //string jsonString = "{\"status\": \"success.\",\"data\": \"not exist.\"}";

                        // This is just a work around to overcome an unknown internal bug. 
                        // In future releases of Owin, you may remove this.
                        //context.SetError(new string(' ', jsonString.Length - 12));

                        //context.Response.StatusCode = 400;
                        context.SetError("invalid_grant", "Provided email and password is incorrect");
                        //context.Response.Write(jsonString);
                        return;
                    }

                    if (context.UserName == row.email && pass == row.password)
                    {

                        if (row.roles == "User" || row.roles == "user")
                        {
                            identity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                            identity.AddClaim(new Claim("Id", row.userId.ToString()));
                            // identity.AddClaim(new Claim(ClaimTypes.Name , row.userName));
                            //identity.AddClaim(new Claim(ClaimTypes., row.email));

                            context.Validated(identity);
                        }

                    }

                }
                else
                {
                    //string jsonString = "{\"status\": \"success.\",\"data\": \"not exist.\"}";

                    // This is just a work around to overcome an unknown internal bug. 
                    // In future releases of Owin, you may remove this.
                    //context.SetError(new string(' ', jsonString.Length - 12));

                    // context.Response.StatusCode = 400;
                    //context.Response.Write(jsonString);
                    context.SetError("invalid_grant", "Provided email and password is incorrect");

                    return;
                    //return;
                }
            }


        }
    }
}